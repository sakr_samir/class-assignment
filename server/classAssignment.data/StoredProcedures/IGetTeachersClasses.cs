﻿using classAssignment.shared.ViewModels.Teacher;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace classAssignment.data.StoredProcedures
{
  public interface IGetTeachersClasses
    {
        public SqlDataReader GetGetTeachersClasses(int SchoolCode);
        public List<TeacherVM> BindGetTeachersClasses(SqlDataReader reader);
    }
}
