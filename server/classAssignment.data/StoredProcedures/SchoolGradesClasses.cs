﻿using classAssignment.shared.ViewModels.Class;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace classAssignment.data.StoredProcedures
{
    public class SchoolGradesClasses : ISchoolGradesClasses
    {
        private readonly ProjectContext _context;
        private readonly IConfiguration _config;

        public SchoolGradesClasses(ProjectContext context,IConfiguration config)
        {
            _context = context;
            _config = config;
        }
        public StageVM BindSchoolClasses(SqlDataReader reader, int SchoolCode)
        {
            List<GradeVM> GradeVMList = new List<GradeVM>();
            List<SpGetSchoolClassesVM> SpGetSchoolClassesVMList = new List<SpGetSchoolClassesVM>();
            while (reader.HasRows)
            {
                while (reader.Read())
                {
                    SpGetSchoolClassesVM SpGetSchoolClassesVM = new SpGetSchoolClassesVM();
                    SpGetSchoolClassesVM.GradeCode = reader.GetInt32(0);
                    SpGetSchoolClassesVM.GradeName = reader.GetString(1);
                    SpGetSchoolClassesVM.ClassesNum = reader.GetInt32(2);
                    SpGetSchoolClassesVM.ClassId = reader.GetInt32(3);
                    SpGetSchoolClassesVM.ClassName = reader.GetString(4);
                    SpGetSchoolClassesVM.StudentCount = reader.GetInt32(5);
                    SpGetSchoolClassesVM.TeacherCount = reader.GetInt32(6);

                    SpGetSchoolClassesVMList.Add(SpGetSchoolClassesVM);
                }
                reader.NextResult();
            }

            var gradeCodes = SpGetSchoolClassesVMList.Select(grade => grade.GradeCode).Distinct();

            //int assignedTeacherNum = 0;
            foreach (var gradeCode in gradeCodes)
            {
                var selectedGrade = SpGetSchoolClassesVMList.FirstOrDefault(grade => grade.GradeCode == gradeCode);
                GradeVM gradeVM = new GradeVM { Code = gradeCode, name= selectedGrade.GradeName,classNum= selectedGrade.ClassesNum };
                var sameGrades = SpGetSchoolClassesVMList.Where(grade => grade.GradeCode == gradeCode&&grade.ClassId!=0);
                gradeVM.classes = sameGrades.Select(grade => new ClassVM
                {
                    id = grade.ClassId,
                    name= grade.ClassName,
                    totalStudents = grade.StudentCount,
                    totalTeachers = grade.TeacherCount
                }).ToList();

                gradeVM.totalStudentNum = _context.Student.Where(x => x.GRADE_CODE == gradeCode&&x.SCH_CODE == SchoolCode).Count();
                gradeVM.unassignedStudentNum = gradeVM.totalStudentNum - gradeVM.classes.Sum(s => s.totalStudents);
               // assignedTeacherNum += gradeVM.classes.Sum(s => s.totalTeachers);
                GradeVMList.Add(gradeVM);

            }
            int totalTeacherNum = _context.Teacher.Count(x => x.SCH_CODE == SchoolCode);

            int assignedTeacherToClassNum = _context.ClassTeacherSubjects
                .Where(x=>x.TeacherSubject.Teacher.SCH_CODE == SchoolCode)
                .Select(x=>x.TeacherSubject.Teacher.EMP_CODE).Distinct().Count();

            int assignedTeacherToSubjectNum = _context.TeacherSubjects
                .Where(x => x.Teacher.SCH_CODE == SchoolCode)
                .Select(x => x.Teacher.EMP_CODE).Distinct().Count();

            StageVM Stage = new StageVM
            {
                Grades = GradeVMList,
                MaxNumberOfClasses = _config.GetValue<int>("MaxNumberOfClasses"),
                unassignedTeacherToClassNum = totalTeacherNum - assignedTeacherToClassNum,
                unassignedTeacherToSubjectNum = totalTeacherNum - assignedTeacherToSubjectNum,
                unassignedStudentNum = GradeVMList.Sum(x=>x.unassignedStudentNum)
            };

            return Stage;
        }

        public SqlDataReader GetSchoolGradesClasses(int SchoolCode)
        {
            using (var command = _context.Database.GetDbConnection().CreateCommand())
            {
                command.CommandText = "GetSchoolClasses";
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@SchoolCode", SchoolCode));
                _context.Database.OpenConnection();
                SqlDataReader reader = (SqlDataReader)command.ExecuteReader();
                return reader;
            }
        }
    }
}
