﻿using classAssignment.shared.ViewModels.Class;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace classAssignment.data.StoredProcedures
{
  public interface ISchoolGradesClasses
    {
        public SqlDataReader GetSchoolGradesClasses(int SchoolCode);
        public StageVM BindSchoolClasses(SqlDataReader reader, int SchoolCode);
    }
}
