﻿using classAssignment.shared.ViewModels.Teacher;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace classAssignment.data.StoredProcedures
{
    public class GetTeachersClasses : IGetTeachersClasses
    {
        private readonly ProjectContext _context;
        public GetTeachersClasses(ProjectContext context)
        {
            _context = context;
        }
        public List<TeacherVM> BindGetTeachersClasses(SqlDataReader reader)
        {
            List<TeacherVM> teacherVMs = new List<TeacherVM>();

            List<SpGetTeacherClassesVM> SpGetTeacherClassesVMList = new List<SpGetTeacherClassesVM>();
            while (reader.HasRows)
            {
                while (reader.Read())
                {
                    SpGetTeacherClassesVM SpGetTeacherClassesVM = new SpGetTeacherClassesVM();
                    SpGetTeacherClassesVM.EMP_CODE = reader.GetInt32(0);
                    SpGetTeacherClassesVM.Teacher_CODE = reader.GetInt32(1);
                    SpGetTeacherClassesVM.Emp_Name = reader.GetString(2);
                    SpGetTeacherClassesVM.Subj_ID = reader.IsDBNull(3)==true?0: reader.GetInt32(3);
                    SpGetTeacherClassesVM.Sub_Name = reader.IsDBNull(4) == true ? "" : reader.GetString(4); 
                    SpGetTeacherClassesVM.Class_Name = reader.IsDBNull(6) == true ? "" : reader.GetString(6);
                    //SpGetTeacherClassesVM.CTS_Id = reader.IsDBNull(5) == true ? 0 : reader.GetInt32(5);
                    SpGetTeacherClassesVM.Class_Id = reader.IsDBNull(7) == true ? 0 : reader.GetInt32(7);

                    SpGetTeacherClassesVMList.Add(SpGetTeacherClassesVM);
                }
                reader.NextResult();
            }

            var teachersCode = SpGetTeacherClassesVMList.Select(emp => emp.EMP_CODE).Distinct();

            foreach (int teacherCode in teachersCode)
            {
                var commonAttributes = SpGetTeacherClassesVMList.Where(x => x.EMP_CODE == teacherCode);
                TeacherVM teacherVM = new TeacherVM { Code = teacherCode, Name = commonAttributes.FirstOrDefault().Emp_Name,TeacherCode=commonAttributes.FirstOrDefault().Teacher_CODE };
                teacherVM.Classes = commonAttributes.Select(x=>x.Class_Name).Distinct().ToList();

                var teacherSubjectsCode = commonAttributes.Select(x => x.Subj_ID).Distinct();
                List<SubjectViewModel> subjects = new List<SubjectViewModel>();
                foreach (var sub_code in teacherSubjectsCode)
                {
                    if (sub_code !=0)
                        subjects.Add(new SubjectViewModel { Id = sub_code, Name = commonAttributes.FirstOrDefault(x => x.Subj_ID == sub_code).Sub_Name });
                }
                teacherVM.TeacherSubjects = subjects;

                teacherVMs.Add(teacherVM);
            }

            
            return teacherVMs;
        }

        public SqlDataReader GetGetTeachersClasses(int SchoolCode)
        {
            using (var command = _context.Database.GetDbConnection().CreateCommand())
            {
                command.CommandText = "GetTeachersClasses";
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@SchoolCode", SchoolCode));
                _context.Database.OpenConnection();
                SqlDataReader reader = (SqlDataReader)command.ExecuteReader();
                return reader;
            }
        }
    }
}
