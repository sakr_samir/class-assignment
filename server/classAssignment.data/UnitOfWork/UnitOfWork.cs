﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using classAssignment.data.Repositories;
using classAssignment.entities;
using classAssignment.entities.Abstraction;

namespace classAssignment.data.UnitOfWork
{
    public class UnitOfWork: IUnitOfWork
    {
        public ProjectContext Context = null;

        private IRepository<User> userRepository;
        public UnitOfWork(ProjectContext context)
        {
            Context = context;
        }
        public Dictionary<Type, object> repositories = new Dictionary<Type, object>();

        public IRepository<User> _UserRepository
        {
            get
            {
                if (userRepository == null)
                {
                    userRepository = new GenericRepository<User>(Context);
                }
                return userRepository;
            }
        }
        public IRepository<T> Repository<T>() where T : class
        {
            if (repositories.Keys.Contains(typeof(T)) == true)
            {
                return repositories[typeof(T)] as IRepository<T>;
            }
            IRepository<T> repo = new GenericRepository<T>(Context);
            repositories.Add(typeof(T), repo);
            return repo;
        }

        

        public async Task<bool> SaveChangesAsync()
        {
            try
            {
                var result = await Context.SaveChangesAsync();
                return Convert.ToBoolean(result);

            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool SaveChanges()
        {
            try
            {
                Context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private bool disposed = false;

        public void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    Context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    
}
}
