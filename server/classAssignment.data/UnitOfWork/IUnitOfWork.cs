﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using classAssignment.data.Repositories;
using classAssignment.entities;
using classAssignment.entities.Abstraction;

namespace classAssignment.data.UnitOfWork
{
   public interface IUnitOfWork
    {
        Task<bool> SaveChangesAsync();
        bool SaveChanges();
        void Dispose(bool disposing);
        IRepository<T> Repository<T>() where T : class;
        IRepository<User> _UserRepository { get; }
    }
}
