﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace classAssignment.data.Migrations
{
    public partial class lookuptables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "SCH_CODE",
                table: "User",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Grade",
                columns: table => new
                {
                    GRADE_CODE = table.Column<int>(type: "int", nullable: false),
                    GRADE_Name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Grade", x => x.GRADE_CODE);
                });

            migrationBuilder.CreateTable(
                name: "Rel",
                columns: table => new
                {
                    REL_CODE = table.Column<int>(type: "int", nullable: false),
                    REL_Name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Rel", x => x.REL_CODE);
                });

            migrationBuilder.CreateTable(
                name: "School",
                columns: table => new
                {
                    SCH_CODE = table.Column<int>(type: "int", nullable: false),
                    GOV_CODE = table.Column<int>(type: "int", nullable: false),
                    GOV_DESC = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DIST_ID = table.Column<int>(type: "int", nullable: false),
                    DIST_NAME = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    STAGE_CODE = table.Column<int>(type: "int", nullable: false),
                    STAGE_DESC = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    REL_CODE = table.Column<int>(type: "int", nullable: false),
                    REL_DESC = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SCH_DESC = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_School", x => x.SCH_CODE);
                });

            migrationBuilder.CreateTable(
                name: "SchoolClass",
                columns: table => new
                {
                    SCH_CLASS_CODE = table.Column<int>(type: "int", nullable: false),
                    SCH_CODE = table.Column<int>(type: "int", nullable: false),
                    SCH_NAME = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    GRADE_CODE = table.Column<int>(type: "int", nullable: false),
                    CLASSES_NUM = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SchoolClass", x => x.SCH_CLASS_CODE);
                });

            migrationBuilder.CreateTable(
                name: "Stage",
                columns: table => new
                {
                    STAGE_CODE = table.Column<int>(type: "int", nullable: false),
                    STAGE_NAME = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Stage", x => x.STAGE_CODE);
                });

            migrationBuilder.CreateTable(
                name: "StageGrade",
                columns: table => new
                {
                    GRADE_STAGE_CODE = table.Column<int>(type: "int", nullable: false),
                    STAGE_CODE = table.Column<int>(type: "int", nullable: false),
                    STAGE_NAME = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    GRADE_CODE = table.Column<int>(type: "int", nullable: false),
                    GRADE_Name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StageGrade", x => x.GRADE_STAGE_CODE);
                });

            migrationBuilder.CreateTable(
                name: "Student",
                columns: table => new
                {
                    STD_CODE = table.Column<int>(type: "int", nullable: false),
                    FIRST_NAME = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FATHER_NAME = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    G_FATHER_NAME = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FAMILY_NAME = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SCH_CODE = table.Column<int>(type: "int", nullable: false),
                    SCH_NAME = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    GRADE_CODE = table.Column<int>(type: "int", nullable: false),
                    GRADE_Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CLASS_CODE = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Student", x => x.STD_CODE);
                });

            migrationBuilder.CreateTable(
                name: "Subject",
                columns: table => new
                {
                    SUBJ_CODE = table.Column<int>(type: "int", nullable: false),
                    SUBJ_NAME = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    STAGE_CODE = table.Column<int>(type: "int", nullable: false),
                    STAGE_NAME = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Subject", x => x.SUBJ_CODE);
                });

            migrationBuilder.CreateTable(
                name: "Teacher",
                columns: table => new
                {
                    EMP_CODE = table.Column<int>(type: "int", nullable: false),
                    EMP_FIRST_NAME = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    EMP_FATHER_NAME = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    EMP_GRAND_FATHER_NAME = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    EMP_FAMILY_NAME = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SCH_CODE = table.Column<int>(type: "int", nullable: false),
                    SCH_NAME = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Teacher", x => x.EMP_CODE);
                });

            migrationBuilder.CreateIndex(
                name: "IX_User_SCH_CODE",
                table: "User",
                column: "SCH_CODE");

            migrationBuilder.AddForeignKey(
                name: "FK_User_School_SCH_CODE",
                table: "User",
                column: "SCH_CODE",
                principalTable: "School",
                principalColumn: "SCH_CODE",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_User_School_SCH_CODE",
                table: "User");

            migrationBuilder.DropTable(
                name: "Grade");

            migrationBuilder.DropTable(
                name: "Rel");

            migrationBuilder.DropTable(
                name: "School");

            migrationBuilder.DropTable(
                name: "SchoolClass");

            migrationBuilder.DropTable(
                name: "Stage");

            migrationBuilder.DropTable(
                name: "StageGrade");

            migrationBuilder.DropTable(
                name: "Student");

            migrationBuilder.DropTable(
                name: "Subject");

            migrationBuilder.DropTable(
                name: "Teacher");

            migrationBuilder.DropIndex(
                name: "IX_User_SCH_CODE",
                table: "User");

            migrationBuilder.DropColumn(
                name: "SCH_CODE",
                table: "User");
        }
    }
}
