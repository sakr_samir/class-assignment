﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace classAssignment.data.Migrations
{
    public partial class addteachersubjectentity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TeacherSubjects",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    EMP_CODE = table.Column<int>(type: "int", nullable: false),
                    SUBJ_CODE = table.Column<int>(type: "int", nullable: false),
                    CreationDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ModificationDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TeacherSubjects", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TeacherSubjects_Subject_SUBJ_CODE",
                        column: x => x.SUBJ_CODE,
                        principalTable: "Subject",
                        principalColumn: "SUBJ_CODE",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TeacherSubjects_Teacher_EMP_CODE",
                        column: x => x.EMP_CODE,
                        principalTable: "Teacher",
                        principalColumn: "EMP_CODE",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TeacherSubjects_EMP_CODE",
                table: "TeacherSubjects",
                column: "EMP_CODE");

            migrationBuilder.CreateIndex(
                name: "IX_TeacherSubjects_SUBJ_CODE",
                table: "TeacherSubjects",
                column: "SUBJ_CODE");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TeacherSubjects");
        }
    }
}
