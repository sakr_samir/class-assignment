﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace classAssignment.data.Migrations
{
    public partial class addSSOView : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SSO_View",
                columns: table => new
                {
                    BCH_CODE = table.Column<int>(type: "int", nullable: false),
                    security_code = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SSO_View", x => x.BCH_CODE);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SSO_View");
        }
    }
}
