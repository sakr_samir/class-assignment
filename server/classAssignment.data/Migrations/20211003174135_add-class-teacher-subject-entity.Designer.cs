﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using classAssignment.data;

namespace classAssignment.data.Migrations
{
    [DbContext(typeof(ProjectContext))]
    [Migration("20211003174135_add-class-teacher-subject-entity")]
    partial class addclassteachersubjectentity
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .UseIdentityColumns()
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("ProductVersion", "5.0.0");

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRole", b =>
                {
                    b.Property<string>("Id")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Name")
                        .HasMaxLength(256)
                        .HasColumnType("nvarchar(256)");

                    b.Property<string>("NormalizedName")
                        .HasMaxLength(256)
                        .HasColumnType("nvarchar(256)");

                    b.HasKey("Id");

                    b.HasIndex("NormalizedName")
                        .IsUnique()
                        .HasDatabaseName("RoleNameIndex")
                        .HasFilter("[NormalizedName] IS NOT NULL");

                    b.ToTable("UserRoles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRoleClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .UseIdentityColumn();

                    b.Property<string>("ClaimType")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("ClaimValue")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("RoleId")
                        .IsRequired()
                        .HasColumnType("nvarchar(450)");

                    b.HasKey("Id");

                    b.HasIndex("RoleId");

                    b.ToTable("UserRoleClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .UseIdentityColumn();

                    b.Property<string>("ClaimType")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("ClaimValue")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("UserId")
                        .IsRequired()
                        .HasColumnType("nvarchar(450)");

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("UserClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserLogin<string>", b =>
                {
                    b.Property<string>("LoginProvider")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("ProviderKey")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("ProviderDisplayName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("UserId")
                        .IsRequired()
                        .HasColumnType("nvarchar(450)");

                    b.HasKey("LoginProvider", "ProviderKey");

                    b.HasIndex("UserId");

                    b.ToTable("UserLogins");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserRole<string>", b =>
                {
                    b.Property<string>("UserId")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("RoleId")
                        .HasColumnType("nvarchar(450)");

                    b.HasKey("UserId", "RoleId");

                    b.HasIndex("RoleId");

                    b.ToTable("UserAssignedRoles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserToken<string>", b =>
                {
                    b.Property<string>("UserId")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("LoginProvider")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("Value")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("UserId", "LoginProvider", "Name");

                    b.ToTable("UserTokens");
                });

            modelBuilder.Entity("classAssignment.entities.Class", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .UseIdentityColumn();

                    b.Property<DateTime>("CreationDate")
                        .HasColumnType("datetime2");

                    b.Property<bool>("IsDeleted")
                        .HasColumnType("bit");

                    b.Property<DateTime>("ModificationDate")
                        .HasColumnType("datetime2");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("SCH_CLASS_CODE")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("SCH_CLASS_CODE");

                    b.ToTable("Class");
                });

            modelBuilder.Entity("classAssignment.entities.ClassTeacherSubject", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .UseIdentityColumn();

                    b.Property<int>("ClassId")
                        .HasColumnType("int");

                    b.Property<DateTime>("CreationDate")
                        .HasColumnType("datetime2");

                    b.Property<bool>("IsDeleted")
                        .HasColumnType("bit");

                    b.Property<DateTime>("ModificationDate")
                        .HasColumnType("datetime2");

                    b.Property<int>("TeacherSubjectId")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("ClassId");

                    b.HasIndex("TeacherSubjectId");

                    b.ToTable("ClassTeacherSubjects");
                });

            modelBuilder.Entity("classAssignment.entities.Grade", b =>
                {
                    b.Property<int>("GRADE_CODE")
                        .HasColumnType("int");

                    b.Property<string>("GRADE_Name")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("GRADE_CODE");

                    b.ToTable("Grade");
                });

            modelBuilder.Entity("classAssignment.entities.Rel", b =>
                {
                    b.Property<int>("REL_CODE")
                        .HasColumnType("int");

                    b.Property<string>("REL_Name")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("REL_CODE");

                    b.ToTable("Rel");
                });

            modelBuilder.Entity("classAssignment.entities.School", b =>
                {
                    b.Property<int>("SCH_CODE")
                        .HasColumnType("int");

                    b.Property<int>("DIST_ID")
                        .HasColumnType("int");

                    b.Property<string>("DIST_NAME")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("GOV_CODE")
                        .HasColumnType("int");

                    b.Property<string>("GOV_DESC")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("REL_CODE")
                        .HasColumnType("int");

                    b.Property<string>("REL_DESC")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("SCH_DESC")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("STAGE_CODE")
                        .HasColumnType("int");

                    b.Property<string>("STAGE_DESC")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("SCH_CODE");

                    b.ToTable("School");
                });

            modelBuilder.Entity("classAssignment.entities.SchoolClass", b =>
                {
                    b.Property<int>("SCH_CLASS_CODE")
                        .HasColumnType("int");

                    b.Property<int>("CLASSES_NUM")
                        .HasColumnType("int");

                    b.Property<int>("GRADE_CODE")
                        .HasColumnType("int");

                    b.Property<int>("SCH_CODE")
                        .HasColumnType("int");

                    b.Property<string>("SCH_NAME")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("SCH_CLASS_CODE");

                    b.ToTable("SchoolClass");
                });

            modelBuilder.Entity("classAssignment.entities.Stage", b =>
                {
                    b.Property<int>("STAGE_CODE")
                        .HasColumnType("int");

                    b.Property<string>("STAGE_NAME")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("STAGE_CODE");

                    b.ToTable("Stage");
                });

            modelBuilder.Entity("classAssignment.entities.StageGrade", b =>
                {
                    b.Property<int>("GRADE_STAGE_CODE")
                        .HasColumnType("int");

                    b.Property<int>("GRADE_CODE")
                        .HasColumnType("int");

                    b.Property<string>("GRADE_Name")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("STAGE_CODE")
                        .HasColumnType("int");

                    b.Property<string>("STAGE_NAME")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("GRADE_STAGE_CODE");

                    b.ToTable("StageGrade");
                });

            modelBuilder.Entity("classAssignment.entities.Student", b =>
                {
                    b.Property<int>("STD_CODE")
                        .HasColumnType("int");

                    b.Property<int>("CLASS_CODE")
                        .HasColumnType("int");

                    b.Property<string>("FAMILY_NAME")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("FATHER_NAME")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("FIRST_NAME")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("GRADE_CODE")
                        .HasColumnType("int");

                    b.Property<string>("GRADE_Name")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("G_FATHER_NAME")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("SCH_CODE")
                        .HasColumnType("int");

                    b.Property<string>("SCH_NAME")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("STD_CODE");

                    b.ToTable("Student");
                });

            modelBuilder.Entity("classAssignment.entities.Subject", b =>
                {
                    b.Property<int>("SUBJ_CODE")
                        .HasColumnType("int");

                    b.Property<int>("STAGE_CODE")
                        .HasColumnType("int");

                    b.Property<string>("STAGE_NAME")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("SUBJ_NAME")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("SUBJ_CODE");

                    b.ToTable("Subject");
                });

            modelBuilder.Entity("classAssignment.entities.Teacher", b =>
                {
                    b.Property<int>("EMP_CODE")
                        .HasColumnType("int");

                    b.Property<string>("EMP_FAMILY_NAME")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("EMP_FATHER_NAME")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("EMP_FIRST_NAME")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("EMP_GRAND_FATHER_NAME")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("SCH_CODE")
                        .HasColumnType("int");

                    b.Property<string>("SCH_NAME")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("EMP_CODE");

                    b.ToTable("Teacher");
                });

            modelBuilder.Entity("classAssignment.entities.TeacherSubject", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .UseIdentityColumn();

                    b.Property<DateTime>("CreationDate")
                        .HasColumnType("datetime2");

                    b.Property<int>("EMP_CODE")
                        .HasColumnType("int");

                    b.Property<bool>("IsDeleted")
                        .HasColumnType("bit");

                    b.Property<DateTime>("ModificationDate")
                        .HasColumnType("datetime2");

                    b.Property<int>("SUBJ_CODE")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("EMP_CODE");

                    b.HasIndex("SUBJ_CODE");

                    b.ToTable("TeacherSubjects");
                });

            modelBuilder.Entity("classAssignment.entities.User", b =>
                {
                    b.Property<string>("Id")
                        .HasColumnType("nvarchar(450)");

                    b.Property<int>("AccessFailedCount")
                        .HasColumnType("int");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Email")
                        .HasMaxLength(256)
                        .HasColumnType("nvarchar(256)");

                    b.Property<bool>("EmailConfirmed")
                        .HasColumnType("bit");

                    b.Property<string>("FullName")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<bool>("IsBlocked")
                        .HasColumnType("bit");

                    b.Property<bool>("IsConfirmed")
                        .HasColumnType("bit");

                    b.Property<bool>("LockoutEnabled")
                        .HasColumnType("bit");

                    b.Property<DateTimeOffset?>("LockoutEnd")
                        .HasColumnType("datetimeoffset");

                    b.Property<string>("NormalizedEmail")
                        .HasMaxLength(256)
                        .HasColumnType("nvarchar(256)");

                    b.Property<string>("NormalizedUserName")
                        .HasMaxLength(256)
                        .HasColumnType("nvarchar(256)");

                    b.Property<string>("PasswordHash")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("PhoneNumber")
                        .HasColumnType("nvarchar(max)");

                    b.Property<bool>("PhoneNumberConfirmed")
                        .HasColumnType("bit");

                    b.Property<string>("RoleName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int?>("SCH_CODE")
                        .HasColumnType("int");

                    b.Property<string>("SecurityStamp")
                        .HasColumnType("nvarchar(max)");

                    b.Property<bool>("TwoFactorEnabled")
                        .HasColumnType("bit");

                    b.Property<string>("UserName")
                        .HasMaxLength(256)
                        .HasColumnType("nvarchar(256)");

                    b.HasKey("Id");

                    b.HasIndex("NormalizedEmail")
                        .HasDatabaseName("EmailIndex");

                    b.HasIndex("NormalizedUserName")
                        .IsUnique()
                        .HasDatabaseName("UserNameIndex")
                        .HasFilter("[NormalizedUserName] IS NOT NULL");

                    b.HasIndex("SCH_CODE");

                    b.ToTable("User");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRoleClaim<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.IdentityRole", null)
                        .WithMany()
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserClaim<string>", b =>
                {
                    b.HasOne("classAssignment.entities.User", null)
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserLogin<string>", b =>
                {
                    b.HasOne("classAssignment.entities.User", null)
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserRole<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.IdentityRole", null)
                        .WithMany()
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("classAssignment.entities.User", null)
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserToken<string>", b =>
                {
                    b.HasOne("classAssignment.entities.User", null)
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("classAssignment.entities.Class", b =>
                {
                    b.HasOne("classAssignment.entities.SchoolClass", "SchoolClass")
                        .WithMany()
                        .HasForeignKey("SCH_CLASS_CODE")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("SchoolClass");
                });

            modelBuilder.Entity("classAssignment.entities.ClassTeacherSubject", b =>
                {
                    b.HasOne("classAssignment.entities.Class", "Class")
                        .WithMany()
                        .HasForeignKey("ClassId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("classAssignment.entities.TeacherSubject", "TeacherSubject")
                        .WithMany()
                        .HasForeignKey("TeacherSubjectId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Class");

                    b.Navigation("TeacherSubject");
                });

            modelBuilder.Entity("classAssignment.entities.TeacherSubject", b =>
                {
                    b.HasOne("classAssignment.entities.Teacher", "Teacher")
                        .WithMany()
                        .HasForeignKey("EMP_CODE")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("classAssignment.entities.Subject", "Subject")
                        .WithMany()
                        .HasForeignKey("SUBJ_CODE")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Subject");

                    b.Navigation("Teacher");
                });

            modelBuilder.Entity("classAssignment.entities.User", b =>
                {
                    b.HasOne("classAssignment.entities.School", "School")
                        .WithMany()
                        .HasForeignKey("SCH_CODE");

                    b.Navigation("School");
                });
#pragma warning restore 612, 618
        }
    }
}
