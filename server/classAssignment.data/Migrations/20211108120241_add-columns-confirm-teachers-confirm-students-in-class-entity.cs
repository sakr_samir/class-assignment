﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace classAssignment.data.Migrations
{
    public partial class addcolumnsconfirmteachersconfirmstudentsinclassentity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "StudentsConfirmed",
                table: "Class",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "TeachersConfirmed",
                table: "Class",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "StudentsConfirmed",
                table: "Class");

            migrationBuilder.DropColumn(
                name: "TeachersConfirmed",
                table: "Class");
        }
    }
}
