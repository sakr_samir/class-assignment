﻿using classAssignment.entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace classAssignment.data
{
    public class ProjectContext : IdentityDbContext<User>
    {


        public ProjectContext(DbContextOptions options)
            : base(options)
        {
        }





        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            base.OnModelCreating(modelBuilder);
            //modelBuilder.BuildIndexesFromAnnotations();

            modelBuilder.Entity<User>(entity =>
            {
                entity.ToTable(name: "User");
            });

            //aspnetroles
            modelBuilder.Entity<IdentityRole>(entity =>
            {
                entity.ToTable(name: "UserRoles");
            });

            //aspnetuserroles
            modelBuilder.Entity<IdentityUserRole<string>>(entity =>
            {
                entity.ToTable("UserAssignedRoles");
            });

            //aspnetuserclaims
            modelBuilder.Entity<IdentityUserClaim<string>>(entity =>
            {
                entity.ToTable("UserClaims");
            });

            //aspnetuserlogins
            modelBuilder.Entity<IdentityUserLogin<string>>(entity =>
            {
                entity.ToTable("UserLogins");
            });

            //aspnetroleclaims
            modelBuilder.Entity<IdentityRoleClaim<string>>(entity =>
            {
                entity.ToTable("UserRoleClaims");
            });

            //aspnetusertokens
            modelBuilder.Entity<IdentityUserToken<string>>(entity =>
            {
                entity.ToTable("UserTokens");
            });
        }


        public DbSet<Stage> Stage { get; set; }

        public DbSet<Grade> Grade { get; set; }

        public DbSet<StageGrade> StageGrade { get; set; }

        public DbSet<Subject> Subject { get; set; }

        public DbSet<Rel> Rel { get; set; }

        public DbSet<School> School { get; set; }

        public DbSet<SchoolClass> SchoolClass { get; set; }

        public DbSet<Student> Student { get; set; }

        public DbSet<Teacher> Teacher { get; set; }

        public DbSet<Class> Class { get; set; }

        public DbSet<TeacherSubject> TeacherSubjects { get; set; }

        public DbSet<ClassTeacherSubject> ClassTeacherSubjects { get; set; }

        public DbSet<SSO_View> SSO_View { get; set; }


        public DbSet<Governorate> Governorates { get; set; }

        public DbSet<Rbt_UnAssignedTeacher> Rbt_UnAssignedTeachers { get; set; }
    }
}
