﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using classAssignment.entities.Abstraction;

namespace classAssignment.entities
{
    public class Subject
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int SUBJ_CODE { get; set; }

        public string SUBJ_NAME { get; set; }

        public int STAGE_CODE { get; set; }

        public string STAGE_NAME { get; set; }
    }
}
