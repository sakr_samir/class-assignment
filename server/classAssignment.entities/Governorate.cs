﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using classAssignment.entities.Abstraction;

namespace classAssignment.entities
{
    public class Governorate
    {
        [Key]
        public int Governorate_Code { get; set; }

        public string Governorate_Name { get; set; }


    }
}