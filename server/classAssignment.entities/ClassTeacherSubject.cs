﻿using classAssignment.entities.Abstraction;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace classAssignment.entities
{
    public class ClassTeacherSubject:BaseModel
    {
        [Key]
        public int Id { get; set; }
        [ForeignKey("TeacherSubject")]
        public int TeacherSubjectId { get; set; }
        public virtual TeacherSubject TeacherSubject { get; set; }
        [ForeignKey("Class")]
        public int ClassId { get; set; }
        public virtual Class Class { get; set; }
    }
}
