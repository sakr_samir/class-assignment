﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace classAssignment.entities
{
    public class StageGrade
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int GRADE_STAGE_CODE { get; set; }

        public int STAGE_CODE { get; set; }

        public string STAGE_NAME { get; set; }

        public int GRADE_CODE { get; set; }

        public string GRADE_Name { get; set; }
    }
}
