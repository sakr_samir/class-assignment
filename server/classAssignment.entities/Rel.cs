﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace classAssignment.entities
{
    public class Rel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int REL_CODE { get; set; }

        public string REL_Name { get; set; }
    }
}
