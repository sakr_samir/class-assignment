﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using classAssignment.entities.Abstraction;

namespace classAssignment.entities
{
    public class Rbt_UnAssignedTeacher 
    {
        [Key]
        public int Id { get; set; }

        public int Gov_Code { get; set; }
        public string Dist_Name { get; set; }
        public string Gov_Desc { get; set; }
        public string Stage_Desc { get; set; }
        public int Sch_Code { get; set; }
        public string Sch_Name { get; set; }
        public string Emp_Name { get; set; }
        public int Emp_Code { get; set; }
        public string Sub_Name { get; set; }

    }
}