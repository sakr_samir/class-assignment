﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using classAssignment.entities.Abstraction;

namespace classAssignment.entities
{
    public class School 
    {
        public int GOV_CODE { get; set; }

        public string GOV_DESC { get; set; }

        public int DIST_ID { get; set; }

        public string DIST_NAME { get; set; }

        public int STAGE_CODE { get; set; }

        public string STAGE_DESC { get; set; }

        public int REL_CODE { get; set; }

        public string REL_DESC { get; set; }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int SCH_CODE { get; set; }

        public string SCH_DESC { get; set; }



    }
}
