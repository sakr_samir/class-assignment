﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace classAssignment.entities
{
    public class SSO_View
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int BCH_CODE { get; set; }
        public int security_code { get; set; }
    }
}
