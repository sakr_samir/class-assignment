﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using classAssignment.entities.Abstraction;

namespace classAssignment.entities
{
    public class Class : BaseModel
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        [ForeignKey("SchoolClass")]
        public int SCH_CLASS_CODE { get; set; }
        public virtual SchoolClass SchoolClass { get; set; }
        public bool TeachersConfirmed { get; set; } = false;
        public bool StudentsConfirmed { get; set; } = false;

    }
}