﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using classAssignment.entities.Abstraction;

namespace classAssignment.entities
{
    public class Grade 
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int GRADE_CODE { get; set; }

        public string GRADE_Name { get; set; }

        public int GRADE_Value { get; set; }

    }
}
