﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using classAssignment.entities.Abstraction;

namespace classAssignment.entities
{
    public class Stage 
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int STAGE_CODE { get; set; }

        public string STAGE_NAME { get; set; }

    }
}
