﻿using classAssignment.entities.Abstraction;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace classAssignment.entities
{
    public class TeacherSubject:BaseModel
    {
        [Key]
        public int Id { get; set; }
        [ForeignKey("Teacher")]
        public int EMP_CODE { get; set; }
        public virtual Teacher Teacher { get; set; }
        [ForeignKey("Subject")]
        public int SUBJ_CODE { get; set; }
        public virtual Subject Subject { get; set; }
    }
}
