﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using classAssignment.entities.Abstraction;

namespace classAssignment.entities
{
    public class Student 
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int STD_CODE { get; set; }

        public string FIRST_NAME { get; set; }

        public string FATHER_NAME { get; set; }

        public string G_FATHER_NAME { get; set; }

        public string FAMILY_NAME { get; set; }

        public int SCH_CODE { get; set; }

        public string SCH_NAME { get; set; }

        public int GRADE_CODE { get; set; }

        public string GRADE_Name { get; set; }

        public int CLASS_CODE { get; set; }


    }
}
