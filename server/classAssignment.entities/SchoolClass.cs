﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using classAssignment.entities.Abstraction;

namespace classAssignment.entities
{
    public class SchoolClass
    {
        public int SCH_CODE { get; set; }

        public string SCH_NAME { get; set; }

        public int GRADE_CODE { get; set; }

        public int CLASSES_NUM { get; set; }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int SCH_CLASS_CODE { get; set; }


    }
}
