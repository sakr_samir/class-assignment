﻿using Microsoft.AspNetCore.Identity;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using classAssignment.entities.Abstraction;


namespace classAssignment.entities
{
    public class User : IdentityUser
    { 
        [Required]
        public string FullName { get; set; }

        public bool IsBlocked { get; set; } = false;

        public bool IsConfirmed { get; set; } = false;

        public string RoleName { get; set; }

        [ForeignKey("School")]
        public int? SCH_CODE { get; set; }

        public virtual School School { get; set; }

    }
}
