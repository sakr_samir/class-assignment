﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using classAssignment.entities.Abstraction;

namespace classAssignment.entities
{
    public class Teacher 
    {
        [Key]
        // uncomment if causes error in migration *****
        //[DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int EMP_CODE { get; set; }
        //can causes issue in migration,please comment ***
        public int TEACHER_CODE { get; set; }
        public string EMP_FIRST_NAME { get; set; }

        public string EMP_FATHER_NAME { get; set; }

        public string EMP_GRAND_FATHER_NAME { get; set; }

        public string EMP_FAMILY_NAME { get; set; }

        public int SCH_CODE { get; set; }

        public string SCH_NAME { get; set; }
    }
}
