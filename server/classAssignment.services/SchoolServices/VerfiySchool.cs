﻿using classAssignment.data.UnitOfWork;
using classAssignment.entities;
using classAssignment.services.SchoolServices.SchoolInterfaces;
using classAssignment.shared.ViewModels.Response;
using Microsoft.AspNetCore.Http;

namespace classAssignment.services.SchoolServices
{
    public class VerfiySchool: IVerfiySchool
    {
        private readonly IUnitOfWork _UnitofWork;
        private readonly ResultViewModel result;

        public VerfiySchool(IUnitOfWork unitofWork)
        {
            this._UnitofWork = unitofWork;
            result = new ResultViewModel();
        }
        public ResultViewModel GetSchoolByCode(int schoolCode)
        {
            if (schoolCode != 0)
            {
                var School = _UnitofWork.Repository<School>().FindBy(ww=>ww.SCH_CODE == schoolCode);
                if (School != null)
                    return result.BindResultViewModel(true, "veryfied school successfully", "تم التأكد من المرسه بنجاح",StatusCodes.Status200OK ,School);
            }
            return result;
        }
    }
}
