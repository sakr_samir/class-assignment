﻿using classAssignment.shared.ViewModels.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace classAssignment.services.SchoolServices.SchoolInterfaces
{
    public interface IVerfiySchool
    {
        ResultViewModel GetSchoolByCode(int schoolCode);
    }
}
