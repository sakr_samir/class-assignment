﻿using classAssignment.data.UnitOfWork;
using classAssignment.entities;
using classAssignment.services.ClassServices.ClassInterfaces;
using classAssignment.shared.ViewModels.Class;
using classAssignment.shared.ViewModels.Response;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace classAssignment.services.ClassServices
{
   public class SetTeachersOnSetUp : ISetTeachersOnSetUp
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ResultViewModel result;


        public SetTeachersOnSetUp(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            result = new ResultViewModel();
        }

        public ResultViewModel SetTeachers (ClassSubjectVM[] subjects)
        {
            try
            {
                if (subjects.Length > 0)
                {
                    List<ClassTeacherSubject> classTeacherSubjects = new List<ClassTeacherSubject>();
                    List<int> classIds = new List<int>();

                    foreach (ClassSubjectVM subject in subjects)
                    {
                        foreach (var teacher in subject.subjectTeachers)
                        {
                            foreach (int classId in teacher.classesIds)
                            {
                                _unitOfWork.Repository<ClassTeacherSubject>().Add(new ClassTeacherSubject
                                {
                                    ClassId = classId,
                                    TeacherSubjectId = teacher.TeacherSubjectId,
                                    CreationDate = DateTime.Now
                                });
                                classIds.Add(classId);
                            }
                        }

                    }


                    var groupedIds = classIds.GroupBy(id => id).Select(id => new { Value = id.Key, Count = id.Count() });

                    List<ClassVM> classVMs = new List<ClassVM>();

                    foreach (var classId in groupedIds)
                    {
                        classVMs.Add(new ClassVM
                        {
                            id = classId.Value,
                            totalTeachers = classId.Count,
                            name = _unitOfWork.Repository<Class>().GetAllAsQueryable().FirstOrDefault(cl=>cl.Id == classId.Value).Name
                        });
                    }
                    _unitOfWork.Repository<ClassTeacherSubject>().SaveChanges();

                    result.BindResultViewModel(true, "teachers assigned successfully", "تم تعيين المدرسين بنجاح ", StatusCodes.Status200OK, classVMs);
                }
            }
            catch (Exception ex)
            {
                result.BindResultViewModel(false, ex.Message, ex.Message, StatusCodes.Status400BadRequest, null);
            }

            return result;
        }
    }
}
