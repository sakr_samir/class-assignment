﻿using AutoMapper;
using classAssignment.data.StoredProcedures;
using classAssignment.data.UnitOfWork;
using classAssignment.entities;
using classAssignment.services.SchoolServices.SchoolInterfaces;
using classAssignment.shared.ViewModels.Class;
using classAssignment.shared.ViewModels.Response;
using Microsoft.AspNetCore.Http;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace classAssignment.services.ClassServices.ClassInterfaces
{
    public class GetGradeClassesService : IGetGradeClassesService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ResultViewModel result;
        private readonly IMapper _mapper;
        private readonly ISchoolGradesClasses _schoolGradesClasses;
        private readonly IVerfiySchool _VerfiySchool;
        

        public GetGradeClassesService(IUnitOfWork unitOfWork, IMapper mapper,
            ISchoolGradesClasses schoolGradesClasses, IVerfiySchool verfiySchool)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            result = new ResultViewModel();
            _schoolGradesClasses = schoolGradesClasses;
            _VerfiySchool = verfiySchool;
        }
        public ResultViewModel GetGradeClasses(int Sch_Code)
        {
            try
            {
                var verfiyResult = _VerfiySchool.GetSchoolByCode(Sch_Code);
                if (verfiyResult.Success)
                {
                    StageVM stageVM = _schoolGradesClasses.BindSchoolClasses(_schoolGradesClasses.GetSchoolGradesClasses(Sch_Code),Sch_Code);

                    result.BindResultViewModel(true, "Grades Collected successfully", "تم الحصول على الفصول بنجاح", StatusCodes.Status200OK, stageVM);
                }
            }
            catch (Exception ex)
            {
                result.BindResultViewModel(false, ex.Message, ex.Message, StatusCodes.Status400BadRequest, null);
            }

            return result;
        }


    }
}
