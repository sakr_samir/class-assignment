﻿using classAssignment.data.UnitOfWork;
using classAssignment.entities;
using classAssignment.services.ClassServices.ClassInterfaces;
using classAssignment.shared.ViewModels.Class;
using classAssignment.shared.ViewModels.Response;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace classAssignment.services.ClassServices
{
    public class ConfirmClassData: IConfirmClassData
    {
        private readonly IUnitOfWork _UnitofWork;
        private readonly IVerfiyClass _VerfiyClass;
        private readonly ResultViewModel result;

        public ConfirmClassData(IUnitOfWork unitofWork, IVerfiyClass verfiyClass)
        {
            this._UnitofWork = unitofWork;
            this._VerfiyClass = verfiyClass;
            result = new ResultViewModel();
        }
        public ResultViewModel ConfirmData(ConfirmClassVM model)
        {
            try
            {
                if (model.classId != 0)
                {
                    var verfiyClass = _VerfiyClass.GetClassByCode(model.classId);
                    if (verfiyClass.Success)
                    {
                        var entity = (Class)verfiyClass.ReturnObject;
                        entity.TeachersConfirmed = model.teachersConfirmed;
                        entity.StudentsConfirmed = model.studentsConfirmed;
                        if (_UnitofWork.SaveChanges())
                        {
                            return result.BindResultViewModel(true, "Data Updated Successfully", "تم تحديث البيانات بنجاح", StatusCodes.Status200OK, null);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return result;
            }
            return result;
        }
    }
}
