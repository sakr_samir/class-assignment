﻿using classAssignment.entities;
using classAssignment.shared.ViewModels.Class;
using classAssignment.shared.ViewModels.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace classAssignment.services.ClassServices.ClassInterfaces
{
   public interface ISetupClassService
    {
        public SchoolClass Vertify(int gradeCode,int Sch_code);
        public ResultViewModel CreateClasses(SetUpClassVM setUpClassVM);
    }
}
