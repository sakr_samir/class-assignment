﻿using classAssignment.entities;
using classAssignment.shared.ViewModels.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace classAssignment.services.ClassServices.ClassInterfaces
{
   public interface IGetGradeClassesService
    {
       public ResultViewModel GetGradeClasses(int Sch_Code);
    }
}
