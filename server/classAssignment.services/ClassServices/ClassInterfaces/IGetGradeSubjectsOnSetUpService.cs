﻿using classAssignment.shared.ViewModels.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace classAssignment.services.ClassServices.ClassInterfaces
{
    public interface IGetGradeSubjectsOnSetUpService
    {
        public ResultViewModel GetGradeSubjects(int Sch_Code);
    }
}
