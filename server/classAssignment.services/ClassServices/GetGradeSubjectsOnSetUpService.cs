﻿using AutoMapper;
using classAssignment.data.UnitOfWork;
using classAssignment.entities;
using classAssignment.services.ClassServices.ClassInterfaces;
using classAssignment.services.SchoolServices.SchoolInterfaces;
using classAssignment.shared.ViewModels.Class;
using classAssignment.shared.ViewModels.Response;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace classAssignment.services.ClassServices
{
    public class GetGradeSubjectsOnSetUpService: IGetGradeSubjectsOnSetUpService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ResultViewModel result;
        private readonly IMapper _mapper;
        private readonly IVerfiySchool _VerfiySchool;


        public GetGradeSubjectsOnSetUpService(IUnitOfWork unitOfWork, IMapper mapper,IVerfiySchool verfiySchool)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            result = new ResultViewModel();
            _VerfiySchool = verfiySchool;
        }
        public ResultViewModel GetGradeSubjects(int Sch_Code)
        {
            try
            {
                var verfiyResult = _VerfiySchool.GetSchoolByCode(Sch_Code);

                if (verfiyResult.Success)
                {
                    var stageCode = ((School)verfiyResult.ReturnObject).STAGE_CODE;
                    var teacherSubject = _unitOfWork.Repository<TeacherSubject>()
                        .GetAllAsQueryable()
                        .Include(ts => ts.Subject)
                        .Include(ts => ts.Teacher)
                        .Where(ts => ts.Subject.STAGE_CODE == stageCode).ToList();

                    var teacherSubjectGBySubjectId = teacherSubject
                        .GroupBy(ts => ts.SUBJ_CODE)
                        .Select(ts => ts.ToList())
                        .ToList();


                    List<ClassSubjectVM> classSubjects = new List<ClassSubjectVM>();
                    foreach (var teachersGroup in teacherSubjectGBySubjectId)
                    {
                        if (teachersGroup.Count>0)
                        {
                            ClassSubjectVM classSubjectVM = new ClassSubjectVM
                            {
                                id = teachersGroup[0].SUBJ_CODE,
                                name = teachersGroup[0].Subject.SUBJ_NAME
                            };

                            foreach (var teacher in teachersGroup)
                            {
                                classSubjectVM.subjectTeachers.Add(new TeacherVM
                                {
                                    code = teacher.Teacher.EMP_CODE,
                                    name = teacher.Teacher.EMP_FIRST_NAME + " " 
                                    + teacher.Teacher.EMP_FATHER_NAME + " " 
                                    + teacher.Teacher.EMP_GRAND_FATHER_NAME + " "
                                    + teacher.Teacher.EMP_FAMILY_NAME,
                                    TeacherSubjectId = teacher.Id
                                });
                            }

                            classSubjects.Add(classSubjectVM);
                        }
                    }
                    
                    result.BindResultViewModel(true, "Subjects Collected successfully", "تم الحصول على المواد بنجاح", StatusCodes.Status200OK, classSubjects);
                }
            }

            catch (Exception ex)
            {
                result.BindResultViewModel(false, ex.Message, ex.Message, StatusCodes.Status400BadRequest, null);
            }

            return result;
        }
    }
}
