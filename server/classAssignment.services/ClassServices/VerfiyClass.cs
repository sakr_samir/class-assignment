﻿using classAssignment.data.UnitOfWork;
using classAssignment.entities;
using classAssignment.services.ClassServices.ClassInterfaces;
using classAssignment.services.SchoolServices.SchoolInterfaces;
using classAssignment.shared.ViewModels.Response;
using Microsoft.AspNetCore.Http;

namespace classAssignment.services.ClassServices
{
    public class VerfiyClass: IVerfiyClass
    {
        private readonly IUnitOfWork _UnitofWork;
        private readonly ResultViewModel result;

        public VerfiyClass(IUnitOfWork unitofWork)
        {
            this._UnitofWork = unitofWork;
            result = new ResultViewModel();
        }
        public ResultViewModel GetClassByCode(int classId)
        {
            if (classId != 0)
            {
                var Class = _UnitofWork.Repository<Class>().FindBy(cl=>cl.Id == classId);
                if (Class != null)
                result.BindResultViewModel(true, "veryfied class successfully", "تم التأكد من الفصل بنجاح",StatusCodes.Status200OK , Class);
            }

            return result;
        }
    }
}
