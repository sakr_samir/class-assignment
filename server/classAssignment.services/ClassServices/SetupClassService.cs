﻿using AutoMapper;
using classAssignment.data.Repositories;
using classAssignment.data.UnitOfWork;
using classAssignment.entities;
using classAssignment.shared.ViewModels.Class;
using classAssignment.shared.ViewModels.Response;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace classAssignment.services.ClassServices.ClassInterfaces
{
    public class SetupClassService: ISetupClassService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ResultViewModel result;
        private readonly IMapper _mapper;
        public SetupClassService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            result = new ResultViewModel();
        }
        public SchoolClass Vertify(int gradeCode,int Sch_code)
        {
            try
            {
                SchoolClass schoolClass = _unitOfWork.Repository<SchoolClass>()
                    .GetAllAsQueryable()
                    .FirstOrDefault(sc => sc.GRADE_CODE==gradeCode&&sc.SCH_CODE == Sch_code);
                if (schoolClass == null)
                {
                    result.BindResultViewModel(false, "grade not exist in this school", "الصف غير موجودة", StatusCodes.Status404NotFound, null);
                }
                return schoolClass;
            }

            catch (Exception ex)
            {
                result.BindResultViewModel(false, ex.Message, ex.Message, StatusCodes.Status400BadRequest, null);
                return null;
            }
        }

        public ResultViewModel CreateClasses(SetUpClassVM setUpClassVM)
        {
            try
            {
                SchoolClass schoolClass = Vertify(setUpClassVM.GradeCode, setUpClassVM.SchoolCode);
                if (schoolClass != null)
                {
                    if (schoolClass.CLASSES_NUM != setUpClassVM.NumOfClasses)
                    {
                        schoolClass.CLASSES_NUM = setUpClassVM.NumOfClasses;
                        _unitOfWork.Repository<SchoolClass>().Update(schoolClass);
                        _unitOfWork.SaveChanges();
                        result.BindResultViewModel(true, "Number of classes saved successfully", "تم حفظ عدد الفصول بنجاح", StatusCodes.Status200OK, null);

                    }

                    if (!setUpClassVM.OnlySave)
                    {
                        IRepository<Class> classRepository = _unitOfWork.Repository<Class>();
                        var grade_value = _unitOfWork.Repository<Grade>().FindBy(g => g.GRADE_CODE == setUpClassVM.GradeCode).GRADE_Value.ToString() ;
                        for (int i = 1; i <= setUpClassVM.NumOfClasses; i++)
                        {
                            classRepository.Add(new Class
                            {
                                Name = i.ToString()+ " / " + grade_value ,
                                SCH_CLASS_CODE = schoolClass.SCH_CLASS_CODE,
                                CreationDate = DateTime.Now
                            });
                        }
                        _unitOfWork.SaveChanges();


                        List<ClassVM> newClasses = classRepository
                            .GetAllAsQueryable()
                            .Where(cl => cl.SCH_CLASS_CODE == schoolClass.SCH_CLASS_CODE)
                            .Select(cl => _mapper.Map<Class, ClassVM>(cl))
                            .AsEnumerable()
                            .OrderBy(cl => int.Parse(cl.name.Replace(" / ", ""))).ToList();

                        result.BindResultViewModel(true, "classes created successfully", "تم إضافة الفصول بنجاح", StatusCodes.Status200OK, newClasses);
                    }

                    

                    
                }
                
            }

            catch (Exception ex)
            {
                result.BindResultViewModel(false, ex.Message, ex.Message, StatusCodes.Status400BadRequest, null);
            }
            
            return result;
        }


    }
}
