﻿using classAssignment.data.UnitOfWork;
using classAssignment.entities;
using classAssignment.services.ClassServices.ClassInterfaces;
using classAssignment.shared.ViewModels.Class;
using classAssignment.shared.ViewModels.Response;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace classAssignment.services.ClassServices
{
    public class UpdateClassSubjectTeachers: IUpdateClassSubjectTeachers
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ResultViewModel result;
        private readonly IVerfiyClass _VerfiyClass;
        public UpdateClassSubjectTeachers(IVerfiyClass verfiyClass, IUnitOfWork unitOfWork)
        {
            _VerfiyClass = verfiyClass;
            _unitOfWork = unitOfWork;
            result = new ResultViewModel();
        }
        public ResultViewModel UpdateTeachers(UpdateTeacherViewModel model)
        {
            try
            {
                if (model.ClassId != 0)
                {
                    var verfiyClass = _VerfiyClass.GetClassByCode(model.ClassId);
                    if (verfiyClass.Success)
                    {
                        if (model.RemoveTeachers.Count() > 0)
                        {
                            var ClassTeacherSubjects = _unitOfWork.Repository<ClassTeacherSubject>().GetAllAsQueryable()
                                .Where(cts => cts.ClassId == model.ClassId && model.RemoveTeachers.Select(ww => ww.TeacherSubjectId).Contains(cts.TeacherSubjectId));
                            foreach (var entity in ClassTeacherSubjects)
                            {
                                _unitOfWork.Repository<ClassTeacherSubject>().Delete(entity);
                            }
                        }
                        if (model.AddTeachers.Count() > 0)
                        {
                            foreach (var entity in model.AddTeachers)
                            {
                                _unitOfWork.Repository<ClassTeacherSubject>().Add(new ClassTeacherSubject()
                                {
                                    ClassId = model.ClassId,
                                    TeacherSubjectId = entity.TeacherSubjectId,
                                    CreationDate = DateTime.Now
                                });
                            }
                        }
                        if (_unitOfWork.SaveChanges())
                        {
                            return result.BindResultViewModel(true, "Data Updated Successfully", "تم تحديث البيانات بنجاح", StatusCodes.Status200OK, null);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return result;
            }
            return result;
        }
    }
}
