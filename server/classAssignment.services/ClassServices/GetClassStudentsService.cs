﻿using AutoMapper;
using classAssignment.data.UnitOfWork;
using classAssignment.entities;
using classAssignment.services.ClassServices.ClassInterfaces;
using classAssignment.services.SchoolServices.SchoolInterfaces;
using classAssignment.shared.ViewModels.Class;
using classAssignment.shared.ViewModels.Response;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace classAssignment.services.ClassServices
{
    public class GetClassStudentsService: IGetClassStudentsService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ResultViewModel result;
        private readonly IVerfiyClass _VerfiyClass;
        public GetClassStudentsService( IVerfiyClass verfiyClass, IUnitOfWork unitOfWork)
        {
            _VerfiyClass = verfiyClass;
            _unitOfWork = unitOfWork;
            result = new ResultViewModel();
        }
        public ResultViewModel GetStudents(int classId)
        {
            try
            {
                var verfiyClassResult = _VerfiyClass.GetClassByCode(classId);
                if (verfiyClassResult.Success)
                {

                    List<ClassStudentVM> studentVMs = _unitOfWork.Repository<Student>()
                        .GetAllAsQueryable()
                        .Where(st => st.CLASS_CODE == classId)
                        .Select(st => new ClassStudentVM
                        {
                            code = st.STD_CODE,
                            name = st.FIRST_NAME + " " + st.FATHER_NAME + " " + st.G_FATHER_NAME + " "+ st.FAMILY_NAME,
                        }
                        ).ToList();
                    result.BindResultViewModel(true, "students Collected successfully", "تم الحصول على الطلاب بنجاح", StatusCodes.Status200OK, studentVMs);
    }
            }
            catch (Exception ex)
            {
                result.BindResultViewModel(false, ex.Message, ex.Message, StatusCodes.Status400BadRequest, null);
            }

            return result;
        }

    }
}
