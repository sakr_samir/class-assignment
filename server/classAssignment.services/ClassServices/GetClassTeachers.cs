﻿using AutoMapper;
using classAssignment.data.UnitOfWork;
using classAssignment.entities;
using classAssignment.services.ClassServices.ClassInterfaces;
using classAssignment.services.SchoolServices.SchoolInterfaces;
using classAssignment.shared.ViewModels.Class;
using classAssignment.shared.ViewModels.Response;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace classAssignment.services.ClassServices
{
    public class GetClassTeachers : IGetClassTeachers
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ResultViewModel result;
        private readonly IVerfiyClass _VerfiyClass;
        public GetClassTeachers(IVerfiyClass verfiyClass, IUnitOfWork unitOfWork)
        {
            _VerfiyClass = verfiyClass;
            _unitOfWork = unitOfWork;
            result = new ResultViewModel();
        }
        public ResultViewModel GetTeachers(int ClassId, int SchoolCode)
        {
            try
            {
                var verfiyClassResult = _VerfiyClass.GetClassByCode(ClassId);
                if (verfiyClassResult.Success && SchoolCode != 0)
                {
                    var classObj=(Class)verfiyClassResult.ReturnObject;
                    var classVM = new ClassDetailsVM()
                    {
                        id = classObj.Id,
                        name=classObj.Name,
                        teachersConfirmed=classObj.TeachersConfirmed,
                        studentsConfirmed=classObj.StudentsConfirmed
                    };
                    List<GetClassSubjectViewModel> Teachers = _unitOfWork.Repository<TeacherSubject>().GetAllAsQueryable()
                    .Include(ts => ts.Teacher).Include(ts => ts.Subject)
                    .Where(ts => ts.Teacher.SCH_CODE == SchoolCode)
                    .ToList()
                    .GroupBy(ts => ts.SUBJ_CODE)
                    .Select(ts => new GetClassSubjectViewModel
                    {
                        id = ts.First().Subject.SUBJ_CODE,
                        name = ts.First().Subject.SUBJ_NAME,
                        subjectTeachers = ts.Select(ts => new GetTeacherViewModel()
                        {
                            TeacherSubjectId = ts.Id,
                            TeacherCode=ts.Teacher.TEACHER_CODE,
                            name = ts.Teacher.EMP_FIRST_NAME + " " + ts.Teacher.EMP_FATHER_NAME + " " + ts.Teacher.EMP_GRAND_FATHER_NAME + " " + ts.Teacher.EMP_FAMILY_NAME
                        }).ToList(),
                        selectedTeachers = _unitOfWork.Repository<ClassTeacherSubject>().GetAllAsQueryable()
                        .Include(cts => cts.TeacherSubject).ThenInclude(ts => ts.Teacher)
                        .Where(cts => cts.ClassId == ClassId && ts.Select(ww=>ww.Id).Contains(cts.TeacherSubjectId))
                        .Select(cts => new GetTeacherViewModel()
                        {
                            TeacherSubjectId = cts.TeacherSubject.Id,
                            TeacherCode=cts.TeacherSubject.Teacher.TEACHER_CODE,
                            name = cts.TeacherSubject.Teacher.EMP_FIRST_NAME + " " + cts.TeacherSubject.Teacher.EMP_FATHER_NAME + " " + cts.TeacherSubject.Teacher.EMP_GRAND_FATHER_NAME + " " + cts.TeacherSubject.Teacher.EMP_FAMILY_NAME
                        }).ToList()

                    }).ToList();
                    return result.BindResultViewModel(true, "teachers Collected successfully", "تم الحصول على المدرسيين بنجاح", StatusCodes.Status200OK, new { teachersList= Teachers,classDatails= classVM });
                }
            }
            catch (Exception ex)
            {
                return result;
            }

            return result;
        }

    }
}
