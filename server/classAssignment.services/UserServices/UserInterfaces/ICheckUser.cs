﻿using System;
using classAssignment.entities;

namespace classAssignment.services.UserServices.UserInterfaces
{
    public interface ICheckUser
    {
        User UserExistById(string userId);
        User UserExistByEmail(string email);
    }
}
