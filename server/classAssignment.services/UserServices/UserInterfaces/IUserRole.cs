﻿using System;
using classAssignment.entities;
using Microsoft.AspNetCore.Identity;

namespace classAssignment.services.UserServices.UserInterfaces
{
    public interface IUserRole
    {
        string GetUserRole(UserManager<User> userManager, User user);
        bool ValidateUserRole(string roleName);
        IdentityResult SetUserRole(string roleName, UserManager<User> userManager, User user);

    }
}
