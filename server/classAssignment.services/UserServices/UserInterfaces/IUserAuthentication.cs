﻿using System;
using classAssignment.shared.ViewModels.Response;
using classAssignment.shared.ViewModels.User;

namespace classAssignment.services.UserServices.UserInterfaces
{
    public interface IUserAuthentication
    {
        ResultViewModel Login(LoginVM model);
    }
}
