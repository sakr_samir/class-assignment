﻿using classAssignment.entities;
using classAssignment.shared.ViewModels.Response;
using classAssignment.shared.ViewModels.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace classAssignment.services.UserServices.UserInterfaces
{
   public interface ISSOLoginService
    {
        public ResultViewModel SSOLogin(SSOLoginVM loginVM);

    }
}
