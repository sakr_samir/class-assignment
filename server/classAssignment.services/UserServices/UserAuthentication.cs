﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using AutoMapper;
using classAssignment.data.UnitOfWork;
using classAssignment.entities;
using classAssignment.services.UserServices.UserInterfaces;
using classAssignment.shared.ViewModels.JWT;
using classAssignment.shared.ViewModels.Response;
using classAssignment.shared.ViewModels.User;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace classAssignment.services.UserServices
{
    public class UserAuthentication : IUserAuthentication
    {
        private readonly IUnitOfWork _UnitOfWork;
        private readonly UserManager<User> _userManager;
        private readonly ICheckUser _checkUserService;
        private readonly IOptions<AppSettings> _appSettings;
        private readonly ResultViewModel result;
        private readonly IUserRole _userRole;
        private readonly IMapper _mapper;
        public UserAuthentication(IUnitOfWork unitOfWork, IMapper mapper, ICheckUser checkUserService,UserManager<User> userManager, IOptions<AppSettings> appSettings, IUserRole userRole)
        {
            _mapper = mapper;
            _UnitOfWork = unitOfWork;
            _userManager = userManager;
            _appSettings = appSettings;
            _userRole = userRole;
            _checkUserService = checkUserService;
            result = new ResultViewModel();
        }
        public User CheckUserExistByEmail(LoginVM model)
        {
            try
            {

                User user = _checkUserService.UserExistByEmail(model.Email);

                if (user == null)
                {
                    result.BindResultViewModel(false, "Email not exist", "البريد الالكتروني غير موجود", StatusCodes.Status404NotFound, null);

                }

                return user;
            }
            catch (Exception ex)
            {
                result.BindResultViewModel(false, ex.Message, ex.Message, StatusCodes.Status400BadRequest, null);
                return null;
            }
        }
        private AuthenticatedUserVM BindSchoolData(School school, AuthenticatedUserVM authenticatedUserVM)
        {
            
                authenticatedUserVM.UserSchoolData = _mapper.Map<UserSchoolVM>(school);
           
            return authenticatedUserVM;
        }
        public ResultViewModel Login(LoginVM model)
        {
            try
            {
                User user = CheckUserExistByEmail(model);
                if (user != null)
                {
                    if (_userManager.CheckPasswordAsync(user, model.Password).GetAwaiter().GetResult())
                    {
                        string userRole = _userRole.GetUserRole(_userManager, user);
                        string JWTToken = GenrateToken(user, userRole);

                        var returnObject = _mapper.Map<AuthenticatedUserVM>(user);
                        returnObject.Token = JWTToken;
                        if(user.School != null)
                        {
                            BindSchoolData(user.School, returnObject);
                        }
                        if (_UnitOfWork.SaveChanges())
                        {
                            return result.BindResultViewModel(true, "Logged in successfully", "تم الدخول بنجاح", StatusCodes.Status200OK, returnObject);
                        }
                        else
                        {
                            return result.BindResultViewModel(false, "Couldn't generate refresh token", "فشل تكوين الرمز", StatusCodes.Status401Unauthorized, null);

                        }
                    }
                    else
                    {
                        return result.BindResultViewModel(false, "Password not correct", "كلمة المرور غير صحيحة", StatusCodes.Status401Unauthorized, null);

                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                result.BindResultViewModel(false, ex.Message, ex.Message, StatusCodes.Status400BadRequest, null);
                return result;
            }
        }
        private string GenrateToken(User user, string userRole)
        {
            try
            {
                var claims = GenerateClaims(user, userRole);

                var signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(_appSettings.Value.Secret));
                int IntTokenDuration = 6;
                DateTime tokenExpiryDateTime = DateTime.Now.AddHours(IntTokenDuration);
                var token = new JwtSecurityToken(
                    claims: claims,
                    expires: tokenExpiryDateTime,
                    signingCredentials: new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256)
                    );
                var serializedToken = new JwtSecurityTokenHandler().WriteToken(token);
                return serializedToken;
            }
            catch (Exception ex)
            {
                result.BindResultViewModel(false, ex.Message, ex.Message, StatusCodes.Status400BadRequest, null);
                return null;
            }
        }
        private List<Claim> GenerateClaims(User user, string userRole)
        {
            try
            {
                var claims = new List<Claim>
            {
                new Claim("id", user.Id.ToString()),
                new Claim(ClaimTypes.Name, user.FullName),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim("roleName", userRole)
            };
                return claims;
            }
            catch (Exception ex)
            {
                result.BindResultViewModel(false, ex.Message, ex.Message, StatusCodes.Status400BadRequest, null);
                return null;
            }
        }

    }
}
