﻿using System;
using System.Linq;
using classAssignment.data.UnitOfWork;
using classAssignment.entities;
using classAssignment.services.UserServices.UserInterfaces;
using Microsoft.EntityFrameworkCore;

namespace user.management.server.services.UserServices
{
    public class CheckUser : ICheckUser
    {
        private readonly IUnitOfWork _unitOfWork;
        public CheckUser(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public User UserExistById(string userId)
        {
            try
            {
                User user = _unitOfWork._UserRepository.Get(userId);
                if (user != null)
                    return user;


                return null;
            }
            catch (Exception)
            {
                return null;
            }

        }
        public User UserExistByEmail(string email)
        {
            try
            {
                User user = _unitOfWork._UserRepository.GetAllAsQueryable().Where(user => user.Email.ToLower().Equals(email.ToLower()) && user.IsBlocked == false).Include(s=>s.School).FirstOrDefault();
                if (user != null)
                    return user;


                return null;
            }
            catch (Exception ex)
            {
                return null;
            }

        }
   }
}
