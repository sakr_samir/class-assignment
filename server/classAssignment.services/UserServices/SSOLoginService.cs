﻿using classAssignment.data.UnitOfWork;
using classAssignment.entities;
using classAssignment.services.SchoolServices.SchoolInterfaces;
using classAssignment.services.UserServices.UserInterfaces;
using classAssignment.shared.ViewModels.Response;
using classAssignment.shared.ViewModels.User;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace classAssignment.services.UserServices
{
    public class SSOLoginService : ISSOLoginService
    {
        private readonly IUnitOfWork _unitOfWork;
        private ResultViewModel result;
        private readonly IUserAuthentication _userAuthentication;
        public SSOLoginService(IUnitOfWork unitOfWork, IUserAuthentication userAuthentication)
        {
            _unitOfWork = unitOfWork;
            result = new ResultViewModel();
            _userAuthentication = userAuthentication;
        }
        public ResultViewModel SSOLogin(SSOLoginVM loginVM)
        {
            var verfiyResult = this.VerfiySSO(loginVM);
            if (verfiyResult.Success)
                return _userAuthentication.Login(new LoginVM { Email =((User)verfiyResult.ReturnObject).Email, Password = "No@12345" });
            return verfiyResult;
        }

        private ResultViewModel VerfiySSO(SSOLoginVM loginVM)
        {
            var ssoView = _unitOfWork.Repository<SSO_View>()
                  .GetAllAsQueryable()
                  .FirstOrDefault(ss => ss.BCH_CODE == loginVM.BCH_CODE &&
                                  ss.security_code == loginVM.security_code
                                 );
            if (ssoView != null)
            {
                var user = _unitOfWork.Repository<User>().GetAllAsQueryable().FirstOrDefault(user => user.SCH_CODE == loginVM.BCH_CODE);
                if (user != null)
                    return result.BindResultViewModel(true, "User Found Succesfully", "تم الحصول على البيانات", StatusCodes.Status200OK, user);
            }
            return result;
        }
    }
}
