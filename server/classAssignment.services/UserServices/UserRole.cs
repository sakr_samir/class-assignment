﻿using System;
using System.Linq;
using classAssignment.entities;
using classAssignment.services.UserServices.UserInterfaces;
using Microsoft.AspNetCore.Identity;

namespace classAssignment.services.UserServices
{
    public class UserRole : IUserRole
    {
        private readonly RoleManager<IdentityRole> _roleManager;
        public UserRole(RoleManager<IdentityRole> roleManager)
        {
            _roleManager = roleManager;
        }
        public string GetUserRole(UserManager<User> userManager, User user)
        {
            try
            {
                var adminRoles = userManager.GetRolesAsync(user).GetAwaiter().GetResult();

                if (adminRoles.Count() > 0)
                    return adminRoles[0];


                return "";
            }
            catch (Exception)
            {
                return "";
            }


        }
        public bool ValidateUserRole(string roleName)
        {
            try
            {
                return _roleManager.RoleExistsAsync(roleName).GetAwaiter().GetResult();

            }
            catch (Exception)
            {
                return false;
            }
        }
        public IdentityResult SetUserRole(string roleName, UserManager<User> userManager, User user)
        {
            try
            {

                return userManager.AddToRoleAsync(user, roleName).GetAwaiter().GetResult();
            }
            catch (Exception)
            {
                return null;
            }

        }
    }
}
