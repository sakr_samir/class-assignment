﻿using classAssignment.data.UnitOfWork;
using classAssignment.shared.ViewModels.Response;
using classAssignment.shared.ViewModels.Student;
using System;
using System.Collections.Generic;
using System.Linq;
using classAssignment.entities;
using System.Threading.Tasks;
using classAssignment.services.Helpers;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Http;
using classAssignment.services.StudentServises.StudentInterfaces;

namespace classAssignment.services.StudentServises
{
    public class GetGradeStudents : IGetGradeStudents
    {
        private readonly IUnitOfWork _UnitOfWork;
        private readonly ResultViewModel result;

        public GetGradeStudents(IUnitOfWork unitOfWork)
        {
            this._UnitOfWork = unitOfWork;
            result = new ResultViewModel();
        }
        public ResultViewModel GetStudents(SearchStudentViewModel model)
        {
            try
            {
                IQueryable<StudentViewModel> students = _UnitOfWork.Repository<Student>().GetAllAsQueryable()
                    .Where(ww => ww.SCH_CODE == model.SchoolCode && ww.GRADE_CODE == model.GradeCode)
                    .OrderBy(ww => ww.FIRST_NAME)
                    .Select(ww => new StudentViewModel()
                    {
                        StudentCode = ww.STD_CODE,
                        ClassCode = ww.CLASS_CODE,
                        StudentName = ww.FIRST_NAME + " " + ww.FATHER_NAME + " " + ww.G_FATHER_NAME + " " + ww.FATHER_NAME
                    });


                // filter assign
                students = String.IsNullOrEmpty(model.assign) ? students : (model.assign == "true" ? students.Where(x => x.ClassCode != 0) : students.Where(x => x.ClassCode == 0));
                students = Searching(students, model);

                List<StudentViewModel> studentsList = PaginatedList<StudentViewModel>.CreateAsync(students.AsNoTracking(), model.PageIndex == null || model.PageIndex == 0 ? 1 : (int)model.PageIndex, model.PageLimit == null || model.PageLimit == 0 ? 10 : (int)model.PageLimit).GetAwaiter().GetResult();

                StudentDataViewModel returnedObj = new StudentDataViewModel();
                returnedObj.Students = studentsList;
                if (model.PageLimit == null || model.PageLimit == 0)
                {
                    returnedObj.PagesNumber = (int)Math.Ceiling(students.Count() / 10.0);
                }
                else
                {
                    returnedObj.PagesNumber = (int)Math.Ceiling(students.Count() / (double)model.PageLimit);
                }
                return result.BindResultViewModel(true, "Data Retrived Successuffly", "تم استرجاع البيانات بنجاح", StatusCodes.Status200OK, returnedObj);
            }
            catch (Exception ex)
            {
                return result;
            }
        }
        private IQueryable<StudentViewModel> Searching(IQueryable<StudentViewModel> students, SearchStudentViewModel model)
        {
            if (model.StudentCode != null && model.StudentCode != 0)
            {
                students = students.Where(ent => ent.StudentCode.ToString().Contains(model.StudentCode.ToString()));
            }
            if (!String.IsNullOrEmpty(model.StudentName))
            {
                students = students.Where(ent => ent.StudentName.ToLower().Contains(model.StudentName.ToLower()));
            }
            if (!String.IsNullOrEmpty(model.ClassName) && model.ClassIds != null)
            {
                students = students.Where(ent => model.ClassIds.Contains(ent.ClassCode));
            }
            return students;
        }

    }
}
