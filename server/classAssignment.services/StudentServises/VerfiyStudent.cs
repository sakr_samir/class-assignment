﻿using classAssignment.data.UnitOfWork;
using classAssignment.entities;
using classAssignment.services.StudentServises.StudentInterfaces;
using classAssignment.shared.ViewModels.Response;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace classAssignment.services.StudentServises
{
    public class VerfiyStudent: IVerfiyStudent
    {
        private readonly IUnitOfWork _UnitOfWork;
        private readonly ResultViewModel result;

        public VerfiyStudent(IUnitOfWork unitOfWork)
        {
            this._UnitOfWork = unitOfWork;
            result = new ResultViewModel();
        }
        public ResultViewModel GetStudentByCode(int StudentCode)
        {
            if(StudentCode != 0)
            {
                var student = _UnitOfWork.Repository<Student>().FindBy(ww => ww.STD_CODE == StudentCode);
                if (student != null)
                    return result.BindResultViewModel(true, "Student Found Successfully","تم الحصول على الطالب بنجاح",StatusCodes.Status200OK,student);
            }
            return result;
        }
    }
}
