﻿using classAssignment.data.UnitOfWork;
using classAssignment.services.ClassServices.ClassInterfaces;
using classAssignment.services.SchoolServices.SchoolInterfaces;
using classAssignment.services.StudentServises.StudentInterfaces;
using classAssignment.shared.ViewModels.Class;
using classAssignment.shared.ViewModels.Response;
using classAssignment.shared.ViewModels.Student;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace classAssignment.services.StudentServises
{
    public class GetSchoolGrades: IGetSchoolGrades
    {
        private readonly IVerfiySchool _VerfiySchool;
        private readonly IGetGradeClassesService _GetGradeClassesService;
        private readonly IGetGradeStudents _GetGradeStudents;
        private readonly ResultViewModel result;

        public GetSchoolGrades(IVerfiySchool verfiySchool,
            IGetGradeClassesService getGradeClassesService, IGetGradeStudents getGradeStudents)
        {
            this._VerfiySchool = verfiySchool;
            this._GetGradeClassesService = getGradeClassesService;
            this._GetGradeStudents = getGradeStudents;
            result = new ResultViewModel();
        }
        public ResultViewModel GetGrades(int SchoolCode,int selectedGradeCode, string assign)
        {
            var verfiySchool = _VerfiySchool.GetSchoolByCode(SchoolCode);
            if (verfiySchool.Success)
            {
                GetSchoolGradeViewModel returnObj = new GetSchoolGradeViewModel();
                var getDradesResult = _GetGradeClassesService.GetGradeClasses(SchoolCode);
                if (getDradesResult.Success)
                {
                    var stageVM = (StageVM)getDradesResult.ReturnObject;
                    
                    
                        returnObj.SchoolGrades = stageVM.Grades.Select(ww => new SchoolGradeViewModel()
                        {
                            GradeCode = ww.Code,
                            GradeName = ww.name,
                            Classes = ww.classes.Select(cl => new ClassViewModel
                            {
                                ClassId = cl.id,
                                ClassName = cl.name.Replace(" / ","/")
                            }).ToList()
                        }).ToList();
                    
                    
                }
                ResultViewModel getStudentsResult;
                if(selectedGradeCode!=0)
                {
                    getStudentsResult = _GetGradeStudents.GetStudents(new SearchStudentViewModel()
                    {
                        SchoolCode = SchoolCode,
                        GradeCode = selectedGradeCode,
                        assign=assign
                    });
                }
                else
                {
                    getStudentsResult = _GetGradeStudents.GetStudents(new SearchStudentViewModel()
                    {
                        SchoolCode = SchoolCode,
                        GradeCode = returnObj.SchoolGrades[0].GradeCode,
                        assign = assign
                    });
                }
                if (getStudentsResult.Success)
                {
                    var StudtentData = (StudentDataViewModel)getStudentsResult.ReturnObject;
                    returnObj.Students = StudtentData.Students;
                    returnObj.PagesNumber = StudtentData.PagesNumber;
                }
                if (returnObj.SchoolGrades.Any())
                    return result.BindResultViewModel(true, "Data retrived successfully", "تم محديث البيانات بنجاح", StatusCodes.Status200OK, returnObj);
            }
            return result;
        }
    }
}
