﻿using classAssignment.shared.ViewModels.Response;
using classAssignment.shared.ViewModels.Student;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace classAssignment.services.StudentServises.StudentInterfaces
{
    public interface IAssignClass
    {
        ResultViewModel AssignStudent(AssignClassViewModel model);
    }
}
