﻿using classAssignment.data.UnitOfWork;
using classAssignment.entities;
using classAssignment.services.ClassServices.ClassInterfaces;
using classAssignment.services.StudentServises.StudentInterfaces;
using classAssignment.shared.ViewModels.Response;
using classAssignment.shared.ViewModels.Student;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace classAssignment.services.StudentServises
{
    public class AssignClass: IAssignClass
    {
        private readonly IUnitOfWork _UnitOfWork;
        private readonly IVerfiyStudent _VerfiyStudent;
        private readonly IVerfiyClass _VerfiyClass;
        private readonly ResultViewModel result;

        public AssignClass(IUnitOfWork unitOfWork, IVerfiyStudent verfiyStudent, IVerfiyClass verfiyClass)
        {
            this._UnitOfWork = unitOfWork;
            _VerfiyStudent = verfiyStudent;
            this._VerfiyClass = verfiyClass;
            result = new ResultViewModel();
        }
        public ResultViewModel AssignStudent(AssignClassViewModel model)
        {
            var verfiyStudent = _VerfiyStudent.GetStudentByCode(model.StudentCode);
            if (verfiyStudent.Success)
            {
                var verfiyClass = _VerfiyClass.GetClassByCode(model.ClassCode);
                if (verfiyClass.Success)
                {
                    var student = (Student)verfiyStudent.ReturnObject;
                    student.CLASS_CODE = model.ClassCode;
                    if (_UnitOfWork.SaveChanges())
                        return result.BindResultViewModel(true, "Student Updated Successfully", "تم تحديث البيانات بنجاح", StatusCodes.Status200OK, null);
                }
            }
            return result;
        }
    }
}
