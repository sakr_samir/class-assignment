﻿using classAssignment.shared.ViewModels.Response;
using classAssignment.shared.ViewModels.Teacher;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace classAssignment.services.TeacherServices.TeacherInterfaces
{
    public interface ITeachersFullReport
    {
        ResultViewModel GetReportData(int govId );
        ResultViewModel GetGovsData();
    }
}
