﻿using classAssignment.data.StoredProcedures;
using classAssignment.data.UnitOfWork;
using classAssignment.entities;
using classAssignment.services.TeacherServices.TeacherInterfaces;
using classAssignment.shared.ViewModels.Response;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace classAssignment.services.TeacherServices
{
    public class TeachersFullReport : ITeachersFullReport
    {
        private readonly ResultViewModel result;
        private readonly IUnitOfWork _unitOfWork;

        public TeachersFullReport(IUnitOfWork unitOfWork)
        {
            result = new ResultViewModel();
            _unitOfWork = unitOfWork;
        }
        public ResultViewModel GetReportData(int govId)
        {
            try
            {
                List<object> reportData = new List<object>();
                List<object> totalNumbers = new List<object>();

                if (govId != 0)
                {
                    var seletedGovData = _unitOfWork.Repository<Rbt_UnAssignedTeacher>().GetAllAsQueryable()
                        .Where(x => x.Gov_Code == govId)
                        .OrderBy(t => t.Dist_Name)
                        .ThenBy(x => x.Stage_Desc)
                        .ThenBy(x => x.Sch_Name)
                        .AsNoTracking()
                        .ToList();

                    int selectedTotalNumTeachers = seletedGovData
                        .Where(t => t.Gov_Code == govId)
                        .Select(t => t.Emp_Code)
                        .Distinct()
                        .Count();
                    int selectedTotalNumSchools = seletedGovData
                        .Where(t => t.Gov_Code == govId)
                        .Select(t => t.Sch_Code)
                        .Distinct()
                        .Count();
                    string GovName;
                    if (seletedGovData.Count > 0)
                    { 
                        GovName = seletedGovData[0].Gov_Desc; 
                    }
                    else
                    {
                        GovName = _unitOfWork.Repository<Governorate>().GetAllAsQueryable().FirstOrDefault(g => g.Governorate_Code == govId).Governorate_Name;
                    }
                    reportData.Add(
                        new 
                        {
                            GovName = GovName,
                            Teachers = seletedGovData.Select(t=> new object[] {t.Dist_Name,t.Stage_Desc,t.Sch_Code,t.Sch_Name,t.Emp_Code,t.Emp_Name,t.Sub_Name}),
                            totalNumbers = new object[] { selectedTotalNumSchools, selectedTotalNumTeachers }
                        });

                    totalNumbers.Add(new object[] { GovName, selectedTotalNumSchools, selectedTotalNumTeachers });
                            
                }
                else
                {
                    
                    var teachersGroups = _unitOfWork.Repository<Rbt_UnAssignedTeacher>().GetAllAsQueryable().AsNoTracking().AsEnumerable()
                        .GroupBy(t => t.Gov_Code).ToList();



                    foreach (var group in teachersGroups)
                    {
                        int selectedTotalNumTeachers = group.Select(s => s.Emp_Code).Distinct().Count();
                        int selectedTotalNumSchools = group.Select(x=>x.Sch_Code).Distinct().Count();
                        List<Rbt_UnAssignedTeacher> selectedTeachers = group.OrderBy(t => t.Dist_Name).ThenBy(x => x.Stage_Desc).ThenBy(x => x.Sch_Name).ToList();
                        string GovName = selectedTeachers[0].Gov_Desc;

                        reportData.Add(
                                        new
                                        {
                                            GovName = GovName,
                                            Teachers = selectedTeachers.Select(t => new object[] { t.Dist_Name, t.Stage_Desc, t.Sch_Code, t.Sch_Name, t.Emp_Code, t.Emp_Name, t.Sub_Name }),
                                            totalNumbers = new object[] { selectedTotalNumSchools, selectedTotalNumTeachers }
                                        });

                        totalNumbers.Add(new object[] { GovName, selectedTotalNumSchools, selectedTotalNumTeachers });
                                       
                    }
                }

                
                return result.BindResultViewModel(true, "Data Retrived successfully", "تم تحديث بيانات التقرير  بنجاح", StatusCodes.Status200OK,
                    new
                    {
                        reportData = reportData,
                        totalNumbers = totalNumbers,
                    } );
            }
            catch (Exception)
            {
                return result;
            }
            
        }
        public ResultViewModel GetGovsData()
        {
            try
            {
                var govsData = _unitOfWork.Repository<Governorate>().GetAllAsQueryable().OrderBy(gov=>gov.Governorate_Name).Select(gov => new
                {
                    Id = gov.Governorate_Code,
                    Name = gov.Governorate_Name
                });
                return result.BindResultViewModel(true, "Data Retrived successfully", "تم تحديث بيانات  بنجاح", StatusCodes.Status200OK, govsData);
            }
            catch (Exception)
            {
                return result;
            }

        }
    }
}
