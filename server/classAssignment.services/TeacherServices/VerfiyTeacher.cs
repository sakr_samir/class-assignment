﻿using classAssignment.data.UnitOfWork;
using classAssignment.entities;
using classAssignment.services.TeacherServices.TeacherInterfaces;
using classAssignment.shared.ViewModels.Response;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

namespace classAssignment.services.TeacherServices
{
    public class VerfiyTeacher: IVerfiyTeacher
    {
        private readonly IUnitOfWork _UnitofWork;
        private readonly ResultViewModel result;

        public VerfiyTeacher(IUnitOfWork unitofWork)
        {
            this._UnitofWork = unitofWork;
            result = new ResultViewModel();
        }
        public ResultViewModel GetTeacherByCode(int EmpCode)
        {
            if (EmpCode != 0)
            {
                var teacher = _UnitofWork.Repository<Teacher>().FindBy(ww=>ww.EMP_CODE == EmpCode);
                if (teacher != null)
                    return result.BindResultViewModel(true, "veryfied teacher successfully", "تم التأكد من المدرس بنجاح",StatusCodes.Status200OK , teacher);
            }
            return result;
        }
    }
}
