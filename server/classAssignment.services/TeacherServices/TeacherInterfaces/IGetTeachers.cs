﻿using classAssignment.shared.ViewModels.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace classAssignment.services.TeacherServices.TeacherInterfaces
{
    public interface IGetTeachers
    {
        ResultViewModel GetTeachersData(int schoolCode, string unassign);
    }
}
