﻿using classAssignment.shared.ViewModels.Response;
using classAssignment.shared.ViewModels.Teacher;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace classAssignment.services.TeacherServices.TeacherInterfaces
{
    public interface IUpdateSubjetcs
    {
        ResultViewModel UpdateTeacherSubjects(UpdateSubjectsVM model);
    }
}
