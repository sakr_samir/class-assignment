﻿using classAssignment.data.UnitOfWork;
using classAssignment.entities;
using classAssignment.services.TeacherServices.TeacherInterfaces;
using classAssignment.shared.ViewModels.Response;
using classAssignment.shared.ViewModels.Teacher;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace classAssignment.services.TeacherServices
{
    public class UnAssignSubjects : IUnAssignSubjects
    {
        private readonly IUnitOfWork _UnitofWork;
        private readonly ResultViewModel result;

        public UnAssignSubjects(IUnitOfWork unitofWork)
        {
            this._UnitofWork = unitofWork;
            result = new ResultViewModel();
        }
        public ResultViewModel UnAssignTeacherSubject(UnAssignSubjectViewModel model)
        {
            try
            {
                if (model.Code != 0 && model.SubjectesToRemove.Count() > 0)
                {
                    var TeacherSubjects = _UnitofWork.Repository<TeacherSubject>().GetAllAsQueryable()
                        .Where(ts => ts.EMP_CODE == model.Code && model.SubjectesToRemove.Select(sub => sub.Id).Contains(ts.SUBJ_CODE))
                        .ToList();
                    if (TeacherSubjects.Any())
                    {
                       // var classes = _UnitofWork.Repository<ClassTeacherSubject>().GetAllAsQueryable()
                       //.Where(cts => TeacherSubjects.Select(ts => ts.Id).Contains(cts.TeacherSubjectId))
                       //.ToList();
                       // if (classes.Any())
                       // {
                       //     foreach (var classObj in classes)
                       //     {
                       //         _UnitofWork.Repository<ClassTeacherSubject>().Delete(classObj);
                       //     }
                       //     _UnitofWork.SaveChanges();
                       // }
                        foreach (var teacherSubject in TeacherSubjects)
                        {
                            _UnitofWork.Repository<TeacherSubject>().Delete(teacherSubject);
                        }
                        if (_UnitofWork.SaveChanges())
                        {
                            return result.BindResultViewModel(true, "Data Updated Successfully", "تم تعديل المواد بنجاح", StatusCodes.Status200OK, null);
                        }
                    }
                }

            }
            catch (Exception)
            {
                return result;
            }
            return result;
        }
    }
}
