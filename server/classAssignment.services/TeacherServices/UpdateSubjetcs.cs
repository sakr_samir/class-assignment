﻿using classAssignment.data.UnitOfWork;
using classAssignment.services.TeacherServices.TeacherInterfaces;
using classAssignment.shared.ViewModels.Response;
using classAssignment.shared.ViewModels.Teacher;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace classAssignment.services.TeacherServices
{
    public class UpdateSubjetcs : IUpdateSubjetcs
    {
        private readonly IAssignSubject _AssignSubject;
        private readonly IUnAssignSubjects _UnAssignSubjects;
        private readonly ResultViewModel result;

        public UpdateSubjetcs(IAssignSubject AssignSubject, IUnAssignSubjects unAssignSubjects)
        {
            this._AssignSubject = AssignSubject;
            this._UnAssignSubjects = unAssignSubjects;
            result = new ResultViewModel();
        }
        public ResultViewModel UpdateTeacherSubjects(UpdateSubjectsVM model)
        {
            try
            {
                ResultViewModel AssignResult = new ResultViewModel();
                ResultViewModel UnAssignResult = new ResultViewModel();
                if (model.Code != 0 && model.AddSubjectes.Count() > 0)
                {
                    AssignResult = _AssignSubject.AssignTeacherSubject(new AssignSubjectViewModel()
                    {
                        Code = model.Code,
                        SubjectesToAdd = model.AddSubjectes
                    });
                }
                if (model.Code != 0 && model.RemoveSubjectes.Count() > 0)
                {
                    UnAssignResult = _UnAssignSubjects.UnAssignTeacherSubject(new UnAssignSubjectViewModel()
                    {
                        Code = model.Code,
                        SubjectesToRemove = model.RemoveSubjectes
                    });
                }
                if (AssignResult.Success)
                    return AssignResult;
                if (UnAssignResult.Success)
                    return UnAssignResult;
            }
            catch (Exception ex)
            {
                return result;
            }
            return result;
        }
    }
}
