﻿using classAssignment.data.StoredProcedures;
using classAssignment.data.UnitOfWork;
using classAssignment.entities;
using classAssignment.services.SchoolServices.SchoolInterfaces;
using classAssignment.services.TeacherServices.TeacherInterfaces;
using classAssignment.shared.ViewModels.Response;
using classAssignment.shared.ViewModels.Teacher;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

namespace classAssignment.services.TeacherServices
{
    public class GetTeachers : IGetTeachers
    {
        private readonly IUnitOfWork _UnitofWork;
        private readonly IVerfiySchool _VerfiySchool;
        private readonly ResultViewModel result;
        private readonly IGetTeachersClasses _GetTeachersClasses;

        public GetTeachers(IUnitOfWork unitofWork, IVerfiySchool verfiySchool, IGetTeachersClasses GetTeachersClasses)
        {
            this._UnitofWork = unitofWork;
            this._VerfiySchool = verfiySchool;
            result = new ResultViewModel();
            _GetTeachersClasses = GetTeachersClasses;
        }
        public ResultViewModel GetTeachersData(int schoolCode, string unassign)
        {
            using var tx = new TransactionScope(scopeOption: TransactionScopeOption.Required, new TransactionOptions() { IsolationLevel = IsolationLevel.ReadUncommitted }, TransactionScopeAsyncFlowOption.Enabled);
            if (schoolCode != 0)
            {
                var verfiyResult = _VerfiySchool.GetSchoolByCode(schoolCode);
                if (verfiyResult.Success)
                {
                    var Subjects = _UnitofWork.Repository<Subject>().GetAllAsQueryable().Where(ww => ww.STAGE_CODE == ((School)verfiyResult.ReturnObject).STAGE_CODE)
                        .Select(subj => new SubjectViewModel
                        {
                            Id = subj.SUBJ_CODE,
                            Name = subj.SUBJ_NAME
                        })
                        .ToList();

                    //var teachers = _UnitofWork.Repository<Teacher>().GetAllAsQueryable().Where(t => t.SCH_CODE == schoolCode).AsNoTracking()
                    //    .Select(t => new TeacherVM()
                    //    {
                    //        Code = t.EMP_CODE,
                    //        Name = t.EMP_FIRST_NAME + " " + t.EMP_FATHER_NAME + " " + t.EMP_GRAND_FATHER_NAME + " " + t.EMP_FAMILY_NAME,
                    //        TeacherSubjects = _UnitofWork.Repository<TeacherSubject>()
                    //        .GetAllAsQueryable()
                    //        .AsNoTracking()
                    //        .Include(ts => ts.Subject)
                    //        .Where(ts => ts.EMP_CODE == t.EMP_CODE)
                    //        .Select(ts => new SubjectViewModel() { Id = ts.Subject.SUBJ_CODE, Name = ts.Subject.SUBJ_NAME })
                    //        .ToList(),
                    //        Classes = _UnitofWork.Repository<ClassTeacherSubject>()
                    //        .GetAllAsQueryable()
                    //        .AsNoTracking()
                    //        .Include(cts => cts.TeacherSubject)
                    //        .Include(cts => cts.Class)
                    //        .Where(cts => cts.TeacherSubject.EMP_CODE == t.EMP_CODE)
                    //        .Select(cts => cts.Class.Name)
                    //        .ToList()
                    //    })
                    //    .ToList();

                    var teachers = _GetTeachersClasses.BindGetTeachersClasses(_GetTeachersClasses.GetGetTeachersClasses(schoolCode));

                    
                    foreach (var teacher in teachers)
                    {
                        teacher.Classes = teacher.Classes.Distinct().Where(x => !string.IsNullOrEmpty(x)).OrderBy(name => int.Parse(name.Replace(" / ", ""))).ToList();
                    }

                    teachers = unassign == "all" ? teachers : (unassign == "class" ? teachers.Where(x => x.Classes.Count == 0).ToList() : teachers.Where(x => x.TeacherSubjects.Count == 0).ToList());
                    tx.Complete();
                    if (teachers.Any())
                        return result.BindResultViewModel(true, "Data Retrived successfully", "تم تحديث قائمه المدرسين بنجاح", StatusCodes.Status200OK, new TeacherViewModel()
                        {
                            //Teachers = teacherSubjetcs,
                            //Teachers = unassign == "all"?teachers:(unassign=="class"?teachers.Where(x=>x.Classes.Count == 0).ToList():teachers.Where(x=>x.TeacherSubjects.Count==0).ToList()),
                            Teachers = teachers,
                            Subjects = Subjects
                        });
                }
            }
            return result;
        }
    }
}
