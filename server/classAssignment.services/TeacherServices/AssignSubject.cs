﻿using classAssignment.data.UnitOfWork;
using classAssignment.entities;
using classAssignment.services.TeacherServices.TeacherInterfaces;
using classAssignment.shared.ViewModels.Response;
using classAssignment.shared.ViewModels.Teacher;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace classAssignment.services.TeacherServices
{
    public class AssignSubject: IAssignSubject
    {
        private readonly IUnitOfWork _UnitofWork;
        private readonly IVerfiyTeacher _VerfiyTeacher;
        private readonly ResultViewModel result;

        public AssignSubject(IUnitOfWork unitofWork, IVerfiyTeacher verfiyTeacher)
        {
            this._UnitofWork = unitofWork;
            this._VerfiyTeacher = verfiyTeacher;
            result = new ResultViewModel();
        }
        public ResultViewModel AssignTeacherSubject(AssignSubjectViewModel model)
        {
            if(model.Code != 0)
            {
                var VerfiyResult = _VerfiyTeacher.GetTeacherByCode(model.Code);
                if (VerfiyResult.Success)
                {
                    foreach (var subject in model.SubjectesToAdd)
                    {
                        _UnitofWork.Repository<TeacherSubject>().Add(new TeacherSubject()
                        {
                            EMP_CODE=model.Code,
                            SUBJ_CODE=subject.Id,
                            CreationDate=DateTime.Now
                        });
                    }
                    if (_UnitofWork.SaveChanges())
                    {
                        return result.BindResultViewModel(true, "Data Saved Successfully", "تم تعيين المواد بنجاح", StatusCodes.Status201Created, null);
                    }
                }
            }
            return result;
        }
    }
}
