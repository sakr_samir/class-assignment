﻿using AutoMapper;
using classAssignment.entities;
using classAssignment.shared.ViewModels.Class;
using classAssignment.shared.ViewModels.User;

namespace classAssignment.mapping
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {

            CreateMap<AuthenticatedUserVM, User>().ReverseMap();
            CreateMap<UserSchoolVM, School>().ReverseMap();
            CreateMap<ClassVM, Class>().ReverseMap();
        }
    }
}
