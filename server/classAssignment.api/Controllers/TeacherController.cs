﻿using classAssignment.data.StoredProcedures;
using classAssignment.services.TeacherServices.TeacherInterfaces;
using classAssignment.shared.ViewModels.Teacher;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace classAssignment.api.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    [Authorize]
    public class TeacherController : ControllerBase
    {
        private readonly IGetTeachers _GetTeachers;
        private readonly IAssignSubject _AssignSubject;
        private readonly IUpdateSubjetcs _UpdateSubjetcs;
        private readonly ITeachersFullReport _teachersFullReport;


        public TeacherController(IGetTeachers getTeachers, IAssignSubject assignSubject,
            IUpdateSubjetcs updateSubjetcs,
            ITeachersFullReport TeachersFullReport)
        {
            this._GetTeachers = getTeachers;
            this._AssignSubject = assignSubject;
            this._UpdateSubjetcs = updateSubjetcs;
            _teachersFullReport = TeachersFullReport;
 
        }
        [HttpGet]
        public IActionResult GetTeachers(int SchoolCode, string unassign ="all")
        {
            return Ok(_GetTeachers.GetTeachersData(SchoolCode, unassign));
        }
        [HttpPost]
        public IActionResult AssignSubject(AssignSubjectViewModel model)
        {
            return Ok(_AssignSubject.AssignTeacherSubject(model));
        }
        [HttpPost]
        public IActionResult UpdateSubject(UpdateSubjectsVM model)
        {
            return Ok(_UpdateSubjetcs.UpdateTeacherSubjects(model));
        }
    }
}
