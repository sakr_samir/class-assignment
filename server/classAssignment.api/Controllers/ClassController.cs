﻿using classAssignment.services.ClassServices.ClassInterfaces;
using classAssignment.shared.ViewModels.Class;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace classAssignment.api.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    [Authorize]
    public class ClassController : ControllerBase
    {
        private readonly IGetGradeClassesService _getGradeClassesService;
        private readonly IGetClassTeachers _getClassTeachers;
        private readonly IUpdateClassSubjectTeachers _updateClassSubjectTeachers;
        private readonly IConfirmClassData _ConfirmClassData;
        private readonly ISetupClassService _setupClassService;
        private readonly IGetClassStudentsService _getClassStudentsService;
        private readonly IGetGradeSubjectsOnSetUpService _getGradeSubjectsOnSetUpService;
        private readonly ISetTeachersOnSetUp _setTeachersOnSetUp;
        public ClassController(IGetGradeClassesService getGradeClassesService, 
            ISetupClassService setupClassService, 
            IGetClassStudentsService getClassStudentsService, 
            IGetGradeSubjectsOnSetUpService getGradeSubjectsOnSetUpService,
            ISetTeachersOnSetUp setTeachersOnSetUp,
            IGetClassTeachers getClassTeachers,
            IUpdateClassSubjectTeachers updateClassSubjectTeachers,
            IConfirmClassData confirmClassData
            )
        {
            _getGradeClassesService = getGradeClassesService;
            this._getClassTeachers = getClassTeachers;
            this._updateClassSubjectTeachers = updateClassSubjectTeachers;
            this._ConfirmClassData = confirmClassData;
            _setupClassService = setupClassService;
            _getClassStudentsService = getClassStudentsService;
            _getGradeSubjectsOnSetUpService = getGradeSubjectsOnSetUpService;
            _setTeachersOnSetUp = setTeachersOnSetUp;
        }
        [HttpGet]
        public IActionResult GetClasses(int SchoolCode)
        {
            return Ok(_getGradeClassesService.GetGradeClasses(SchoolCode));
        }
        [HttpPost]
        public IActionResult SetupClasses(SetUpClassVM setUpClassVM)
        {
            return Ok(_setupClassService.CreateClasses(setUpClassVM));
        }
        [HttpGet]
        public IActionResult GetStudents(int classId)
        {
            return Ok(_getClassStudentsService.GetStudents(classId));
        }
        [HttpGet]
        public IActionResult GetSubjectsOnSetUp(int SchoolCode)
        {
            return Ok(_getGradeSubjectsOnSetUpService.GetGradeSubjects(SchoolCode));
        }
        [HttpPost]
        public IActionResult SetTeachersOnSetUp(ClassSubjectVM[] subjects)
        {
           return Ok(_setTeachersOnSetUp.SetTeachers(subjects));
        }
        [HttpGet]
        public IActionResult GetTeachers(int ClassId,int SchoolCode)
        {
            return Ok(_getClassTeachers.GetTeachers(ClassId,SchoolCode));
        }
        [HttpPost]
        public IActionResult UpdateTeachers(UpdateTeacherViewModel model)
        {
            return Ok(_updateClassSubjectTeachers.UpdateTeachers(model));
        }
        [HttpPost]
        public IActionResult ConfirmClass(ConfirmClassVM model)
        {
            return Ok(_ConfirmClassData.ConfirmData(model));
        }

    }
}
