﻿using classAssignment.services.StudentServises.StudentInterfaces;
using classAssignment.shared.ViewModels.Student;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace classAssignment.api.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    [Authorize]
    public class StudentController : ControllerBase
    {
        private readonly IGetGradeStudents _GetGradeStudents;
        private readonly IGetSchoolGrades _GetSchoolGrades;
        private readonly IAssignClass _AssignClass;

        public StudentController(IGetGradeStudents getGradeStudents, IGetSchoolGrades getSchoolGrades, IAssignClass assignClass)
        {
            this._GetGradeStudents = getGradeStudents;
            this._GetSchoolGrades = getSchoolGrades;
            this._AssignClass = assignClass;
        }
        [HttpGet]
        public IActionResult GetSchoolGrades(int SchoolCode, int selectedGradeCode,string assign)
        {
            return Ok(_GetSchoolGrades.GetGrades(SchoolCode, selectedGradeCode, assign));
        }
        [HttpPost]
        public IActionResult GetGradeStudents(SearchStudentViewModel model)
        {
            return Ok(_GetGradeStudents.GetStudents(model));
        }
        [HttpPost]
        public IActionResult AssginClass(AssignClassViewModel model)
        {
            return Ok(_AssignClass.AssignStudent(model));
        }
    }
}
