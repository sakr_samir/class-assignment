﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using classAssignment.services;
using classAssignment.shared.ViewModels;
using classAssignment.services.UserServices.UserInterfaces;
using classAssignment.shared.ViewModels.Response;
using classAssignment.shared.ViewModels.User;

namespace classAssignment.api.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class UserController : ControllerBase
    {
        private readonly IUserAuthentication _userAuthentication;
        private readonly ISSOLoginService _SSOLoginService;

        public UserController(IUserAuthentication userAuthentication, ISSOLoginService SSOLoginService)
        {
            _userAuthentication = userAuthentication;
            _SSOLoginService = SSOLoginService;
        }

        [HttpPost]
        public IActionResult LoginAdmin(LoginVM loginVM)
        {

            ResultViewModel result = _userAuthentication.Login(loginVM);
            return Ok(result);
        }

        [HttpPost]
        public IActionResult SSOLogin(SSOLoginVM loginVM)
        {
            ResultViewModel result = _SSOLoginService.SSOLogin(loginVM);
            return Ok(result);
        }


    }
}
