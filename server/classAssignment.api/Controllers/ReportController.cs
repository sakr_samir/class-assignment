﻿using classAssignment.data.StoredProcedures;
using classAssignment.services.TeacherServices.TeacherInterfaces;
using classAssignment.shared.ViewModels.Teacher;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace classAssignment.api.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    [Authorize]
    public class ReportController : ControllerBase
    {
        private readonly ITeachersFullReport _teachersFullReport;


        public ReportController(ITeachersFullReport TeachersFullReport)
        {
            _teachersFullReport = TeachersFullReport;
        }
        

        [HttpGet]
        public IActionResult GetUnAssignedTeachers(int govId)
        {
            return Ok(_teachersFullReport.GetReportData(govId));
        }

        [HttpGet]
        public IActionResult GetAllGovs()
        {
            return Ok(_teachersFullReport.GetGovsData());
        }
    }
}
