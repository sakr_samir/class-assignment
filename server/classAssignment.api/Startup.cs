using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using System.Text;
using classAssignment.data;
using classAssignment.entities;
using classAssignment.data.Repositories;
using classAssignment.data.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using classAssignment.services.UserServices.UserInterfaces;
using classAssignment.services.UserServices;
using user.management.server.services.UserServices;
using classAssignment.shared.ViewModels.JWT;
using Microsoft.OpenApi.Models;
using System.Collections.Generic;
using classAssignment.mapping;
using System;
using static classAssignment.shared.Utilities.Constants;
using classAssignment.services.TeacherServices;
using classAssignment.services.TeacherServices.TeacherInterfaces;
using classAssignment.services.ClassServices.ClassInterfaces;
using classAssignment.data.StoredProcedures;
using classAssignment.services.SchoolServices.SchoolInterfaces;
using classAssignment.services.SchoolServices;
using classAssignment.services.StudentServises.StudentInterfaces;
using classAssignment.services.StudentServises;
using classAssignment.services.ClassServices;

namespace classAssignment.api
{

    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            //CORS config
            services.AddCors(o => o.AddPolicy("Policy", builder =>
            {
                builder.AllowAnyOrigin()
                       .AllowAnyMethod()
                       .AllowAnyHeader();
            }));

            //init swagger
            services.AddSwaggerGen(c =>
            {
                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Description = @"JWT Authorization header using the Bearer scheme. \r\n\r\n 
                      Enter 'Bearer' [space] and then your token in the text input below.
                      \r\n\r\nExample: 'Bearer 12345abcdef'",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer"
                });

                c.AddSecurityRequirement(new OpenApiSecurityRequirement()
                {
                 {
                  new OpenApiSecurityScheme
                  {
                   Reference = new OpenApiReference
                   {
                      Type = ReferenceType.SecurityScheme,
                      Id = "Bearer"
                   },
                     Scheme = "oauth2",
                     Name = "Bearer",
                     In = ParameterLocation.Header,

                  },
                  new List<string>()
                 }
                });

            });
            services.AddControllers();

            //Database Context
            services.AddDbContext<ProjectContext>(options =>
                options.UseSqlServer(
                    Configuration.GetConnectionString("DefaultConnection")));

            // configure strongly typed settings object
            services.Configure<AppSettings>(Configuration.GetSection("AppSettings"));

            //Add Identity
            services.AddIdentity<User, IdentityRole>()
                .AddEntityFrameworkStores<ProjectContext>()
                .AddDefaultTokenProviders();

            //UnitOfWOrk
            services.AddScoped<IUnitOfWork, UnitOfWork>();


            //Register Entity
            services.AddScoped(typeof(IRepository<User>), typeof(GenericRepository<User>));

            //User
            services.AddScoped<IUserAuthentication, UserAuthentication>();
            services.AddScoped<IUserRole, UserRole>();
            services.AddScoped<ICheckUser, CheckUser>();
            services.AddScoped<ISSOLoginService, SSOLoginService>();

            //Teacher
            services.AddScoped<IGetTeachers, GetTeachers>();
            services.AddScoped<IVerfiyTeacher, VerfiyTeacher>();
            services.AddScoped<IAssignSubject, AssignSubject>();
            services.AddScoped<IUnAssignSubjects, UnAssignSubjects>();
            services.AddScoped<IUpdateSubjetcs, UpdateSubjetcs>();
            services.AddScoped<IGetTeachersClasses, GetTeachersClasses>();


            services.AddScoped<ITeachersFullReport, TeachersFullReport>();


            //School
            services.AddScoped<IVerfiySchool, VerfiySchool>();

            //Class
            services.AddScoped<IGetGradeClassesService, GetGradeClassesService>();
            services.AddScoped<ISchoolGradesClasses, SchoolGradesClasses>();
            services.AddScoped<ISetupClassService, SetupClassService>();
            services.AddScoped<IGetClassStudentsService, GetClassStudentsService>();
            services.AddScoped<IGetGradeSubjectsOnSetUpService, GetGradeSubjectsOnSetUpService>();
            services.AddScoped<IVerfiyClass, VerfiyClass>();
            services.AddScoped<IGetClassTeachers, GetClassTeachers>();
            services.AddScoped<ISetTeachersOnSetUp, SetTeachersOnSetUp>();
            services.AddScoped<IUpdateClassSubjectTeachers, UpdateClassSubjectTeachers>();
            services.AddScoped<IConfirmClassData, ConfirmClassData>();

            //Student
            services.AddScoped<IGetGradeStudents, GetGradeStudents>();
            services.AddScoped<IGetSchoolGrades, GetSchoolGrades>();
            services.AddScoped<IVerfiyStudent, VerfiyStudent>();
            services.AddScoped<IAssignClass, AssignClass>();

            // Auto Mapper Configurations
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new AutoMapperProfiles());
            });

            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);


            //Mapper
            //services.AddAutoMapper(typeof(Startup));

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_3_0);
            services.AddControllers();
            //services.AddSession();
            services.AddSession(options => {
                options.IdleTimeout = TimeSpan.FromMinutes(20);
            });

            // configure strongly typed settings objects
            var appSettingsSection = Configuration.GetSection("AppSettings");
            services.Configure<AppSettings>(appSettingsSection);

            // configure jwt authentication
            var appSettings = appSettingsSection.Get<AppSettings>();
            var key = Encoding.ASCII.GetBytes(appSettings.Secret);
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(x =>
            {
                x.Events = new JwtBearerEvents
                {
                    OnTokenValidated = context =>
                    {
                        var userService = context.HttpContext.RequestServices.GetRequiredService<IRepository<User>>();
                        var jwtToken = (JwtSecurityToken)context.SecurityToken;
                        var userId = jwtToken.Claims.First(x => x.Type == "id").Value;
                        var user = userService.Get(userId);
                        if (user == null)
                        {
                            // return unauthorized if user no longer exists
                            context.Fail("Unauthorized");
                        }
                        return Task.CompletedTask;
                    }
                };
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });


            /*Spa Startup*/
            SpaStartup.ConfigureServices(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IServiceProvider serviceProvider)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors("Policy");

            app.UseAuthentication();

            CreateRolesIfNotExist(serviceProvider).Wait();

            app.UseRouting();

            app.UseStaticFiles();


            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseSwagger();
            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), 
            string UrlPah = string.Empty;
            if (env.IsDevelopment())
            {
                UrlPah = "/swagger/v1/swagger.json";
            }
            else
            {
                UrlPah = "v1/swagger.json";

            }

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint($"{UrlPah}", "Maintainace.API V1");
            });

            /*Spa Startup*/
            SpaStartup.Configure(app);

        }

        private async Task CreateRolesIfNotExist(IServiceProvider serviceProvider)
        {
            var roleManage = serviceProvider.GetRequiredService<RoleManager<IdentityRole>>();
            foreach (var role in typeof(UserRoles).GetFields())
            {
                var RoleExistes = await roleManage.RoleExistsAsync(role.GetValue(role.Name).ToString());
                if (!RoleExistes)
                {
                    await roleManage.CreateAsync(new IdentityRole(role.GetValue(role.Name).ToString()));
                }
            }
        }



    }


}
