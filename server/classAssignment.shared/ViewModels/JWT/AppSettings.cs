﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace classAssignment.shared.ViewModels.JWT
{
    public class AppSettings
    {
        public string Secret { get; set; }
    }
}
