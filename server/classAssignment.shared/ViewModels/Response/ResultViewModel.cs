﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;

namespace classAssignment.shared.ViewModels.Response
{
    public class ResultViewModel
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public string ArabicMessage { get; set; }
        public Object ReturnObject { get; set; }
        public int StatusCode { get; set; }
        public ResultViewModel()
        {
            Success = false;
            Message = "an error occurred, try again later";
            ArabicMessage = "حدث خطا، حاول مرة اخري";
            StatusCode = StatusCodes.Status400BadRequest;
            ReturnObject = null;

        }
        public ResultViewModel BindResultViewModel(bool success, string message, string arabicMessage, int statusCode, Object returnObject)
        {
            Success = success;
            Message = message;
            ArabicMessage = arabicMessage;
            StatusCode = statusCode;
            ReturnObject = returnObject;

            return this;
        }
    }

}

