﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace classAssignment.shared.ViewModels.Student
{
    public class SearchStudentViewModel
    {
        public int SchoolCode { get; set; }
        public int GradeCode { get; set; }
        public int? StudentCode { get; set; }
        public string StudentName { get; set; }
        public string ClassName { get; set; }
        public List<int> ClassIds { get; set; }
        public int? PageIndex { get; set; }
        public int? PageLimit { get; set; }

        public string assign { get; set; }
    }
}
