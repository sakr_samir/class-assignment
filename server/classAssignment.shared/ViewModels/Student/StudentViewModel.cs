﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace classAssignment.shared.ViewModels.Student
{
    public class StudentViewModel
    {
        public int StudentCode { get; set; }
        public string StudentName { get; set; }
        public int ClassCode { get; set; }
    }
}
