﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace classAssignment.shared.ViewModels.Student
{
    public class AssignClassViewModel
    {
        public int StudentCode { get; set; }
        public int ClassCode { get; set; }
    }
}
