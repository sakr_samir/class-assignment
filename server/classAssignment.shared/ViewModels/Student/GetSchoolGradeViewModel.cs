﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace classAssignment.shared.ViewModels.Student
{
    public class GetSchoolGradeViewModel
    {
        public List<SchoolGradeViewModel> SchoolGrades { get; set; }
        public List<StudentViewModel> Students { get; set; }
        public int PagesNumber { get; set; }
    }
}
