﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace classAssignment.shared.ViewModels.Student
{
    public class SchoolGradeViewModel
    {
        public int GradeCode { get; set; }
        public string GradeName { get; set; }
        public List<ClassViewModel> Classes { get; set; } = new List<ClassViewModel>();
    }
}
