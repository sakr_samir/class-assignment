﻿using System;
namespace classAssignment.shared.ViewModels.User
{
    public class AuthenticatedUserVM
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public string FullName { get; set; }
        public string Token { get; set; }
        public string RoleName { get; set; }
        public UserSchoolVM UserSchoolData { get; set; }
    }
}
