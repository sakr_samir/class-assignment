﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace classAssignment.shared.ViewModels.User
{
    public class SSOLoginVM
    {
        public int BCH_CODE { get; set; }
        public int security_code { get; set; }
    }
}
