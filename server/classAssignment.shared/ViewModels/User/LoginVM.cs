﻿using System;
using System.ComponentModel.DataAnnotations;

namespace classAssignment.shared.ViewModels.User
{
    public class LoginVM
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }
    }
}
