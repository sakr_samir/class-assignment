﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace classAssignment.shared.ViewModels.Teacher
{
    public class AssignSubjectViewModel
    {
        public int Code { get; set; }
        public List<SubjectViewModel> SubjectesToAdd { get; set; }
    }
    public class UnAssignSubjectViewModel
    {
        public int Code { get; set; }
        public List<SubjectViewModel> SubjectesToRemove { get; set; }
    }
}
