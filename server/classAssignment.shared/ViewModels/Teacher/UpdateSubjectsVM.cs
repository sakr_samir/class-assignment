﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace classAssignment.shared.ViewModels.Teacher
{
    public class UpdateSubjectsVM
    {
        public int Code { get; set; }
        public List<SubjectViewModel> AddSubjectes { get; set; }
        public List<SubjectViewModel> RemoveSubjectes { get; set; }
    }
}
