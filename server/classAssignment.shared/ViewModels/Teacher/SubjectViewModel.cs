﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace classAssignment.shared.ViewModels.Teacher
{
    public class SubjectViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
