﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace classAssignment.shared.ViewModels.Teacher
{
    public class SpGetTeacherClassesVM
    {
        public int EMP_CODE { get; set; }
        public int Teacher_CODE { get; set; }
        public string Emp_Name { get; set; }
        public int Subj_ID { get; set; }
        public string Sub_Name { get; set; }
        public string Class_Name { get; set; }
     
        public int CTS_Id { get; set; }
        public int Class_Id { get; set; }
    }
}
