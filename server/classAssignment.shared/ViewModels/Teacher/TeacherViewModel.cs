﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace classAssignment.shared.ViewModels.Teacher
{
    public class TeacherViewModel
    {
        public List<TeacherVM> Teachers { get; set; }
        public List<SubjectViewModel> Subjects { get; set; }
    }
    public class TeacherVM
    {
        public int Code { get; set; }
        public int TeacherCode { get; set; }
        public string Name { get; set; }
        public List<SubjectViewModel> TeacherSubjects { get; set; }
        public List<string> Classes { get; set; }
    }
}
