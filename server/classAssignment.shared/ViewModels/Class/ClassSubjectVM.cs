﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace classAssignment.shared.ViewModels.Class
{
    public class ClassSubjectVM
    {
        public int id { get; set; }
        public string name { get; set; }

        public List<TeacherVM> subjectTeachers { get; set; }
        public ClassSubjectVM()
        {
            subjectTeachers = new List<TeacherVM>();
        }

    }

    public class GetClassSubjectViewModel
    {
        public int id { get; set; }
        public string name { get; set; }

        public List<GetTeacherViewModel> subjectTeachers { get; set; }
        public List<GetTeacherViewModel> selectedTeachers { get; set; }

        public GetClassSubjectViewModel()
        {
            selectedTeachers = new List<GetTeacherViewModel>();
            subjectTeachers = new List<GetTeacherViewModel>();
        }

    }
}
