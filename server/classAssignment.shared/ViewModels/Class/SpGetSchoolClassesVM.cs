﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace classAssignment.shared.ViewModels.Class
{
    public class SpGetSchoolClassesVM
    {
        public int GradeCode { get; set; }
        public string GradeName { get; set; }
        public int ClassesNum { get; set; }
        public int ClassId { get; set; }
        public string ClassName { get; set; }
        public int StudentCount { get; set; }
        public int TeacherCount { get; set; }
    }
}
