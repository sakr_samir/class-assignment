﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace classAssignment.shared.ViewModels.Class
{
    public class GradeVM
    {
        public int Code { get; set; }
        public string name { get; set; }

        public List<ClassVM> classes { get; set; }
        public int classNum { get; set; }

        public int unassignedStudentNum { get; set; }

        public int totalStudentNum { get; set; }
        public GradeVM()
        {
            classes = new List<ClassVM>();
        }

    }
}
