﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace classAssignment.shared.ViewModels.Class
{
    public class TeacherVM
    {
        public int code { get; set; }
        public string name { get; set; }
        public List<int> classesIds { get; set; }
        public int TeacherSubjectId { get; set; }
        public TeacherVM()
        {
            classesIds = new List<int>();
    }
    }
}
