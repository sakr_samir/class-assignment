﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace classAssignment.shared.ViewModels.Class
{
    public class ConfirmClassVM
    {
        public int classId { get; set; }
        public bool studentsConfirmed { get; set; }
        public bool teachersConfirmed { get; set; }
    }
}
