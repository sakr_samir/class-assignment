﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace classAssignment.shared.ViewModels.Class
{
    public class SetUpClassVM
    {
        public int GradeCode { get; set; }
        public int NumOfClasses { get; set; }
        public int SchoolCode { get; set; }
        public bool OnlySave { get; set; }

    }
}
