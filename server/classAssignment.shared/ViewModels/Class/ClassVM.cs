﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace classAssignment.shared.ViewModels.Class
{
    public class ClassVM
    {
        public int id { get; set; }
        public string name { get; set; }
        public int totalStudents { get; set; }
        public int totalTeachers { get; set; }
        public List<ClassStudentVM> students { get; set; }
        //public List<ClassSubjectVM> subjects { get; set; }
        public ClassVM()
        {
            students = new List<ClassStudentVM>();
            //subjects = new List<ClassSubjectVM>();
        }
    }
    public class ClassDetailsVM
    {
        public int id { get; set; }
        public string name { get; set; }
        public bool teachersConfirmed { get; set; }
        public bool studentsConfirmed { get; set; }
    }
}
