﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace classAssignment.shared.ViewModels.Class
{
    public class GetTeacherViewModel
    {
        public int TeacherSubjectId{ get; set; }
        public int TeacherCode { get; set; }
        public string name { get; set; }
    }
}
