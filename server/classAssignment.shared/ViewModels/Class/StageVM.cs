﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace classAssignment.shared.ViewModels.Class
{
    public class StageVM
    {
        public List<GradeVM> Grades { get; set; }
        public int MaxNumberOfClasses { get; set; }
        public StageVM()
        {
            Grades = new List<GradeVM>();
        }
        public int unassignedTeacherToClassNum { get; set; }
        public int unassignedTeacherToSubjectNum { get; set; }
        public int unassignedStudentNum { get; set; }

    }
}
