﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace classAssignment.shared.ViewModels.Class
{
    public class UpdateTeacherViewModel
    {
        public int ClassId { get; set; }
        public List<GetTeacherViewModel> AddTeachers { get; set; }
        public List<GetTeacherViewModel> RemoveTeachers { get; set; }
    }
}
