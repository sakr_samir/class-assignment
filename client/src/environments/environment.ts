export const environment = {
  production: false,
  appVersion: 'sisappenza2020',
  baseApiUrl: 'https://localhost:44365/api',
};
