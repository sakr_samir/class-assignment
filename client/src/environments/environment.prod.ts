export const environment = {
  production: true,
  appVersion: 'sisappenza2020',
  baseApiUrl: 'https://www.appenza.app:5051/api',
};
