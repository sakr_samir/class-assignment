import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserRole } from './modules/user/interfaces/user-roles.enum';
import { AuthGuardService } from './modules/user/services/auth-guard.service';
import { RoleGuardService } from './modules/user/services/role-guard.service';

export const routes: Routes = [
  {
    path: 'dashboard',
    canActivate: [AuthGuardService],
    loadChildren: () =>
      import('@modules/dashboard/dashboard.module').then(
        (m) => m.DashboardModule
      ),
  },
  {
    path: 'teachers',
    canActivate: [RoleGuardService],
    data: {
      expectedRole: UserRole.SuperAdmin,
      anotherRole: UserRole.SchoolAdmin,
    },
    loadChildren: () =>
      import('@modules/teachers/teachers.module').then((m) => m.TeachersModule),
  },
  {
    path: 'classes',
    canActivate: [RoleGuardService],
    data: {
      expectedRole: UserRole.SuperAdmin,
      anotherRole: UserRole.SchoolAdmin,
    },
    loadChildren: () =>
      import('@modules/classes/classes.module').then((m) => m.ClassesModule),
  },
  {
    path: 'students',
    canActivate: [RoleGuardService],
    data: {
      expectedRole: UserRole.SuperAdmin,
      anotherRole: UserRole.SchoolAdmin,
    },
    loadChildren: () =>
      import('@modules/students/students.module').then((m) => m.StudentsModule),
  },
  {
    path: 'auth',
    loadChildren: () =>
      import('./modules/user/user.module').then((m) => m.UserModule),
  },

  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  },
  { path: '**', redirectTo: 'errors/404', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
