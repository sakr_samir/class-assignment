import { Injectable } from '@angular/core';
import { saveAs } from 'file-saver';
import {
    AlignmentType,
    BorderStyle,
    Document,
    HeadingLevel,
    HeightRule,
    Packer,
    Paragraph,
    SectionType,
    ShadingType,
    Table,
    TableCell,
    TableLayoutType,
    TableRow,
    TabStopPosition,
    TabStopType,
    TextRun,
    WidthType,
  } from 'docx';
import { UnAssignedTeachersPageProps } from '../interfaces/UnAssignedTeachers.PageProps.interface';
import { DashboardService } from '../services/dashboard.service';
import { ReportData } from '../interfaces/ReportData.interfece';

@Injectable({
  providedIn: 'root',
})
export class UnAssignedTeachersPage {
    pageProps:UnAssignedTeachersPageProps={
        isLoading:false,
        message:'',
        allData:[],
        govs:[],
        selectedGov:0,
        totalNumbers:[]
    };
  constructor(private dashboardService: DashboardService) {}

async SetUnAssignedTeachers(govId:number) {
  this.pageProps.message = '';
    this.pageProps.isLoading=true;
    try {
      const result: any = await this.dashboardService.GetUnAssignedTeachers(govId);
      if (result.success) {
        this.pageProps.allData = result.returnObject.reportData;
        this.pageProps.totalNumbers = result.returnObject.totalNumbers;
        let childern = this.getReportDataDesign();
        this.exportDoc(childern);
      } else {
        this.pageProps.message = result.arabicMessage;
      }
    } catch ({ error, statusText }) {
      this.pageProps.message = 'حدث خطأ ما حاول مره اخري';
    }
    this.pageProps.isLoading = false;

  }

getReportDataDesign()
{
  let reportData:ReportData[] = [];

  let totalNumbers:{headers,rowData:any[]}={headers:{},rowData:[]};
  
  ///// start total
  this.pageProps.totalNumbers.forEach(total=>{
      totalNumbers.rowData.push(
        new TableRow({
          children: [
            new TableCell({
              children: [
                new Paragraph({
                  alignment: AlignmentType.CENTER,
                  text: total.totalTeachers.toString(),
                  heading: HeadingLevel.HEADING_2,
                }),
              ],
              borders: {
                top: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
                bottom: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
                left: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
                right: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
              },
            }),
            new TableCell({
              children: [
                new Paragraph({
                  alignment: AlignmentType.CENTER,
                  text: total.totalSchools.toString(),
                  heading: HeadingLevel.HEADING_2,
                }),
              ],
              borders: {
                top: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
                bottom: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
                left: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
                right: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
              },
            }),
            new TableCell({
              children: [
                new Paragraph({
                  alignment: AlignmentType.CENTER,
                  text: total.govName,
                  heading: HeadingLevel.HEADING_2,
                }),
              ],
              borders: {
                top: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
                bottom: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
                left: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
                right: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
              },
            }),
          ],
        })
      );
  });
  totalNumbers.headers = new Table({
    rows: [
      new TableRow({
        children: [
          new TableCell({
            children: [
              new Paragraph({
                alignment: AlignmentType.CENTER,
                text: 'عدد المدرسين',
                heading: HeadingLevel.HEADING_1,
                shading:{fill: "FFFF00"}
              }),
            ],
            borders: {
              top: {
                style: BorderStyle.DOUBLE,
                size: 3,
                color: '#000000',
              },
              bottom: {
                style: BorderStyle.DOUBLE,
                size: 3,
                color: '#000000',
              },
              left: {
                style: BorderStyle.DOUBLE,
                size: 3,
                color: '#000000',
              },
              right: {
                style: BorderStyle.DOUBLE,
                size: 3,
                color: '#000000',
              },
            },
          }),
          new TableCell({
            children: [
              new Paragraph({
                alignment: AlignmentType.CENTER,
                text: 'عدد المدارس',
                heading: HeadingLevel.HEADING_1,
                shading:{fill: "FFFF00"}
              }),
            ],
            borders: {
              top: {
                style: BorderStyle.DOUBLE,
                size: 3,
                color: '#000000',
              },
              bottom: {
                style: BorderStyle.DOUBLE,
                size: 3,
                color: '#000000',
              },
              left: {
                style: BorderStyle.DOUBLE,
                size: 3,
                color: '#000000',
              },
              right: {
                style: BorderStyle.DOUBLE,
                size: 3,
                color: '#000000',
              },
            },
          }),
          new TableCell({
            children: [
              new Paragraph({
                alignment: AlignmentType.CENTER,
                text: 'المديرية',
                heading: HeadingLevel.HEADING_1,
                shading:{fill: "FFFF00"}
              }),
            ],
            borders: {
              top: {
                style: BorderStyle.DOUBLE,
                size: 3,
                color: '#000000',
              },
              bottom: {
                style: BorderStyle.DOUBLE,
                size: 3,
                color: '#000000',
              },
              left: {
                style: BorderStyle.DOUBLE,
                size: 3,
                color: '#000000',
              },
              right: {
                style: BorderStyle.DOUBLE,
                size: 3,
                color: '#000000',
              },
            },
          }),
        ],
      }),
      ...totalNumbers.rowData,
    ],
    alignment: AlignmentType.CENTER,
    borders: {
      top: {
        style: BorderStyle.DOUBLE,
        size: 3,
        color: '#000000',
      },
      bottom: {
        style: BorderStyle.DOUBLE,
        size: 3,
        color: '#000000',
      },
      left: {
        style: BorderStyle.DOUBLE,
        size: 3,
        color: '#000000',
      },
      right: {
        style: BorderStyle.DOUBLE,
        size: 3,
        color: '#000000',
      },
    },
    width: {
      size: 130,
      type: WidthType.PERCENTAGE,
    },
  })

  ///// end total

  this.pageProps.allData.forEach(gov=>{
    let reportItem :ReportData = 
    {
      paragraphs:[],
      numbers:{headers:{},rowData:[]},
      details: {headers:{},rowData:[]},
      TotalNumbers:{headers:{},rowData:[]}
    };

      reportItem.TotalNumbers.rowData.push(
        new TableRow({
          children: [
            new TableCell({
              children: [
                new Paragraph({
                  alignment: AlignmentType.CENTER,
                  text: gov.totalTeachers.toString(),
                  heading: HeadingLevel.HEADING_2,
                }),
              ],
              borders: {
                top: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
                bottom: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
                left: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
                right: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
              },
            }),
            new TableCell({
              children: [
                new Paragraph({
                  alignment: AlignmentType.CENTER,
                  text: gov.totalSchools.toString(),
                  heading: HeadingLevel.HEADING_2,
                }),
              ],
              borders: {
                top: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
                bottom: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
                left: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
                right: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
              },
            }),
            new TableCell({
              children: [
                new Paragraph({
                  alignment: AlignmentType.CENTER,
                  text: gov.govName,
                  heading: HeadingLevel.HEADING_2,
                }),
              ],
              borders: {
                top: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
                bottom: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
                left: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
                right: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
              },
            }),
          ],
        })
      );
      reportItem.TotalNumbers.headers = new Table({
        rows: [
          new TableRow({
            children: [
              new TableCell({
                children: [
                  new Paragraph({
                    alignment: AlignmentType.CENTER,
                    text: 'عدد المدرسين',
                    heading: HeadingLevel.HEADING_1,
                    shading:{fill: "FFFF00"}
                  }),
                ],
                borders: {
                  top: {
                    style: BorderStyle.DOUBLE,
                    size: 3,
                    color: '#000000',
                  },
                  bottom: {
                    style: BorderStyle.DOUBLE,
                    size: 3,
                    color: '#000000',
                  },
                  left: {
                    style: BorderStyle.DOUBLE,
                    size: 3,
                    color: '#000000',
                  },
                  right: {
                    style: BorderStyle.DOUBLE,
                    size: 3,
                    color: '#000000',
                  },
                },
              }),
              new TableCell({
                children: [
                  new Paragraph({
                    alignment: AlignmentType.CENTER,
                    text: 'عدد المدارس',
                    heading: HeadingLevel.HEADING_1,
                    shading:{fill: "FFFF00"}
                  }),
                ],
                borders: {
                  top: {
                    style: BorderStyle.DOUBLE,
                    size: 3,
                    color: '#000000',
                  },
                  bottom: {
                    style: BorderStyle.DOUBLE,
                    size: 3,
                    color: '#000000',
                  },
                  left: {
                    style: BorderStyle.DOUBLE,
                    size: 3,
                    color: '#000000',
                  },
                  right: {
                    style: BorderStyle.DOUBLE,
                    size: 3,
                    color: '#000000',
                  },
                },
              }),
              new TableCell({
                children: [
                  new Paragraph({
                    alignment: AlignmentType.CENTER,
                    text: 'المديرية',
                    heading: HeadingLevel.HEADING_1,
                    shading:{fill: "FFFF00"}
                  }),
                ],
                borders: {
                  top: {
                    style: BorderStyle.DOUBLE,
                    size: 3,
                    color: '#000000',
                  },
                  bottom: {
                    style: BorderStyle.DOUBLE,
                    size: 3,
                    color: '#000000',
                  },
                  left: {
                    style: BorderStyle.DOUBLE,
                    size: 3,
                    color: '#000000',
                  },
                  right: {
                    style: BorderStyle.DOUBLE,
                    size: 3,
                    color: '#000000',
                  },
                },
              }),
            ],
          }),
          ...reportItem.TotalNumbers.rowData,
        ],
        alignment: AlignmentType.CENTER,
        borders: {
          top: {
            style: BorderStyle.DOUBLE,
            size: 3,
            color: '#000000',
          },
          bottom: {
            style: BorderStyle.DOUBLE,
            size: 3,
            color: '#000000',
          },
          left: {
            style: BorderStyle.DOUBLE,
            size: 3,
            color: '#000000',
          },
          right: {
            style: BorderStyle.DOUBLE,
            size: 3,
            color: '#000000',
          },
        },
        width: {
          size: 130,
          type: WidthType.PERCENTAGE,
        },
      })



    reportItem.paragraphs=
      new Paragraph({
        alignment: AlignmentType.CENTER,
        text: gov.govName,
        heading: HeadingLevel.TITLE,
        pageBreakBefore: true,
      });


    reportItem.numbers.rowData.push(
      new TableRow({
        children: [
          new TableCell({
            children: [
              new Paragraph({
                alignment: AlignmentType.CENTER,
                text: gov.totalTeachers.toString(),
                heading: HeadingLevel.HEADING_2,
              }),
            ],
            borders: {
              top: {
                style: BorderStyle.SINGLE,
                size: 3,
                color: '#000000',
              },
              bottom: {
                style: BorderStyle.SINGLE,
                size: 3,
                color: '#000000',
              },
              left: {
                style: BorderStyle.SINGLE,
                size: 3,
                color: '#000000',
              },
              right: {
                style: BorderStyle.SINGLE,
                size: 3,
                color: '#000000',
              },
            },
          }),
          new TableCell({
            children: [
              new Paragraph({
                alignment: AlignmentType.CENTER,
                text: gov.totalSchools.toString(),
                heading: HeadingLevel.HEADING_2,
              }),
            ],
            borders: {
              top: {
                style: BorderStyle.SINGLE,
                size: 3,
                color: '#000000',
              },
              bottom: {
                style: BorderStyle.SINGLE,
                size: 3,
                color: '#000000',
              },
              left: {
                style: BorderStyle.SINGLE,
                size: 3,
                color: '#000000',
              },
              right: {
                style: BorderStyle.SINGLE,
                size: 3,
                color: '#000000',
              },
            },
          }),
        ],
      })
    );
    
   if(gov.teachers.length>0){
    gov.teachers.forEach(teacher=>{
      reportItem.details.rowData.push(
        new TableRow({
          children: [
            new TableCell({
              children: [
                new Paragraph({
                  alignment: AlignmentType.CENTER,
                  text: teacher.sub_Name,
                  heading: HeadingLevel.HEADING_2,
                }),
              ],
              borders: {
                top: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
                bottom: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
                left: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
                right: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
              },
            }),
            new TableCell({
              children: [
                new Paragraph({
                  alignment: AlignmentType.CENTER,
                  text: teacher.emp_Name,
                  heading: HeadingLevel.HEADING_2,
                }),
              ],
              borders: {
                top: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
                bottom: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
                left: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
                right: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
              },
            }),
            new TableCell({
              children: [
                new Paragraph({
                  alignment: AlignmentType.CENTER,
                  text: teacher.emp_Code.toString(),
                  heading: HeadingLevel.HEADING_2,
                }),
              ],
              borders: {
                top: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
                bottom: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
                left: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
                right: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
              },
            }),
            new TableCell({
              children: [
                new Paragraph({
                  alignment: AlignmentType.CENTER,
                  text: teacher.sch_Name,
                  heading: HeadingLevel.HEADING_2,
                }),
              ],
              borders: {
                top: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
                bottom: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
                left: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
                right: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
              },
            }),
            new TableCell({
              children: [
                new Paragraph({
                  alignment: AlignmentType.CENTER,
                  text: teacher.sch_Code.toString(),
                  heading: HeadingLevel.HEADING_2,
                }),
              ],
              borders: {
                top: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
                bottom: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
                left: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
                right: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
              },
            }),
            new TableCell({
              children: [
                new Paragraph({
                  alignment: AlignmentType.CENTER,
                  text: teacher.stage_Desc,
                  heading: HeadingLevel.HEADING_2,
                }),
              ],
              borders: {
                top: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
                bottom: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
                left: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
                right: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
              },
            }),
            new TableCell({
              children: [
                new Paragraph({
                  alignment: AlignmentType.CENTER,
                  text: teacher.dist_Name,
                  heading: HeadingLevel.HEADING_2,
                }),
              ],
              borders: {
                top: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
                bottom: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
                left: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
                right: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
              },
            }),
          ],
        }));
    
       
          
    });
   }
   else{
      reportItem.details.rowData.push(
        new TableRow({
          children: [
            new TableCell({
              children: [
                new Paragraph({
                  alignment: AlignmentType.CENTER,
                  text: '-',
                  heading: HeadingLevel.HEADING_2,
                }),
              ],
              borders: {
                top: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
                bottom: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
                left: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
                right: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
              },
            }),
            new TableCell({
              children: [
                new Paragraph({
                  alignment: AlignmentType.CENTER,
                  text: '-',
                  heading: HeadingLevel.HEADING_2,
                }),
              ],
              borders: {
                top: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
                bottom: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
                left: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
                right: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
              },
            }),
            new TableCell({
              children: [
                new Paragraph({
                  alignment: AlignmentType.CENTER,
                  text: '-',
                  heading: HeadingLevel.HEADING_2,
                }),
              ],
              borders: {
                top: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
                bottom: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
                left: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
                right: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
              },
            }),
            new TableCell({
              children: [
                new Paragraph({
                  alignment: AlignmentType.CENTER,
                  text: '-',
                  heading: HeadingLevel.HEADING_2,
                }),
              ],
              borders: {
                top: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
                bottom: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
                left: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
                right: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
              },
            }),
            new TableCell({
              children: [
                new Paragraph({
                  alignment: AlignmentType.CENTER,
                  text: '-',
                  heading: HeadingLevel.HEADING_2,
                }),
              ],
              borders: {
                top: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
                bottom: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
                left: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
                right: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
              },
            }),
            new TableCell({
              children: [
                new Paragraph({
                  alignment: AlignmentType.CENTER,
                  text: '-',
                  heading: HeadingLevel.HEADING_2,
                }),
              ],
              borders: {
                top: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
                bottom: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
                left: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
                right: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
              },
            }),
            new TableCell({
              children: [
                new Paragraph({
                  alignment: AlignmentType.CENTER,
                  text: '-',
                  heading: HeadingLevel.HEADING_2,
                }),
              ],
              borders: {
                top: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
                bottom: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
                left: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
                right: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
              },
            }),
            
          ],
        }));
   }

    // header detials
    reportItem.details.headers =  new Table({
      rows: [
        new TableRow({
          children: [
            new TableCell({
              children: [
                new Paragraph({
                  alignment: AlignmentType.CENTER,
                  text: 'المادة',
                  heading: HeadingLevel.HEADING_1,
                  shading:{fill: "FFFF00"}
                }),
              ],
              borders: {
                top: {
                  style: BorderStyle.DOUBLE,
                  size: 3,
                  color: '#000000',
                },
                bottom: {
                  style: BorderStyle.DOUBLE,
                  size: 3,
                  color: '#000000',
                },
                left: {
                  style: BorderStyle.DOUBLE,
                  size: 3,
                  color: '#000000',
                },
                right: {
                  style: BorderStyle.DOUBLE,
                  size: 3,
                  color: '#000000',
                },
              },
            }),
            new TableCell({
              children: [
                new Paragraph({
                  alignment: AlignmentType.CENTER,
                  text: 'اسم المدرس',
                  heading: HeadingLevel.HEADING_1,
                  shading:{fill: "FFFF00"}
                }),
              ],
              borders: {
                top: {
                  style: BorderStyle.DOUBLE,
                  size: 3,
                  color: '#000000',
                },
                bottom: {
                  style: BorderStyle.DOUBLE,
                  size: 3,
                  color: '#000000',
                },
                left: {
                  style: BorderStyle.DOUBLE,
                  size: 3,
                  color: '#000000',
                },
                right: {
                  style: BorderStyle.DOUBLE,
                  size: 3,
                  color: '#000000',
                },
              },
            }),
            new TableCell({
              children: [
                new Paragraph({
                  alignment: AlignmentType.CENTER,
                  text: 'كود المدرس',
                  heading: HeadingLevel.HEADING_1,
                  shading:{fill: "FFFF00"}
                }),
              ],
              borders: {
                top: {
                  style: BorderStyle.DOUBLE,
                  size: 3,
                  color: '#000000',
                },
                bottom: {
                  style: BorderStyle.DOUBLE,
                  size: 3,
                  color: '#000000',
                },
                left: {
                  style: BorderStyle.DOUBLE,
                  size: 3,
                  color: '#000000',
                },
                right: {
                  style: BorderStyle.DOUBLE,
                  size: 3,
                  color: '#000000',
                },
              },
            }),
            new TableCell({
              children: [
                new Paragraph({
                  alignment: AlignmentType.CENTER,
                  text: 'اسم المدرسة',
                  heading: HeadingLevel.HEADING_1,
                  shading:{fill: "FFFF00"}
                }),
              ],
              borders: {
                top: {
                  style: BorderStyle.DOUBLE,
                  size: 3,
                  color: '#000000',
                },
                bottom: {
                  style: BorderStyle.DOUBLE,
                  size: 3,
                  color: '#000000',
                },
                left: {
                  style: BorderStyle.DOUBLE,
                  size: 3,
                  color: '#000000',
                },
                right: {
                  style: BorderStyle.DOUBLE,
                  size: 3,
                  color: '#000000',
                },
              },
            }),
            new TableCell({
              children: [
                new Paragraph({
                  alignment: AlignmentType.CENTER,
                  text: 'كود المدرسة',
                  heading: HeadingLevel.HEADING_1,
                  shading:{fill: "FFFF00"}
                }),
              ],
              borders: {
                top: {
                  style: BorderStyle.DOUBLE,
                  size: 3,
                  color: '#000000',
                },
                bottom: {
                  style: BorderStyle.DOUBLE,
                  size: 3,
                  color: '#000000',
                },
                left: {
                  style: BorderStyle.DOUBLE,
                  size: 3,
                  color: '#000000',
                },
                right: {
                  style: BorderStyle.DOUBLE,
                  size: 3,
                  color: '#000000',
                },
              },
            }),
            new TableCell({
              children: [
                new Paragraph({
                  alignment: AlignmentType.CENTER,
                  text: 'المرحلة',
                  heading: HeadingLevel.HEADING_1,
                  shading:{fill: "FFFF00"}
                }),
              ],
              borders: {
                top: {
                  style: BorderStyle.DOUBLE,
                  size: 3,
                  color: '#000000',
                },
                bottom: {
                  style: BorderStyle.DOUBLE,
                  size: 3,
                  color: '#000000',
                },
                left: {
                  style: BorderStyle.DOUBLE,
                  size: 3,
                  color: '#000000',
                },
                right: {
                  style: BorderStyle.DOUBLE,
                  size: 3,
                  color: '#000000',
                },
              },
            }),
            new TableCell({
              children: [
                new Paragraph({
                  alignment: AlignmentType.CENTER,
                  text: 'الادارة',
                  heading: HeadingLevel.HEADING_1,
                  shading:{fill: "FFFF00"}
                }),
              ],
              borders: {
                top: {
                  style: BorderStyle.DOUBLE,
                  size: 3,
                  color: '#000000',
                },
                bottom: {
                  style: BorderStyle.DOUBLE,
                  size: 3,
                  color: '#000000',
                },
                left: {
                  style: BorderStyle.DOUBLE,
                  size: 3,
                  color: '#000000',
                },
                right: {
                  style: BorderStyle.DOUBLE,
                  size: 3,
                  color: '#000000',
                },
              },
            }),
          ],
        }),
        ...reportItem.details.rowData,
      ],
      alignment: AlignmentType.CENTER,
      borders: {
        top: {
          style: BorderStyle.DOUBLE,
          size: 3,
          color: '#000000',
        },
        bottom: {
          style: BorderStyle.DOUBLE,
          size: 3,
          color: '#000000',
        },
        left: {
          style: BorderStyle.DOUBLE,
          size: 3,
          color: '#000000',
        },
        right: {
          style: BorderStyle.DOUBLE,
          size: 3,
          color: '#000000',
        },
      },
      width: {
        size: 130,
        type: WidthType.PERCENTAGE,
      },
    });

    // header numbers
    reportItem.numbers.headers = new Table({
      rows: [
        new TableRow({
          children: [
            new TableCell({
              children: [
                new Paragraph({
                  alignment: AlignmentType.CENTER,
                  text: 'عدد المدرسين',
                  heading: HeadingLevel.HEADING_1,
                  shading:{fill: "FFFF00"}
                }),
              ],
              borders: {
                top: {
                  style: BorderStyle.DOUBLE,
                  size: 3,
                  color: '#000000',
                },
                bottom: {
                  style: BorderStyle.DOUBLE,
                  size: 3,
                  color: '#000000',
                },
                left: {
                  style: BorderStyle.DOUBLE,
                  size: 3,
                  color: '#000000',
                },
                right: {
                  style: BorderStyle.DOUBLE,
                  size: 3,
                  color: '#000000',
                },
              },
            }),
            new TableCell({
              children: [
                new Paragraph({
                  alignment: AlignmentType.CENTER,
                  text: 'عدد المدارس',
                  heading: HeadingLevel.HEADING_1,
                  shading:{fill: "FFFF00"}
                }),
              ],
              borders: {
                top: {
                  style: BorderStyle.DOUBLE,
                  size: 3,
                  color: '#000000',
                },
                bottom: {
                  style: BorderStyle.DOUBLE,
                  size: 3,
                  color: '#000000',
                },
                left: {
                  style: BorderStyle.DOUBLE,
                  size: 3,
                  color: '#000000',
                },
                right: {
                  style: BorderStyle.DOUBLE,
                  size: 3,
                  color: '#000000',
                },
              },
            }),
          ],
        }),
        ...reportItem.numbers.rowData,
      ],
      alignment: AlignmentType.CENTER,
      borders: {
        top: {
          style: BorderStyle.DOUBLE,
          size: 3,
          color: '#000000',
        },
        bottom: {
          style: BorderStyle.DOUBLE,
          size: 3,
          color: '#000000',
        },
        left: {
          style: BorderStyle.DOUBLE,
          size: 3,
          color: '#000000',
        },
        right: {
          style: BorderStyle.DOUBLE,
          size: 3,
          color: '#000000',
        },
      },
      width: {
        size: 130,
        type: WidthType.PERCENTAGE,
      },
    })

    reportData.push(reportItem);
  });


  let designTable:any[] = [];



  // loop first to push total first
  designTable.push(new Paragraph({
    alignment: AlignmentType.CENTER,
    text: 'اجمالى المدرسين الغير موزعين',
    heading: HeadingLevel.TITLE,
    pageBreakBefore: true,
  }));
  designTable.push(totalNumbers.headers);



  reportData.forEach(item=>{
    designTable.push(item.paragraphs);
    designTable.push(item.details.headers);
    designTable.push(item.numbers.headers);
  });
return designTable;
}



  exportDoc(childern:any[]){
    const document = new Document({
      sections: [
        {
          children: childern,
        },
      ],
    });
    Packer.toBlob(document).then((blob) => {
      saveAs(blob, `تقرير اجمالى مدرسين لم يوزعوا.docx`);
    });
  
  }

  async getGovData(){
    this.pageProps.message = '';
    this.pageProps.isLoading=true;
    try {
      const result: any = await this.dashboardService.GetGovData();
      if (result.success) {
        this.pageProps.govs = result.returnObject;
      } else {
        this.pageProps.message = result.arabicMessage;
      }
    } catch ({ error, statusText }) {
      this.pageProps.message = 'حدث خطأ ما حاول مره اخري';
    }
    this.pageProps.isLoading = false;
  }
}
