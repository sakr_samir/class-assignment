import { Injectable } from '@angular/core';
import { saveAs } from 'file-saver';

import { UnAssignedTeachersPageProps } from '../interfaces/UnAssignedTeachers.PageProps.interface';
import { DashboardService } from '../services/dashboard.service';
import * as Excel from "exceljs/dist/exceljs.min.js";
import * as ExcelProper from "exceljs";
import { Workbook } from 'exceljs';

@Injectable({
  providedIn: 'root',
})
export class UnAssignedTeachersPageXls {
    pageProps:UnAssignedTeachersPageProps={
        isLoading:false,
        message:'',
        allData:[],
        GovBookData:{},
        govs:[],
        selectedGov:0,
        totalNumbers:[]
    };
  constructor(private dashboardService: DashboardService) {}

async SetUnAssignedTeachersXls(govId:number) {
  this.pageProps.message = '';
    this.pageProps.isLoading=true;
    try {
      const result: any = await this.dashboardService.GetUnAssignedTeachers(govId);
      if (result.success) {
        this.pageProps.allData = result.returnObject.reportData;
        this.pageProps.totalNumbers = result.returnObject.totalNumbers;
        this.generateExcel();
      } else {
        this.pageProps.message = result.arabicMessage;
      }
    } catch ({ error, statusText }) {
      this.pageProps.message = 'حدث خطأ ما حاول مره اخري';
    }
    this.pageProps.isLoading = false;

  }

  async SetGovBooksXls(govId:number) {
    this.pageProps.message = '';
      this.pageProps.isLoading=true;
      try {
        const result: any = await this.dashboardService.GetGovBookReport(govId);
        if (result.success) {
          this.pageProps.GovBookData = result.returnObject;
          this.generateGovBookExcel();
        } else {
          this.pageProps.message = result.arabicMessage;
        }
      } catch ({ error, statusText }) {
        this.pageProps.message = 'حدث خطأ ما حاول مره اخري';
      }
      this.pageProps.isLoading = false;
  
    }

  async getGovData(){
    this.pageProps.message = '';
    this.pageProps.isLoading=true;
    try {
      const result: any = await this.dashboardService.GetGovData();
      if (result.success) {
        this.pageProps.govs = result.returnObject;
      } else {
        this.pageProps.message = result.arabicMessage;
      }
    } catch ({ error, statusText }) {
      this.pageProps.message = 'حدث خطأ ما حاول مره اخري';
    }
    this.pageProps.isLoading = false;
  }

  generateTotal(workbook:ExcelProper.Workbook){
    /// start total ///
    const totalTitle = 'تقرير إجمالى المعلمين الغير موزعين على فصول';
    const TotalHeader = ["المديرية", "عدد المدارس", "عدد المعلمين"];
    const totalData =  this.pageProps.totalNumbers;
    let totalWorksheet = workbook.addWorksheet('الاجمالى');
    //Add Row and formatting
    let totalTitleRow = totalWorksheet.addRow([totalTitle]);
    totalTitleRow.font = { name: 'Comic Sans MS', family: 4, size: 18, underline: 'double', bold: true }
    totalTitleRow.alignment = { vertical: 'middle', horizontal: 'center', wrapText: true };
    totalWorksheet.addRow([]);
    totalWorksheet.addRow(['Date : ' + new Date()])
    
    totalWorksheet.mergeCells('A1:C2');
    //Blank Row 
    totalWorksheet.addRow([]);
    //Add Header Row
    let totalHeaderRow = totalWorksheet.addRow(TotalHeader);
    // // Cell Style : Fill and Border
    totalHeaderRow.eachCell((cell, number) => {
      cell.fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: 'FFFFFF00' },
        bgColor: { argb: 'FF0000FF' },
      }
      cell.font = {size:16};
      cell.alignment = { vertical: 'middle', horizontal: 'center', wrapText: true };
      cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
    })
    // Add Data and Conditional Formatting
    totalData.forEach(d => {
      let totalDataRow = totalWorksheet.addRow(d);
      totalDataRow.eachCell((cell, number) => {
        cell.font = {size:14};
        cell.alignment = { vertical: 'middle', horizontal: 'center', wrapText: true };
        cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
      })
    });
    totalWorksheet.getColumn(1).width = 30;
    totalWorksheet.getColumn(2).width = 30;
    totalWorksheet.getColumn(3).width = 30;
    //Footer Row
    let footerRow = totalWorksheet.addRow(['هذا التقرير خاص بإجمالى أعداد المعلمين الغير موزعين على فصول']);
    footerRow.getCell(1).fill = {
      type: 'pattern',
      pattern: 'solid',
      fgColor: { argb: 'FFCCFFE5' }
    };
    footerRow.alignment =  { vertical: 'middle', horizontal: 'center', wrapText: true };
    footerRow.getCell(1).border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
    //Merge Cells
    totalWorksheet.mergeCells(`A${footerRow.number}:C${footerRow.number}`);
    return workbook;
  }

  generateDetails(workbook:ExcelProper.Workbook){
    this.pageProps.allData.forEach(gov=>{
      /// **** start gov *****///

      /// start total ///
    const totalTitle = "' "+ gov.govName + " '";

    //const TotalHeader = ["المادة","اسم المدرس", "كود المدرس", "اسم المدرسة ","كود المدرسة", "المرحلة ", "الادارة "];
    const TotalHeader = ["الادارة","المرحلة","كود المدرسة","اسم المدرسة","كود المعلم","اسم المعلم","المادة"];
    const totalData =  gov.teachers;
    let totalWorksheet = workbook.addWorksheet(gov.govName);
    //Add Row and formatting
    let totalTitleRow = totalWorksheet.addRow([totalTitle]);
    totalTitleRow.font = { name: 'Comic Sans MS', family: 4, size: 18, underline: 'double', bold: true }
    totalTitleRow.alignment ={ vertical: 'middle', horizontal: 'center', wrapText: true }
    totalWorksheet.addRow([]);
    totalWorksheet.addRow(['Date : ' + new Date()])
    
    totalWorksheet.mergeCells('A1:G2');
    //Blank Row 
    totalWorksheet.addRow([]);
    //Add Header Row
    let totalHeaderRow = totalWorksheet.addRow(TotalHeader);
    // // Cell Style : Fill and Border
    totalHeaderRow.eachCell((cell, number) => {
      cell.fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: 'FFFFFF00' },
        bgColor: { argb: 'FF0000FF' },
      }
      cell.font = {size:16};
      cell.alignment = { vertical: 'middle', horizontal: 'center', wrapText: true };
      cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
    })
    // Add Data and Conditional Formatting
    totalData.forEach(d => {
      let totalDataRow = totalWorksheet.addRow(d);
      totalDataRow.eachCell((cell, number) => {
        cell.font = {size:14};
        cell.alignment = { vertical: 'middle', horizontal: 'center', wrapText: true };
        cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
      })
    });
    totalWorksheet.getColumn(1).width = 30;
    totalWorksheet.getColumn(2).width = 30;
    totalWorksheet.getColumn(3).width = 30;
    totalWorksheet.getColumn(4).width = 30;
    totalWorksheet.getColumn(5).width = 30;
    totalWorksheet.getColumn(6).width = 30;
    totalWorksheet.getColumn(7).width = 30;
    //// **** total *** ////
    let totalRow = totalWorksheet.addRow(['عدد المدارس','عدد المعلمين']);

    totalRow.eachCell((cell, number) => {
      cell.fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: 'FFFFFF00' },
        bgColor: { argb: 'FFCCFFE5' },
      }
      cell.font = {size:16};
      cell.alignment = { vertical: 'middle', horizontal: 'center', wrapText: true };
      cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
    });
    totalRow.alignment =  { vertical: 'middle', horizontal: 'center', wrapText: true };
    let totalItemDataRow = totalWorksheet.addRow(gov.totalNumbers);
    totalItemDataRow.eachCell((cell, number) => {
      cell.font = {size:16};
      cell.alignment = { vertical: 'middle', horizontal: 'center', wrapText: true };
      cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
    });

    
    //Footer Row
    let footerRow = totalWorksheet.addRow(['هذا التقرير خاص بعرض تفصيلى للمعلمين الغير موزعين على فصول']);
    footerRow.getCell(1).fill = {
      type: 'pattern',
      pattern: 'solid',
      fgColor: { argb: 'FFCCFFE5' }
    };
    footerRow.alignment =  { vertical: 'middle', horizontal: 'center', wrapText: true };
    footerRow.getCell(1).border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
    //Merge Cells
    totalWorksheet.mergeCells(`A${footerRow.number}:G${footerRow.number}`);

      /// **** end gov *****///
    });

    
    return workbook;
  }

  generateExcel() {
    let workbook = new Workbook();
    workbook = this.generateTotal(workbook);
    workbook = this.generateDetails(workbook);
    //Generate Excel File with given name
    workbook.xlsx.writeBuffer().then((data) => {
      let blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
      saveAs(blob, 'تقرير معلمين لم يوزعوا.xlsx');
    })
  }

generateGovBookExcel() {
    const totalTitle = 'تقرير تسليم الكتب الدراسية بالمديرية';
    const TotalHeader = ["عدد الطلاب الغير مستلمة", "عدد الطلاب المستلمة", "عدد الطلاب الكلى"];
    let workbook = new Workbook();
    let totalWorksheet = workbook.addWorksheet('الاجمالى');

    let totalTitleRow = totalWorksheet.addRow([totalTitle]);
    totalTitleRow.font = { name: 'Comic Sans MS', family: 4, size: 18, underline: 'double', bold: true }
    totalTitleRow.alignment = { vertical: 'middle', horizontal: 'center', wrapText: true };
    totalWorksheet.addRow([]);
    totalWorksheet.addRow(['Date : ' + new Date()])
    
    totalWorksheet.mergeCells('A1:C2');

    totalWorksheet.addRow([]);

    let totalHeaderRow = totalWorksheet.addRow(TotalHeader);

    totalHeaderRow.eachCell((cell, number) => {
      cell.fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: 'FFFFFF00' },
        bgColor: { argb: 'FF0000FF' },
      }
      cell.font = {size:16};
      cell.alignment = { vertical: 'middle', horizontal: 'center', wrapText: true };
      cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
    })

    let totalDataRow = totalWorksheet.addRow([this.pageProps.GovBookData.notHasBooks, this.pageProps.GovBookData.hasBooks, this.pageProps.GovBookData.total]);
    
    totalDataRow.eachCell((cell, number) => {
      cell.font = {size:14};
      cell.alignment = { vertical: 'middle', horizontal: 'center', wrapText: true };
      cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
    });

    // Format columns
    totalWorksheet.getColumn(1).width = 30;
    totalWorksheet.getColumn(2).width = 30;
    totalWorksheet.getColumn(3).width = 30;

    let footerRow = totalWorksheet.addRow(['هذا التقرير خاص بإجمالى أعداد الطلاب الكلى والمستلمين الكتب الدراسية وغير المستلمين']);
    footerRow.getCell(1).fill = {
      type: 'pattern',
      pattern: 'solid',
      fgColor: { argb: 'FFCCFFE5' }
    };
    footerRow.alignment =  { vertical: 'middle', horizontal: 'center', wrapText: true };
    footerRow.getCell(1).border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }

    workbook.xlsx.writeBuffer().then((data) => {
      let blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
      saveAs(blob, 'تقرير تسليم الكتب الدراسية بالمديرية.xlsx');
    })
}


}
