import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { StorageService } from '../shared/services/storage.service';
import { UserRole } from '../user/interfaces/user-roles.enum';
import { UnAssignedTeachersPageProps } from './interfaces/UnAssignedTeachers.PageProps.interface';
import { NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { UnAssignedTeachersPageXls } from './pages/unAssignedTeachers.page.xls';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  @ViewChild('Status') Status: TemplateRef<any>;
  pageProps:UnAssignedTeachersPageProps;
  constructor(private unAssignedTeachersPage:UnAssignedTeachersPageXls,private storage: StorageService,private router: Router,private modal: NgbModal) {
    this.pageProps = this.unAssignedTeachersPage.pageProps;
   }

  ngOnInit(): void {
    let userData = this.storage.getNonPrimitive('user_access_data');
    if (userData.roleName !=  UserRole.SuperAdmin){
      this.router.navigateByUrl('/classes').then(() => {
        window.location.reload();
      });
    }
    else{
      this.unAssignedTeachersPage.getGovData().finally(()=>{
        if (this.pageProps.message !== '') {
          this.modal.open(this.Status);
        }
      });
    }
    
  }
  exportClassDetials() {
    this.unAssignedTeachersPage.SetUnAssignedTeachersXls(this.pageProps.selectedGov).finally(()=>{
      if (this.pageProps.message !== '') {
        this.modal.open(this.Status);
      }
    });

   //this.unAssignedTeachersPage.generateExcel();
  }

  exportGovBookDetials() {
    if(this.pageProps.selectedGov && +this.pageProps.selectedGov > 0){
      this.unAssignedTeachersPage.SetGovBooksXls(this.pageProps.selectedGov).finally(()=>{
        if (this.pageProps.message !== '') {
          this.modal.open(this.Status);
        }
      });
    }
    else{
      this.pageProps.message = 'حدد مديرية أولا';
      this.modal.open(this.Status);
    }
    

   //this.unAssignedTeachersPage.generateExcel();
  }
}
