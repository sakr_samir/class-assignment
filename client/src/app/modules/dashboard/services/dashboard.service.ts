import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { EnvService } from '@app/env.service';
import { IResultViewModel } from '@app/modules/shared/interfaces/result.interface';


@Injectable({
  providedIn: 'root',
})
export class DashboardService {
  private baseUrl;
  constructor(private http: HttpClient, private environment: EnvService) {
    this.baseUrl = `${environment.baseApiUrl}/Report`;
  }

  GetUnAssignedTeachers(govId:number) {
    return this.http
      .get<IResultViewModel>(
        `${this.baseUrl}/GetUnAssignedTeachers?govId=${govId}`
      )
      .toPromise();
  }

  GetGovBookReport(govId:number) {
    return this.http
      .get<IResultViewModel>(
        `${this.baseUrl}/GetReportGovBookData?govId=${govId}`
      )
      .toPromise();
  }

  GetGovData() {
    return this.http
      .get<IResultViewModel>(
        `${this.baseUrl}/GetAllGovs`
      )
      .toPromise();
  }

}
