import { Goverment } from './Goverment.interfece';
import{ UnAssignedTeachers} from './UnAssignedTeachers.interface';

export interface UnAssignedTeachersPageProps {
  isLoading: boolean;
  message: string;
  //teachersList: UnAssignedTeachers[];
  totalNumbers: any[];
  allData:any[];
  govs:[];
  selectedGov:number;
  GovBookData:any;
}
