import { UnAssignedTeachers } from "./UnAssignedTeachers.interface";

export interface Goverment{
    govId:number;
    govName:string
    teachers:UnAssignedTeachers[]
    totalSchools:number;
    totalTeachers:number;
  }