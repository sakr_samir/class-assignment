export interface UnAssignedTeachers{
  dist_Name: string;
  gov_Desc: string;
  gov_Id: number;
  stage_Desc: string;
  sch_Code: number;
  sch_Name: string;
  emp_Name: string;
  emp_Code: number;
  sub_Name:string;
}
