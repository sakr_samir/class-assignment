export interface ReportData{
    numbers:{headers,rowData:any[]},
    details:{headers,rowData:any[]},
    paragraphs:any,
    TotalNumbers:{headers,rowData:any[]},
  }