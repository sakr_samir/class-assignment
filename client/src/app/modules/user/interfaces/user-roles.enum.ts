export enum UserRole {
  Admin = 'Admin',
  SuperAdmin = 'Super Admin',
  SchoolAdmin = 'School Admin',
}
