export interface LoginPageProps {
  isLogging: boolean;
  errorMessage: string;
  passwordType: string;
}
