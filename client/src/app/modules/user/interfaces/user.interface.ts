import { IResultViewModel } from '@app/modules/shared/interfaces/result.interface';

export interface IUserResultViewModel extends IResultViewModel {
  returnObject: User;
}
interface User {
  id: string;
  firstName: string;
  lastName: string;
  email: string;
  roleName: string;
  firstTime: boolean;
  token: string;
  schoolId: number;
}
