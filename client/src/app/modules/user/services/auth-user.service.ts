import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { EnvService } from '@app/env.service';
import { StorageService } from '@app/modules/shared/services/storage.service';
import { SSoLoginVM } from '../interfaces/sso-login,interface';

@Injectable({
  providedIn: 'root',
})
export class AuthUserService {
  private TOKEN_DATA_KEY = 'user_access_token';
  private USER_DATA_KEY = 'user_access_data';
  private USER_AUTH = 'user_authenticated';
  private USER_ROLE = 'User_role';
  private USER_NAME = 'User_name';
  private User_School_Data = 'user_school_data';
  private baseUrl;

  constructor(
    private http: HttpClient,
    private storage: StorageService,
    private router: Router,
    private environment: EnvService
  ) {
    this.baseUrl = `${environment.baseApiUrl}/User`;
  }

  ssoLogin(obj:SSoLoginVM): Promise<any>{
    return this.http
      .post<any>(`${this.baseUrl}/SSOLogin`, obj)
      .toPromise();
  }

  login(userCredentials: { email: string; password: string }): Promise<any> {
    return this.http
      .post<any>(`${this.baseUrl}/LoginAdmin`, userCredentials)
      .toPromise();
  }
  saveUserData(userDataKey) {
    this.storage.setMultipleItems([
      { key: this.USER_DATA_KEY, value: userDataKey },
      { key: this.TOKEN_DATA_KEY, value: userDataKey.token },
      { key: this.USER_AUTH, value: 'true' },
      { key: this.USER_ROLE, value: userDataKey.roleName },
      { key: this.USER_NAME, value: userDataKey.fullName },
      { key: this.User_School_Data, value: userDataKey.userSchoolData },
    ]);
  }

  getUserData(): any {
    return this.storage.get(this.USER_DATA_KEY);
  }
  getUserSchoolData(): any {
    return this.storage.get(this.User_School_Data);
  }
  isUserAuthenticated(): boolean {
    return !!this.storage.get(this.USER_AUTH);
  }
  getUserRole(): string {
    return this.storage.get(this.USER_ROLE);
  }
  getUserName(): string {
    return this.storage.get(this.USER_NAME);
  }
  getTokenFromStorage(): string {
    return this.storage.get(this.TOKEN_DATA_KEY);
  }

  logout() {
    let language = this.storage.get('user-language');
    this.storage.clear();
    this.storage.set('user-language', language);
    //this.router.navigateByUrl('/auth/login');
  }
}
