import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { AuthUserService } from './auth-user.service';
import { map, catchError } from 'rxjs/operators';

@Injectable()
export class AuthUserInterceptor implements HttpInterceptor {
  constructor(private authUserService: AuthUserService) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const token = this.authUserService.getTokenFromStorage();
    if (token && !request.url.includes('cloudinary')) {
      request = request.clone({
        headers: request.headers.set('Authorization', 'Bearer ' + token),
      });
    }

    if (
      !request.headers.has('Content-Type') &&
      !request.url.includes('cloudinary')
    ) {
      request = request.clone({
        headers: request.headers.set('Content-Type', 'application/json'),
      });
    }

    return next.handle(request).pipe(
      map((event: HttpEvent<any>) => {
        return event;
      }),
      catchError((error: HttpErrorResponse) => {
        if (error.status === 401) {
          this.authUserService.logout();
        }
        return throwError(error);
      })
    );
  }
}
