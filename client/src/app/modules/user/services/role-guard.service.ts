import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot } from '@angular/router';
import { StorageService } from '@app/modules/shared/services/storage.service';
import { AuthUserService } from './auth-user.service';

@Injectable({
  providedIn: 'root',
})
export class RoleGuardService {
  constructor(private auth: AuthUserService, private storage: StorageService) {}

  canActivate(route: ActivatedRouteSnapshot): boolean {
    // this will be passed from the route config
    // on the data property
    const expectedRole = route.data.expectedRole;
    const anotherRole = route.data.anotherRole;
    let userData = this.storage.getNonPrimitive('user_access_data');

    if (
      this.auth.isUserAuthenticated() &&
      (userData.roleName === expectedRole || userData.roleName === anotherRole)
    ) {
      return true;
    } else {
      this.auth.logout();
      return false;
    }
  }
}
