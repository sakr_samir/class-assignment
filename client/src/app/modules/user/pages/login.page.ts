import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { LanguageHandlerService } from '@app/modules/shared/services/language-handler.service';
import { TranslateService } from '@ngx-translate/core';
import { LoginPageProps } from '../interfaces/login-page-props.interface';
import { SSoLoginVM } from '../interfaces/sso-login,interface';
import { UserRole } from '../interfaces/user-roles.enum';
import { AuthUserService } from '../services/auth-user.service';

@Injectable({
  providedIn: 'root',
})
export class LoginPage {
  pageProps: LoginPageProps = {
    isLogging: false,
    errorMessage: '',
    passwordType: 'password',
  };
  constructor(
    private authUserService: AuthUserService,
    private router: Router,
    private languageHandler: LanguageHandlerService,
    private translate: TranslateService
  ) { }
  // onPageEnter(): void {
  //   const userRole = this.authUserService.getUserRole();
  //   const isLoggedIn = this.authUserService.isUserAuthenticated();
  //   if (isLoggedIn) {
  //     if (userRole === UserRole.SchoolAdmin) {
  //       this.router.navigateByUrl('/classes');
  //     }
  //     if (userRole === UserRole.SuperAdmin) {
  //       this.router.navigateByUrl('/dashboard');
  //     }
  //   }
  // }

  async ssoLogin(SchoolCode: number, SchoolId: number) {
    try {
      this.pageProps.errorMessage = '';
      this.pageProps.isLogging = true;
      let obj: SSoLoginVM = {
        BCH_CODE: SchoolCode,
        security_code: SchoolId
      }
      this.authUserService.logout();
      const response = await this.authUserService.ssoLogin(obj);
      const result = response.returnObject;
      if (response.success) {
        this.authUserService.saveUserData(result);
        if (result.roleName === UserRole.SchoolAdmin) {
          this.router.navigateByUrl('/classes').then(() => {
            window.location.reload();
          });
        }
        // if (result.roleName === UserRole.SuperAdmin) {
        //   this.router.navigateByUrl('/dashboard').then(() => {
        //     window.location.reload();
        //   });
        // }
        if (result.roleName === UserRole.SuperAdmin) {
          this.router.navigateByUrl('/dashboard').then(() => {
            window.location.reload();
          });
        }
      } else {
        window.location.href="http://student.emis.gov.eg/new/index.aspx";
        // this.pageProps.errorMessage = response.arabicMessage;
      }
    } catch ({ error, statusText }) {
      window.location.href="http://student.emis.gov.eg/new/index.aspx";
      // this.pageProps.errorMessage = 'حدث خطأ ما حاول مره اخري';
    }
    this.pageProps.isLogging = false;
  }

  // async loginUser(loginForm: FormGroup) {
  //   if (loginForm.invalid) {
  //     this.pageProps.errorMessage = this.translate.instant('loginErrorMessage');
  //   }
  //   const languageKey = this.languageHandler.getLanguageKey();
  //   try {
  //     this.pageProps.isLogging = true;
  //     const response = await this.authUserService.login(loginForm.value);
  //     const result = response.returnObject;
  //     if (response.success) {
  //       this.pageProps.isLogging = false;
  //       this.authUserService.saveUserData(result);
  //       if (result.roleName === UserRole.SchoolAdmin) {
  //         this.router.navigateByUrl('/classes').then(() => {
  //           window.location.reload();
  //         });
  //       }
  //       if (result.roleName === UserRole.SuperAdmin) {
  //         this.router.navigateByUrl('/dashboard').then(() => {
  //           window.location.reload();
  //         });
  //       }
  //     } else {
  //       this.pageProps.isLogging = false;
  //       this.pageProps.errorMessage =
  //         languageKey === 'en'
  //           ? 'The username or password is incorrect'
  //           : 'اسم المستخدم او كلمة المرور غير صحيحة';
  //     }
  //   } catch ({ error, statusText }) {
  //     this.pageProps.isLogging = false;
  //     if (languageKey === 'en') {
  //       this.pageProps.errorMessage =
  //         error.message != null ? error.message : statusText;
  //     } else {
  //       this.pageProps.errorMessage =
  //         error.arabicMessage != null ? error.arabicMessage : statusText;
  //     }
  //   }
  // }
}
