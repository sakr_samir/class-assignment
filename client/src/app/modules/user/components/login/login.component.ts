import { LoginPageProps } from './../../interfaces/login-page-props.interface';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoginPage } from '../../pages/login.page';
import { ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  pageProps: LoginPageProps;
  @ViewChild('Status') Status: TemplateRef<any>;


  constructor(private fb: FormBuilder, private loginPage: LoginPage
    , private route: ActivatedRoute, private modal: NgbModal) { }

  // "https://class.emis.gov.eg/?schid=123546&scode=1253600"

  ngOnInit(): void {
    this.pageProps = this.loginPage.pageProps;
    this.pageProps.errorMessage='';
    this.route.queryParams
      .subscribe(params => {
        let schoolCode = +params.scode;
        let schoolId = +params.schid;
        if (isNaN(schoolCode) || isNaN(schoolCode))
          // this.pageProps.errorMessage = 'حدث خطأ ما حاول مره اخري !!';
          window.location.href="http://student.emis.gov.eg/new/index.aspx";
          else
          this.loginPage.ssoLogin(schoolCode, schoolId);
      }
      );
    //this.initForm();
  }

  // initForm() {
  //   this.loginForm = this.fb.group({
  //     Email: [
  //       '',
  //       Validators.compose([
  //         Validators.required,
  //         Validators.pattern(
  //           /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  //         ),
  //         Validators.minLength(3),
  //         Validators.maxLength(320),
  //       ]),
  //     ],
  //     Password: [
  //       '',
  //       Validators.compose([
  //         Validators.required,
  //         Validators.maxLength(100),
  //         Validators.minLength(8),
  //         Validators.pattern(
  //           /(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$-/:-?{-~!"^_`\[\]@%$*])(?=.{8,})/
  //         ),
  //       ]),
  //     ],
  //   });
  // }

  // loginUser() {
  //   this.loginPage.loginUser(this.loginForm);
  // }

  // HideShowPassword() {
  //   if (this.pageProps.passwordType == 'text')
  //     this.pageProps.passwordType = 'password';
  //   else this.pageProps.passwordType = 'text';
  // }
}
