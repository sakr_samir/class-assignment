import { StudentVM } from './../../interfaces/student.interface';
import { StudentPage } from './../../pages/student.page';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { StudentPageProps } from '../../interfaces/studentPageProps.interface';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-list-students',
  templateUrl: './list-students.component.html',
  styleUrls: ['./list-students.component.scss'],
})
export class ListStudentsComponent implements OnInit {
  modalReference: any;
  items = [1, 1, 1, 1, 1, 1, 1, 1, 1];
  grids = [2, 2, 2, 2];
  page = 1;
  assign: string;
  searchByName = false;
  searchByCode = false;
  searchByID = false;
  searchByClass = false;
  filterAssignBook = 0;
  pageProps: StudentPageProps;
  studentName = '';
  className = '';
  studentCode: number;
  selectedStudentCodeForBook: number;
  
  @ViewChild('Status') Status: TemplateRef<any>;
  @ViewChild('books') booksPopup: TemplateRef<any>;
  active = 0;
  constructor(
    private modalService: NgbModal,
    private studentPage: StudentPage,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    // let selectedGrade = this.activatedRoute.snapshot.params['selectedGradeCode'];
    // console.log(selectedGrade);

    this.activatedRoute.queryParams.subscribe((params) => {
      let selectedGrade = +params.selectedGradeCode;
      if (params.assign == undefined || params.assign == null) {
        this.assign = '';
      } else {
        this.assign = params.assign;
      }

      if (!(isNaN(selectedGrade) || selectedGrade === 0)) {
        this.studentPage
          .getSchoolGrades(selectedGrade, this.assign)
          .finally(() => {
            this.active = this.pageProps.schoolGrades.findIndex(
              (x) => x.gradeCode === selectedGrade
            );
          });
      } else {
        this.studentPage.OnPageEnter(0, this.assign);
        this.active = 0;
      }
    });
    // this.activatedRoute.params.subscribe((params: Params) => {
    //   //this.classDetailsPage.onPageEnter(params['id']);

    // });

    this.pageProps = this.studentPage.pageProps;
  }

  ChangeGrade(gradeCode: number) {
    this.page = 1;
    this.studentPage
      .getGradeStudents(
        gradeCode,
        this.studentCode,
        this.studentName,
        this.className,
        this.page,
        this.assign,
        this.filterAssignBook,
      )
      .finally(() => {
        if (this.pageProps.message != '') this.modalService.open(this.Status);
      });
  }
  
  assignStudentClass(student: StudentVM) {
    this.studentPage
      .assignClass(+student.classCode, +student.studentCode)
      .finally(() => {
        if (this.pageProps.message != '') this.modalService.open(this.Status);
      });
  }

  onPageChange(page: number, gradeCode: number) {
    (this.studentCode = null),
      (this.studentName = null),
      (this.className = null);
    this.studentPage
    .getGradeStudents(
      gradeCode,
      this.studentCode,
        this.studentName,
        this.className,
        page,
        this.assign,
        this.filterAssignBook,
      )
      .finally(() => {
        if (this.pageProps.message != '') this.modalService.open(this.Status);
      });
  }

  search(gradeCode: number) {
    this.studentPage
      .getGradeStudents(
        gradeCode,
        this.studentCode,
        this.studentName,
        this.className,
        1,
        this.assign,
       this.filterAssignBook
      // 2
      )
      .finally(() => {
        if (this.pageProps.message != '') {
          this.studentCode = null;
          this.studentName = null;
          this.className = null;
          this.modalService.open(this.Status);
        }
      });
  }

  close() {
    this.modalService.dismissAll();
  }

  opneModal(content, size?) {
    this.modalReference = this.modalService.open(content, {
      size: size ? size : 'xl',
      centered: true,
    });
  }

  selectAllBooks() {
    this.pageProps.schoolGrades[this.active].books.forEach(book => book.isSelected = true);
  }

  openBooks(student: StudentVM){
    this.selectedStudentCodeForBook = student.studentCode;
    this.pageProps.schoolGrades[this.active].books.forEach(c=>c.isSelected = false);

    student.books.forEach(_=>{
      let x = this.pageProps.schoolGrades[this.active].books.find(x=> x.bookId == _);
      if(x){
        x.isSelected = true;
      }
    });
    this.modalService.open(this.booksPopup);
  }

  getBookTermName(term){
    if(term == 1){
      return 'ترم أول ';
    }
    else if(term ==2)
      {
        return 'ترم ثانى';
      }
      else{
        return 'ترم أول / ترم ثانى'
      }
  }

  SaveStudentBooks(){
    this.modalService.dismissAll();
    let selectedIds = [];
    this.pageProps.schoolGrades[this.active].books.forEach(x=>{
      if(x.isSelected)
        {
          selectedIds.push(x.bookId);
        }
    })
    let body = {
      StudentCode:this.selectedStudentCodeForBook,
      SelectedBookIds:selectedIds
    };

    this.studentPage
      .SaveBooks(body)
      .finally(() => {
        if (this.pageProps.message != '') {
          this.studentCode = null;
          this.studentName = null;
          this.className = null;
          this.modalService.open(this.Status);
        }
        else{
          this.modalService.dismissAll();
          this.pageProps.students.find(_=>_.studentCode == this.selectedStudentCodeForBook).books = selectedIds;
        }
      });
    
  }

}
