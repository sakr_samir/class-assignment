import { ClassVM } from './../interfaces/class.interface';
import { StudentService } from './../services/student.service';
import { Injectable } from '@angular/core';
import { AuthUserService } from '@app/modules/user/services/auth-user.service';
import { StudentPageProps } from '../interfaces/studentPageProps.interface';
import { SearchStudentVM } from '../interfaces/searchStudentVM.interface';
import { AssignClassVM } from '../interfaces/assign-class.interface';

@Injectable({
  providedIn: 'root',
})
export class StudentPage {
  pageProps: StudentPageProps = {
    isLoading: false,
    message: '',
    pagesNumber: 1,
    schoolGrades: [],
    students: [],
    studentsCopy: []
  };
  constructor(
    private userService: AuthUserService,
    private studentService: StudentService
  ) { }

  OnPageEnter(selectedGardeCode: number,assign:string): void {
    this.getSchoolGrades(selectedGardeCode,assign);
  }
  getSchoolCode() {
    return JSON.parse(this.userService.getUserSchoolData()).scH_CODE;
  }
  async getSchoolGrades(selcetdGrade: number,assign:string) {
    this.pageProps.message = '';
    const schoolCode = this.getSchoolCode();
    this.pageProps.isLoading = true;
    try {
      const result: any = await this.studentService.GetSchoolGrades(
        schoolCode,
        selcetdGrade,
        assign
      );
      if (result.success) {
        this.pageProps.schoolGrades = result.returnObject.schoolGrades;
        this.pageProps.students = result.returnObject.students;
        this.pageProps.studentsCopy = JSON.parse(
          JSON.stringify(this.pageProps.students)
        );
        //this.pageProps.GradeBooks = result.returnObject.books;
        this.pageProps.pagesNumber = result.returnObject.pagesNumber;
      } else {
        this.pageProps.message = result.arabicMessage;
      }
    } catch ({ error, statusText }) {
      this.pageProps.message = 'حدث خطأ ما حاول مره اخري';
    }
    this.pageProps.isLoading = false;
  }
  async getGradeStudents(
    gradeCode?: any,
    studentCode?: any,
    studentName?: any,
    className?: any,
    PageIndex?: any,
    assign?:any,
    BookAssignFilter?:any
  ) {
    this.pageProps.message = '';
    this.pageProps.isLoading = true;
    try {
      let obj: SearchStudentVM = {
        BookAssignFilter:+BookAssignFilter,
        pageIndex: PageIndex ?? 1,
        pageLimit: 10,
        SchoolCode: this.getSchoolCode(),
        GradeCode: +gradeCode,
        StudentCode: +studentCode,
        StudentName: studentName,
        ClassName:className,
        ClassIds: this.searchOnClasses(gradeCode, className),
        assign:assign,
      };
      const response: any = await this.studentService.GetGradeStudents(obj);
      if (response.success) {
        this.pageProps.students = response.returnObject.students;
        this.pageProps.studentsCopy = JSON.parse(
          JSON.stringify(this.pageProps.students)
        );
        this.pageProps.pagesNumber = response.returnObject.pagesNumber;
      } else this.pageProps.message = response.message;
    } catch (err) {
      this.pageProps.message = 'Something went wrong please, try again later';
    }
    this.pageProps.isLoading = false;
  }
  async assignClass(ClassCode: number, StudentCode: number) {
    this.pageProps.message = '';
    this.pageProps.isLoading = true;
    let obj: AssignClassVM = {
      classCode: ClassCode,
      studentCode: StudentCode,
    };
    try {
      const result: any = await this.studentService.assignClass(obj);
      if (result.success) {
        this.pageProps.students.find(
          (ww) => ww.studentCode == StudentCode
        ).classCode = ClassCode;
        this.pageProps.studentsCopy = JSON.parse(
          JSON.stringify(this.pageProps.students)
        );
      } else {
        this.pageProps.students = this.pageProps.studentsCopy;
        this.pageProps.message = result.arabicMessage;
      }
    } catch ({ error, statusText }) {
      this.pageProps.message = 'حدث خطأ ما حاول مره اخري';
      this.pageProps.students = this.pageProps.studentsCopy;
    }
    this.pageProps.isLoading = false;
  }

  searchOnClasses(gradeCode: number, ClassName?: any) {
    if (ClassName != '') {
      let gradeClasses = this.pageProps.schoolGrades.find(
        (ww) => ww.gradeCode == gradeCode
      ).classes;
      let result = this.transform(gradeClasses, ClassName) as ClassVM[];
      let classesIds: number[] = [];
      result.forEach((element) => {
        classesIds.push(element.classId);
      });      
      return classesIds;
    }
  }

  transform(value: any, args?: any): any {    
    if (!value) return null;
    if (!args) return value;

    args = args.replace(/\s/g, "").toLowerCase();
    return value.filter(function (data) {
      return JSON.stringify(data).toLowerCase().includes(args);
    });
  }

  async SaveBooks(body) {
    this.pageProps.message = '';
    this.pageProps.isLoading = true;
   
    try {
      const result: any = await this.studentService.SaveBooks(body);
      if (result.success) {
        
      } else {
       
        this.pageProps.message = result.arabicMessage;
      }
    } catch ({ error, statusText }) {
      this.pageProps.message = 'حدث خطأ ما حاول مره اخري';
      this.pageProps.students = this.pageProps.studentsCopy;
    }
    this.pageProps.isLoading = false;
  }
}
