export interface SearchStudentVM {
  SchoolCode: number;
  GradeCode: number;
  StudentCode: number;
  StudentName: string;
  ClassName:string;
  ClassIds: number[];
  pageIndex: number;
  pageLimit: number;
  assign:string;
  BookAssignFilter:number;
}
