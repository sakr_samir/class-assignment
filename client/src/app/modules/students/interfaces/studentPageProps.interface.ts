import { SchoolGradeVM } from './school-grade.interface';
import { StudentVM } from './student.interface';

export interface StudentPageProps {
  message: string;
  isLoading: boolean;
  schoolGrades: SchoolGradeVM[];
  students: StudentVM[];
  studentsCopy: StudentVM[];
  pagesNumber: number;
  
}
