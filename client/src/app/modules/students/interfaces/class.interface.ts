export interface ClassVM {
  classId: number;
  className: string;
}
