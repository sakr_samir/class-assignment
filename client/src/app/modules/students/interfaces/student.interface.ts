export interface StudentVM {
  studentCode: number;
  studentName: string;
  classCode: number;
  books:any[];
}
