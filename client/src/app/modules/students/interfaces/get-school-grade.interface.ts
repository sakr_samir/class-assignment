import { StudentVM } from './student.interface';
import { SchoolGradeVM } from './school-grade.interface';
export interface GetSchoolGradeVM {
  schoolGrades: SchoolGradeVM[];
  students: StudentVM[];
  pagesNumber: number;
}
