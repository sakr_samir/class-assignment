import { StudentVM } from './student.interface';
export interface StudentDataVM {
  students: StudentVM[];
  pagesNumber: number;
}
