import { ClassVM } from './class.interface';
export interface SchoolGradeVM {
  gradeCode: number;
  gradeName: string;
  classes: ClassVM[];
  books:any [];
}
