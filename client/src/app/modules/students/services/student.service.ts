import { Params } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IResultViewModel } from '@app/modules/shared/result.interface';
import { AssignClassVM } from '../interfaces/assign-class.interface';
import { SearchStudentVM } from '../interfaces/searchStudentVM.interface';
import { EnvService } from '@app/env.service';

@Injectable({
  providedIn: 'root',
})
export class StudentService {
  private baseUrl;
  constructor(private http: HttpClient, private environment: EnvService) {
    this.baseUrl = `${environment.baseApiUrl}/Student`;
  }
  GetSchoolGrades(schoolCode: number, selcetdGrade: number,assign:string) {
    return this.http
      .get<IResultViewModel>(
        `${this.baseUrl}/GetSchoolGrades?SchoolCode=${schoolCode}&&selectedGradeCode=${selcetdGrade}&&assign=${assign}`
      )
      .toPromise();
  }
  GetGradeStudents(SearchObj: SearchStudentVM): Promise<IResultViewModel> {
    return this.http
      .post<IResultViewModel>(`${this.baseUrl}/GetGradeStudents`, SearchObj)
      .toPromise();
  }
  assignClass(obj: AssignClassVM) {
    return this.http.post(`${this.baseUrl}/AssginClass`, obj).toPromise();
  }

  SaveBooks(obj) {
    return this.http.post(`${this.baseUrl}/SaveBooks`, obj).toPromise();
  }
}
