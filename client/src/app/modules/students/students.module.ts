import { SharedModule } from './../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StudentsRoutingModule } from './students-routing.module';
import { ListStudentsComponent } from './components/list-students/list-students.component';
import { FormsModule } from '@angular/forms';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

@NgModule({
  declarations: [ListStudentsComponent],
  imports: [
    CommonModule,
    MatProgressSpinnerModule,
    SharedModule,
    StudentsRoutingModule,
    FormsModule,
  ],
})
export class StudentsModule {}
