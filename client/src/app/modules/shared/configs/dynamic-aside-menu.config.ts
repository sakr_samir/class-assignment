export const DynamicAsideMenuConfig = {
  items: [
    // {
    //   title: 'Reports',
    //   root: true,
    //   icon: 'flaticon2-architecture-and-city',
    //   svg: './assets/media/svg/icons/Design/Layers.svg',
    //   page: '/teachers/reportStudents',
    //   bullet: 'dot'
    // },
    {
      title: 'المعلمين (تحديد مواد المعلمين)',
      root: true,
      icon: 'fas fa-user',
      svg: '',
      page: '/teachers',
      bullet: 'dot',
    },
    {
      title: 'الفصول الدراسيه (تحديد المعلمين على الفصول)',
      root: true,
      icon: 'fas fa-graduation-cap',
      svg:'',
      page: '/classes',
      bullet: 'dot',
    },
    {
      title: 'الطلاب',
      root: true,
      icon: 'fas fa-users',
      svg: '',
      page: '/students',
      bullet: 'dot',
    },
    // {
    //   title: 'pray',
    //   root: true,
    //   bullet: 'dot',
    //   page: '/islamic',
    //   icon: 'flaticon2-digital-marketing',
    //   svg: './assets/media/svg/icons/Design/Layers.svg',
    //   submenu: [
    //     {
    //       title: 'list',
    //       page: '/islamic/list'
    //     },
    //     {
    //       title: 'edit',
    //       page: '/islamic/edit'
    //     },
    //   ]
    // },
    // { section: 'Applications' },
  ],
};
