import { Component, OnInit, AfterViewInit } from '@angular/core';
import KTLayoutQuickUser from '../../../../../../../assets/js/layout/extended/quick-user';
import KTLayoutHeaderTopbar from '../../../../../../../assets/js/layout/base/header-topbar';
import { KTUtil } from '../../../../../../../assets/js/components/util';
import { LayoutService } from '@app/modules/shared/services/layout.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AuthUserService } from '@app/modules/user/services/auth-user.service';
import { StorageService } from '@app/modules/shared/services/storage.service';

@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.scss'],
})
export class TopbarComponent implements OnInit, AfterViewInit {
  extrasUserDisplay: boolean;
  extrasLanguagesDisplay: boolean;
  adminName: string;
  extrasUserLayout: 'offcanvas' | 'dropdown';

  constructor(
    private layout: LayoutService,
    private modal: NgbModal,
    private authUserService: AuthUserService,
    private storageServic: StorageService
  ) {}

  ngOnInit(): void {
    // topbar extras
    this.extrasUserDisplay = this.layout.getProp('extras.user.display');
    this.extrasUserLayout = this.layout.getProp('extras.user.layout');
    this.extrasLanguagesDisplay = this.layout.getProp(
      'extras.languages.display'
    );
    this.adminName = this.authUserService.getUserName();
  }

  ngAfterViewInit(): void {
    KTUtil.ready(() => {
      if (this.extrasUserDisplay && this.extrasUserLayout === 'offcanvas') {
        KTLayoutQuickUser.init('kt_quick_user');
      }
      KTLayoutHeaderTopbar.init('kt_header_mobile_topbar_toggle');
    });
  }

  openModal(content) {
    this.modal.open(content);
  }
  async logout() {
    this.modal.dismissAll();
    this.authUserService.logout();
    window.location.href = 'http://student.emis.gov.eg/new/index.aspx';
  }
}
