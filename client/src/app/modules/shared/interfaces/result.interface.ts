export interface IResultViewModel {
  success: boolean;
  message: string;
  arabicMessage: string;
  returnObject?: any;
  statusCode: number;
}
