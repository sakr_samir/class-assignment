import { EditTeacherInfoComponent } from './../classes/components/edit-teacher-info/edit-teacher-info.component';
import { InlineSVGModule } from 'ng-inline-svg';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutComponent } from './components/_layout/layout.component';
import { NgbTooltipModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MultiSelectModule } from 'primeng/multiselect';
import { ArabicNumeralsPipe } from './services/arabic-numerals.pipe';

@NgModule({
  declarations: [LayoutComponent, EditTeacherInfoComponent,ArabicNumeralsPipe],
  imports: [
    CommonModule,
    NgbTooltipModule,
    InlineSVGModule,
    NgbModule,
    MultiSelectModule,
  ],
  exports: [
    NgbModule,
    MultiSelectModule,
    InlineSVGModule,
    EditTeacherInfoComponent,
    ArabicNumeralsPipe
  ],
})
export class SharedModule {}
