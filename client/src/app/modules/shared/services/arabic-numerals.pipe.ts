import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'arabicNumerals'
})
export class ArabicNumeralsPipe implements PipeTransform {
  transform(value: string | number): string {
    const arabicNumeralsMap = ['٠', '١', '٢', '٣', '٤', '٥', '٦', '٧', '٨', '٩'];
    return value.toString().replace(/\d/g, (digit) => arabicNumeralsMap[parseInt(digit, 10)]);
  }
}
