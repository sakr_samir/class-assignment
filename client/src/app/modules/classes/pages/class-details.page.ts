import { ConfirmClass } from './../interfaces/confirm-class.interface';
import { AuthUserService } from './../../user/services/auth-user.service';
import { Subject, SubjectVM } from './../interfaces/subject.interface';
import { Class } from '../interfaces/class.interface';
import { Grade } from '../interfaces/grade.interface';
import { Injectable } from '@angular/core';
import { ClassDetailsPageProps } from '../interfaces/class-detailsPageProps.interface';
import { Student } from '../interfaces/student.interface';
import { ClassService } from '../services/class.service';
import { UpdateTeachersVM } from '../interfaces/update-teachers.interface';
import { saveAs } from 'file-saver';
import {
  AlignmentType,
  BorderStyle,
  Document,
  HeadingLevel,
  HeightRule,
  Packer,
  Paragraph,
  Table,
  TableCell,
  TableLayoutType,
  TableRow,
  TabStopPosition,
  TabStopType,
  TextRun,
  WidthType,
} from 'docx';

@Injectable({
  providedIn: 'root',
})
export class ClassDetailsPage {
  pageProps: ClassDetailsPageProps = {
    class: {
      id: 0,
      name: '',
      studentsConfirmed: false,
      teachersConfirmed: false,
    },
    isLoading: false,
    message: '',
    classId: 0,
    classSubjects: [],
    classSubjectsCopy: [],
    teachersConfirmed: false,
    studentsConfirmed: false,
    students: [],
    teacherSubjects: [],
    books:[]
  };

  constructor(
    private classService: ClassService,
    private userService: AuthUserService
  ) {}

  onPageEnter(classId: number) {
    this.pageProps.classId = classId;
    this.getClassTeachers();
  }

  async getClassStudents() {
    this.pageProps.message = '';
    this.pageProps.isLoading = true;
    try {
      const response: any = await this.classService.getClassStudents(
        this.pageProps.classId
      );
      this.pageProps.students = response.returnObject;
      if (!response.success) this.pageProps.message = response.arabicMessage;
    } catch (err) {
      this.pageProps.message = 'حدث خطأ ما حاول مره اخري';
    }
    this.pageProps.isLoading = false;
  }

  async getClassTeachers() {
    this.pageProps.message = '';
    const schoolCode = JSON.parse(
      this.userService.getUserSchoolData()
    ).scH_CODE;
    this.pageProps.isLoading = true;
    try {
      const result: any = await this.classService.getClassDetails(
        this.pageProps.classId,
        schoolCode
      );
      if (result.success) {
        this.pageProps.class = result.returnObject.classDatails;
        this.pageProps.books = result.returnObject.books;
        this.pageProps.classSubjects = result.returnObject.teachersList;
        this.bindTeachersubjects();
        console.log(this.pageProps.teacherSubjects);
        this.pageProps.classSubjectsCopy = JSON.parse(
          JSON.stringify(this.pageProps.classSubjects)
        );
      } else {
        this.pageProps.message = result.arabicMessage;
      }
    } catch ({ error, statusText }) {
      this.pageProps.message = 'حدث خطأ ما حاول مره اخري';
    }
    this.pageProps.isLoading = false;
  }

  async updateSubjects(subject: SubjectVM) {
    this.pageProps.message = '';
    this.pageProps.isLoading = true;
    const oldTeachers = this.pageProps.classSubjectsCopy.find(
      (ww) => ww.id === subject.id
    )?.selectedTeachers;
    const adding = [];
    const notChanged = [];
    const removed = [];
    subject.selectedTeachers.forEach((element) => {
      if (
        oldTeachers.findIndex(
          (ww) => ww.teacherSubjectId === element.teacherSubjectId
        ) === -1
      ) {
        adding.push(element);
      } else {
        notChanged.push(element);
      }
    });
    oldTeachers.forEach((element) => {
      if (
        notChanged.findIndex(
          (ee) => ee.teacherSubjectId === element.teacherSubjectId
        ) === -1
      ) {
        removed.push(element);
      }
    });
    try {
      const updateVm: UpdateTeachersVM = {
        classId: +this.pageProps.classId,
        addTeachers: adding,
        removeTeachers: removed,
      };
      if (
        updateVm.addTeachers.length > 0 ||
        updateVm.removeTeachers.length > 0
      ) {
        const result: any = await this.classService.updateTeachers(updateVm);
        if (result.success) {
          this.pageProps.classSubjects.find(
            (ww) => ww.id === subject.id
          ).selectedTeachers = subject.selectedTeachers;
          this.pageProps.classSubjectsCopy = JSON.parse(
            JSON.stringify(this.pageProps.classSubjects)
          );
          this.bindTeachersubjects();
        } else {
          this.pageProps.message = result.arabicMessage;
          this.pageProps.classSubjects = this.pageProps.classSubjectsCopy;
          this.bindTeachersubjects();
          this.pageProps.classSubjectsCopy = JSON.parse(
            JSON.stringify(this.pageProps.classSubjects)
          );
        }
      }
    } catch ({ error, statusText }) {
      this.pageProps.message = 'حدث خطأ ما حاول مره اخري';
      this.pageProps.classSubjects = this.pageProps.classSubjectsCopy;
      this.bindTeachersubjects();
      this.pageProps.classSubjectsCopy = JSON.parse(
        JSON.stringify(this.pageProps.classSubjects)
      );
    }
    this.pageProps.isLoading = false;
  }
  bindTeachersubjects() {
    this.pageProps.teacherSubjects = [];
    this.pageProps.classSubjects.forEach((sub) => {
      sub.selectedTeachers.forEach((teacher) => {
        let indexOfTeacher = this.pageProps.teacherSubjects.findIndex(
          (x) => x.code == teacher.teacherCode
        );
        if (indexOfTeacher == -1) {
          this.pageProps.teacherSubjects.push({
            code: teacher.teacherCode,
            name: teacher.name,
            subject: sub.name,
          });
        } else {
          this.pageProps.teacherSubjects[indexOfTeacher].subject =
            this.pageProps.teacherSubjects[indexOfTeacher].subject +
            '-' +
            sub.name;
        }
      });
    });
  }
  async ConfirmClassData() {
    this.pageProps.message = '';
    this.pageProps.isLoading = true;
    try {
      let obj: ConfirmClass = {
        classId: this.pageProps.class.id,
        studentsConfirmed: this.pageProps.studentsConfirmed,
        teachersConfirmed: this.pageProps.teachersConfirmed,
      };
      const response: any = await this.classService.confirmClass(obj);
      if (response.success) {
        this.pageProps.class.studentsConfirmed =
          this.pageProps.studentsConfirmed;
        this.pageProps.class.teachersConfirmed =
          this.pageProps.teachersConfirmed;
        this.pageProps.studentsConfirmed = false;
        this.pageProps.teachersConfirmed = false;
      } else {
        this.pageProps.studentsConfirmed = false;
        this.pageProps.teachersConfirmed = false;
        this.pageProps.message = response.arabicMessage;
      }
    } catch (err) {
      this.pageProps.message = 'حدث خطأ ما حاول مره اخري';
    }
    this.pageProps.isLoading = false;
  }

  exportClassDetials() {
    let schoolData = JSON.parse(this.userService.getUserSchoolData());
    let schoolName = `${schoolData.scH_DESC} - ${schoolData.reL_DESC} ( ${schoolData.scH_CODE} )`;
    let studentRows = [];
    this.pageProps.students.forEach((element) => {
      studentRows.push(
        new TableRow({
          children: [
            new TableCell({
              children: [
                new Paragraph({
                  alignment: AlignmentType.CENTER,
                  text: element.name,
                  heading: HeadingLevel.HEADING_2,
                }),
              ],
              borders: {
                top: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
                bottom: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
                left: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
                right: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
              },
            }),
            new TableCell({
              children: [
                new Paragraph({
                  alignment: AlignmentType.CENTER,
                  text: element.code.toString(),
                  heading: HeadingLevel.HEADING_2,
                }),
              ],
              borders: {
                top: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
                bottom: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
                left: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
                right: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
              },
            }),
          ],
        })
      );
    });
    let teacherRows = [];
    this.pageProps.teacherSubjects.forEach((teacher) => {
      teacherRows.push(
        new TableRow({
          children: [
            new TableCell({
              children: [
                new Paragraph({
                  alignment: AlignmentType.CENTER,
                  text: teacher.subject,
                  heading: HeadingLevel.HEADING_2,
                }),
              ],
              borders: {
                top: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
                bottom: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
                left: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
                right: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
              },
            }),

            new TableCell({
              children: [
                new Paragraph({
                  alignment: AlignmentType.CENTER,
                  text: teacher.name,
                  heading: HeadingLevel.HEADING_2,
                }),
              ],
              borders: {
                top: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
                bottom: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
                left: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
                right: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
              },
            }),
            new TableCell({
              children: [
                new Paragraph({
                  alignment: AlignmentType.CENTER,
                  text: teacher.code.toString(),
                  heading: HeadingLevel.HEADING_2,
                }),
              ],
              borders: {
                top: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
                bottom: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
                left: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
                right: {
                  style: BorderStyle.SINGLE,
                  size: 3,
                  color: '#000000',
                },
              },
            }),
          ],
        })
      );
    });
    const document = new Document({
      sections: [
        {
          children: [
            new Paragraph({
              alignment: AlignmentType.RIGHT,
              text: `المديريه :    ${schoolData.goV_DESC}`,
              heading: HeadingLevel.HEADING_1,
            }),
            new Paragraph({
              alignment: AlignmentType.RIGHT,
              text: `الإداره التعليميه :   ${schoolData.disT_NAME}`,
              heading: HeadingLevel.HEADING_1,
            }),
            new Paragraph({
              alignment: AlignmentType.RIGHT,
              text: `المدرسه :  ${schoolName}`,
              heading: HeadingLevel.HEADING_1,
            }),
            new Paragraph({
              alignment: AlignmentType.RIGHT,
              text: `الفصل الدراسي :  ${this.pageProps.class.name}`,
              heading: HeadingLevel.HEADING_1,
            }),
            new Paragraph({
              alignment: AlignmentType.CENTER,
              text: `المعلمين`,
              heading: HeadingLevel.TITLE,
            }),
            new Table({
              rows: [
                new TableRow({
                  children: [
                    new TableCell({
                      children: [
                        new Paragraph({
                          alignment: AlignmentType.CENTER,
                          text: ' اسم المادة',
                          heading: HeadingLevel.HEADING_1,
                        }),
                      ],
                      borders: {
                        top: {
                          style: BorderStyle.DOUBLE,
                          size: 3,
                          color: '#000000',
                        },
                        bottom: {
                          style: BorderStyle.DOUBLE,
                          size: 3,
                          color: '#000000',
                        },
                        left: {
                          style: BorderStyle.DOUBLE,
                          size: 3,
                          color: '#000000',
                        },
                        right: {
                          style: BorderStyle.DOUBLE,
                          size: 3,
                          color: '#000000',
                        },
                      },
                    }),

                    new TableCell({
                      children: [
                        new Paragraph({
                          alignment: AlignmentType.CENTER,
                          text: 'المعلم المختص',
                          heading: HeadingLevel.HEADING_1,
                        }),
                      ],
                      borders: {
                        top: {
                          style: BorderStyle.DOUBLE,
                          size: 3,
                          color: '#000000',
                        },
                        bottom: {
                          style: BorderStyle.DOUBLE,
                          size: 3,
                          color: '#000000',
                        },
                        left: {
                          style: BorderStyle.DOUBLE,
                          size: 3,
                          color: '#000000',
                        },
                        right: {
                          style: BorderStyle.DOUBLE,
                          size: 3,
                          color: '#000000',
                        },
                      },
                    }),
                    new TableCell({
                      children: [
                        new Paragraph({
                          alignment: AlignmentType.CENTER,
                          text: 'كود المعلم',
                          heading: HeadingLevel.HEADING_1,
                        }),
                      ],
                      borders: {
                        top: {
                          style: BorderStyle.DOUBLE,
                          size: 3,
                          color: '#000000',
                        },
                        bottom: {
                          style: BorderStyle.DOUBLE,
                          size: 3,
                          color: '#000000',
                        },
                        left: {
                          style: BorderStyle.DOUBLE,
                          size: 3,
                          color: '#000000',
                        },
                        right: {
                          style: BorderStyle.DOUBLE,
                          size: 3,
                          color: '#000000',
                        },
                      },
                    }),
                  ],
                }),
                ...teacherRows,
              ],
              alignment: AlignmentType.CENTER,
              borders: {
                top: {
                  style: BorderStyle.DOUBLE,
                  size: 3,
                  color: '#000000',
                },
                bottom: {
                  style: BorderStyle.DOUBLE,
                  size: 3,
                  color: '#000000',
                },
                left: {
                  style: BorderStyle.DOUBLE,
                  size: 3,
                  color: '#000000',
                },
                right: {
                  style: BorderStyle.DOUBLE,
                  size: 3,
                  color: '#000000',
                },
              },
              width: {
                size: 80,
                type: WidthType.PERCENTAGE,
              },
              layout: TableLayoutType.AUTOFIT,
            }),
            new Paragraph({
              alignment: AlignmentType.CENTER,
              text: `الطلاب`,
              heading: HeadingLevel.TITLE,
            }),
            new Table({
              rows: [
                new TableRow({
                  children: [
                    new TableCell({
                      children: [
                        new Paragraph({
                          alignment: AlignmentType.CENTER,
                          text: 'اسم الطالب',
                          heading: HeadingLevel.HEADING_1,
                        }),
                      ],
                      borders: {
                        top: {
                          style: BorderStyle.DOUBLE,
                          size: 3,
                          color: '#000000',
                        },
                        bottom: {
                          style: BorderStyle.DOUBLE,
                          size: 3,
                          color: '#000000',
                        },
                        left: {
                          style: BorderStyle.DOUBLE,
                          size: 3,
                          color: '#000000',
                        },
                        right: {
                          style: BorderStyle.DOUBLE,
                          size: 3,
                          color: '#000000',
                        },
                      },
                    }),
                    new TableCell({
                      children: [
                        new Paragraph({
                          alignment: AlignmentType.CENTER,
                          text: 'كود الطالب',
                          heading: HeadingLevel.HEADING_1,
                        }),
                      ],
                      borders: {
                        top: {
                          style: BorderStyle.DOUBLE,
                          size: 3,
                          color: '#000000',
                        },
                        bottom: {
                          style: BorderStyle.DOUBLE,
                          size: 3,
                          color: '#000000',
                        },
                        left: {
                          style: BorderStyle.DOUBLE,
                          size: 3,
                          color: '#000000',
                        },
                        right: {
                          style: BorderStyle.DOUBLE,
                          size: 3,
                          color: '#000000',
                        },
                      },
                    }),
                  ],
                }),
                ...studentRows,
              ],
              alignment: AlignmentType.CENTER,
              borders: {
                top: {
                  style: BorderStyle.DOUBLE,
                  size: 3,
                  color: '#000000',
                },
                bottom: {
                  style: BorderStyle.DOUBLE,
                  size: 3,
                  color: '#000000',
                },
                left: {
                  style: BorderStyle.DOUBLE,
                  size: 3,
                  color: '#000000',
                },
                right: {
                  style: BorderStyle.DOUBLE,
                  size: 3,
                  color: '#000000',
                },
              },
              width: {
                size: 80,
                type: WidthType.PERCENTAGE,
              },
            }),
          ],
        },
      ],
    });

    Packer.toBlob(document).then((blob) => {
      saveAs(blob, `الفصل الدراسي ${this.pageProps.class.name}.docx`);
    });
  }

  async SaveClassBooks(body:any) {
    this.pageProps.message = '';
    this.pageProps.isLoading = true;
    try {
        const result: any = await this.classService.SaveClassBooks(body);
        if (result.success) {
        } else {
          this.pageProps.message = result.arabicMessage;
        }
      }
     catch ({ error, statusText }) {
      this.pageProps.message = 'حدث خطأ ما حاول مره اخري';
    }
    this.pageProps.isLoading = false;
  }
}
