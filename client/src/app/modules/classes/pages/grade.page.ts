import { GradeService } from '../services/grade.service';
import { Injectable } from '@angular/core';
import { GradePageProps } from '../interfaces/gradePageProps.interface';
import { AuthUserService } from '@app/modules/user/services/auth-user.service';
import { SetUpClass } from '../interfaces/SetUpClass.interface';
import { Subject } from '../interfaces/subject.interface';
import { Class } from '../interfaces/class.interface';

@Injectable({
  providedIn: 'root',
})
export class GradePage {
  pageProps: GradePageProps = {
    stage: [],
    selectedGrade: {
      code: 0,
      name: '',
      classes: [],
      //books:[],
      classNum: 0,
      subjects: [],
      unassignedStudentNum:0,
      totalStudentNum:0
    },
    isLoading: false,
    message: '',
    maxNumberOfClasses: 0,
    unassignedTeacherToClassNum:0,
    unassignedTeacherToSubjectNum:0,
    unassignedStudentNum:0
  };
  school_code: number;
  constructor(
    private gradeService: GradeService,
    private userService: AuthUserService
  ) {
    this.school_code = JSON.parse(
      this.userService.getUserSchoolData()
    ).scH_CODE;
  }

  onPageEnter() {
    //let schoolCode = JSON.parse(this.userService.getUserSchoolData()).scH_CODE;
    this.getGradesClasses(this.school_code).finally(() => {
      this.pageProps.selectedGrade = this.pageProps.stage[0];
    });
  }

  async getGradesClasses(schoolCode: number) {
    this.pageProps.isLoading = true;
    try {
      const response: any = await this.gradeService.getGradeClasses(schoolCode);
      if (response.success) {
        this.pageProps.stage = response.returnObject.grades;
        this.pageProps.maxNumberOfClasses =
          response.returnObject.maxNumberOfClasses;
        this.pageProps.unassignedTeacherToClassNum=response.returnObject.unassignedTeacherToClassNum;
        this.pageProps.unassignedTeacherToSubjectNum=response.returnObject.unassignedTeacherToSubjectNum;
        this.pageProps.unassignedStudentNum=response.returnObject.unassignedStudentNum;
      } else {
        this.pageProps.message = response.arabicMessage;
      }
    } catch (err) {
      this.pageProps.message = 'حدث خطأ ما حاول مره اخري';
    }

    this.pageProps.isLoading = false;
  }

  async SetUpClasses(setUpClass: SetUpClass) {
    this.pageProps.isLoading = true;
    try {
      const response: any = await this.gradeService.createClasses(setUpClass);
      if (response.success) {
        if (!setUpClass.OnlySave) {
          this.pageProps.selectedGrade.classes = response.returnObject;
          this.pageProps.stage.find(
            (grade) => grade.code === setUpClass.GradeCode
          ).classes = response.returnObject;
          this.pageProps.selectedGrade.classNum =
            this.pageProps.selectedGrade.classes.length;
        } else {
          this.pageProps.selectedGrade.classNum = setUpClass.NumOfClasses;
          this.pageProps.stage.find(
            (grade) => grade.code === setUpClass.GradeCode
          ).classNum = setUpClass.NumOfClasses;
        }
      } else {
        this.pageProps.message = response.arabicMessage;
      }
    } catch (err) {
      this.pageProps.message = 'حدث خطأ ما حاول مره أخرى !';
    }
    this.pageProps.isLoading = false;
  }

  async getGradeSubjects(schoolCode: number) {
    this.pageProps.isLoading = true;
    try {
      const response: any = await this.gradeService.getGradeSubjects(
        schoolCode
      );
      if (response.success) {
        this.pageProps.selectedGrade.subjects = response.returnObject;
      } else {
        this.pageProps.message = response.arabicMessage;
      }
    } catch (err) {
      this.pageProps.message = 'حدث خطأ ما حاول مره اخري';
    }

    this.pageProps.isLoading = false;
  }

  async AssignTeacherOnSetUp(subjects: Subject[]) {
    this.pageProps.isLoading = true;
    try {
      const response: any = await this.gradeService.AssignTeacherOnSetUp(
        subjects
      );
      if (response.success) {
        let assignedClasses: Class[] = response.returnObject;
        let classesIds: number[] = this.pageProps.selectedGrade.classes.map(
          function (cl) {
            return cl.id;
          }
        );
        assignedClasses.forEach((cl) => {
          if (classesIds.includes(cl.id)) {
            this.pageProps.selectedGrade.classes.find(
              (x) => x.id === cl.id
            ).totalTeachers = assignedClasses.find(
              (x) => x.id === cl.id
            ).totalTeachers;
          }
        });
        //this.pageProps.selectedGrade.classes = response.returnObject;
      } else {
        this.pageProps.message = response.arabicMessage;
      }
    } catch (err) {
      this.pageProps.message = 'حدث خطأ ما حاول مره اخري';
    }
    this.pageProps.isLoading = false;
  }
}
