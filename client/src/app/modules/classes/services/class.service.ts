import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { EnvService } from '@app/env.service';
import { IResultViewModel } from '@app/modules/shared/result.interface';
import { ConfirmClass } from '../interfaces/confirm-class.interface';
import { UpdateTeachersVM } from '../interfaces/update-teachers.interface';

@Injectable({
  providedIn: 'root',
})
export class ClassService {
  private baseUrl;

  constructor(private http: HttpClient, private environment: EnvService) {
    this.baseUrl = `${environment.baseApiUrl}/Class`;
  }

  getClassStudents(classId: number): Promise<IResultViewModel> {
    return this.http
      .get<IResultViewModel>(`${this.baseUrl}/GetStudents?classId=${classId}`)
      .toPromise();
  }
  getClassDetails(classId: number,schooCode): Promise<IResultViewModel> {
    return this.http
      .get<IResultViewModel>(`${this.baseUrl}/GetTeachers?ClassId=${classId}&SchoolCode=${schooCode}`)
      .toPromise();
  }
  updateTeachers(teacherVM: UpdateTeachersVM) {    
    return this.http
      .post(`${this.baseUrl}/UpdateTeachers`, teacherVM)
      .toPromise();
  }
  confirmClass(obj: ConfirmClass) {    
    return this.http
      .post(`${this.baseUrl}/ConfirmClass`, obj)
      .toPromise();
  }

  SaveClassBooks(body: any) {    
    return this.http
      .post(`${this.baseUrl}/SaveBooksClass`, body)
      .toPromise();
  }
}
