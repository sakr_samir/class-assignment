import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { EnvService } from '@app/env.service';
import { IResultViewModel } from '@app/modules/shared/result.interface';
import { SetUpClass } from '../interfaces/SetUpClass.interface';

import { Subject } from '../interfaces/subject.interface';

@Injectable({
  providedIn: 'root',
})
export class GradeService {
  private baseUrl;

  constructor(private http: HttpClient, private environment: EnvService) {
    this.baseUrl = `${environment.baseApiUrl}/Class`;
  }

  getGradeClasses(schoolCode: number): Promise<IResultViewModel> {
    return this.http
      .get<IResultViewModel>(
        `${this.baseUrl}/GetClasses?SchoolCode=${schoolCode}`
      )
      .toPromise();
  }

  createClasses(setUpClass: SetUpClass): Promise<IResultViewModel> {
    return this.http
      .post<IResultViewModel>(`${this.baseUrl}/SetupClasses/`, setUpClass)
      .toPromise();
  }

  getGradeSubjects(schoolCode: number): Promise<IResultViewModel> {
    return this.http
      .get<IResultViewModel>(
        `${this.baseUrl}/GetSubjectsOnSetUp?SchoolCode=${schoolCode}`
      )
      .toPromise();
  }

  AssignTeacherOnSetUp(subjects: Subject[]): Promise<IResultViewModel> {
    return this.http
      .post<IResultViewModel>(`${this.baseUrl}/SetTeachersOnSetUp/`, subjects)
      .toPromise();
  }
}
