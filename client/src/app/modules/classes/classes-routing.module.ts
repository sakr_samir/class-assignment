import { EditTeacherInfoComponent } from './components/edit-teacher-info/edit-teacher-info.component';
import { ClassDetailsComponent } from './components/class-details/class-details.component';
import { ListClassesComponent } from './components/list-classes/list-classes.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: ListClassesComponent,
  },
  {
    path: 'details/:id',
    component: ClassDetailsComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ClassesRoutingModule {}
