import { SharedModule } from '@shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClassesRoutingModule } from './classes-routing.module';
import { ListClassesComponent } from './components/list-classes/list-classes.component';
import { CreateUpdateClassComponent } from './components/create-update-class/create-update-class.component';
import { AssignTeachersComponent } from './components/assign-teachers/assign-teachers.component';
import { AddStudentsComponent } from './components/add-students/add-students.component';
import { ClassDetailsComponent } from './components/class-details/class-details.component';
import { FormsModule } from '@angular/forms';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';


@NgModule({
  declarations: [
    ListClassesComponent,
    CreateUpdateClassComponent,
    AssignTeachersComponent,
    AddStudentsComponent,
    ClassDetailsComponent,
    // EditTeacherInfoComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    ClassesRoutingModule,
    FormsModule,
    MatProgressSpinnerModule,
  ],
})
export class ClassesModule {}
