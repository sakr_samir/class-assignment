import { Teacher, TeacherVM } from './teacher.interface';

export interface Subject {
  id: number;
  name: string;
  subjectTeachers: Teacher[];
}

export interface SubjectVM {
  id: number;
  name: string;
  subjectTeachers: TeacherVM[];
  selectedTeachers: TeacherVM[];
}
