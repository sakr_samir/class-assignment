export interface ConfirmClass{
    classId:number,
    teachersConfirmed:boolean,
    studentsConfirmed:boolean
}