import { Class, ClassDetails } from './class.interface';
import { Student } from './student.interface';
import { SubjectVM } from './subject.interface';
export interface ClassDetailsPageProps {
  class: ClassDetails;
  message: string;
  isLoading: boolean;
  classId: number;
  classSubjects: SubjectVM[];
  classSubjectsCopy: SubjectVM[];
  teachersConfirmed: boolean;
  studentsConfirmed: boolean;
  students: Student[];
  books: any[];
  teacherSubjects: { code: number; name: string; subject: string }[];
}
