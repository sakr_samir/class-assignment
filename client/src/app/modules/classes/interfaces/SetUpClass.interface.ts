export interface SetUpClass {
  GradeCode: number;
  NumOfClasses: number;
  SchoolCode: number;
  OnlySave: boolean;
}
