import { Teacher } from './teacher.interface';

export interface UpdateTeachersVM {
  classId: number;
  addTeachers: Teacher[];
  removeTeachers: Teacher[];
}