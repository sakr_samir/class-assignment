import { Grade } from './grade.interface';
export interface GradePageProps {
  stage: Grade[];
  selectedGrade: Grade;
  message: string;
  isLoading: boolean;
  maxNumberOfClasses: number;
  unassignedStudentNum:number;
  unassignedTeacherToClassNum:number;
  unassignedTeacherToSubjectNum:number;
}
