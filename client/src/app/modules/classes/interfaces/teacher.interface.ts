export interface Teacher {
  code: number;
  name: string;
  classesIds: number[];
  teacherSubjectId: number;
}

export interface TeacherVM {
  teacherSubjectId: number;
  teacherCode:number;
  name: string;
}
