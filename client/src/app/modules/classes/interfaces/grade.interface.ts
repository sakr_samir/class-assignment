import { Class } from './class.interface';
import { Subject } from './subject.interface';

export interface Grade {
  code: number;
  name: string;
  classes: Class[];
  //books: any[];
  classNum: number;
  subjects: Subject[];
  unassignedStudentNum:number;
  totalStudentNum:number;
}
