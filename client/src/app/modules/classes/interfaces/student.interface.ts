export interface Student {
  code: number;
  name: string;
}
