import { Student } from './student.interface';
import { Subject } from './subject.interface';

export interface Class {
  id: number;
  name: string;
  totalStudents: number;
  totalTeachers: number;
  students: Student[];
  // subjects: Subject[];
}

export interface ClassDetails {
  id: number;
  name: string;
  teachersConfirmed:boolean,
  studentsConfirmed:boolean
}
