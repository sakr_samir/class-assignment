import { Router } from '@angular/router';
import { SetUpClass } from './../../interfaces/SetUpClass.interface';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { GradePageProps } from '../../interfaces/gradePageProps.interface';
import { GradePage } from '../../pages/grade.page';
import { Grade } from '../../interfaces/grade.interface';

@Component({
  selector: 'app-list-classes',
  templateUrl: './list-classes.component.html',
  styleUrls: ['./list-classes.component.scss'],
})
export class ListClassesComponent implements OnInit {
  @ViewChild('addedSuccess', { static: false })
  private addedSuccess: any;

  modalReference: any;
  pageProps: GradePageProps;
  @ViewChild('Status') Status: TemplateRef<any>;
  @ViewChild('books') booksPopup: TemplateRef<any>;
  constructor(
    private modalService: NgbModal,
    private gradePage: GradePage,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.gradePage.onPageEnter();
    this.pageProps = this.gradePage.pageProps;
    //this.pageProps.selectedGrade = this.pageProps.stage[0];
  }

  close() {
    this.modalService.dismissAll();
  }

  opneModal(content, size?) {
    this.modalReference = this.modalService.open(content, {
      size: size ? size : 'xl',
      centered: true,
      backdrop : 'static',
      keyboard : false
    });
  }
  done(data: { NumOfClasses: number; Type: boolean }) {
    if (data.NumOfClasses) {
      let setupclass: SetUpClass = {
        GradeCode: this.pageProps.selectedGrade.code,
        NumOfClasses: +data.NumOfClasses,
        SchoolCode: this.gradePage.school_code,
        OnlySave: !data.Type,
      };

      if (setupclass.OnlySave) {
        if (setupclass.NumOfClasses != this.pageProps.selectedGrade.classNum) {
          this.gradePage.SetUpClasses(setupclass).finally(() => {
            if (this.pageProps.message !== '') {
              this.modalService.open(this.Status);
            }
          });
        }

        this.modalReference.close();
      } else {
        this.modalService.dismissAll();
        this.gradePage.SetUpClasses(setupclass).finally(() => {
          if (this.pageProps.message == '') {
            this.opneModal(this.addedSuccess, 'md');
          }
        });
      }

      // if you want to close first popup
      // this.modalReference.close();
    }
  }

  routeToStudents() {
    this.modalService.dismissAll();
    this.router.navigateByUrl('/students');
  }
  routeToHome() {
    this.modalService.dismissAll();
    this.router.navigateByUrl('/');
  }
  routeToStudentswithQQUery(queryString) {
    if(queryString == '')
    {
      this.router.navigateByUrl(
        `/students?selectedGradeCode=${this.pageProps.selectedGrade.code}`
      );
    }
    else{
      this.router.navigateByUrl(
        `/students?selectedGradeCode=${this.pageProps.selectedGrade.code}&&assign=${queryString}`
      );
    }
    
  }
  routeToTeachers(queryString) {
    if(queryString == '')
    {
      this.router.navigateByUrl('/teachers');
    }
    else{
      this.router.navigateByUrl(
        `/teachers?unassign=${queryString}`
      );
    }
  }

  SaveStudentBooks(){
    // this.modalService.dismissAll();
    // let selectedIds = [];
    // this.pageProps.schoolGrades[this.active].books.forEach(x=>{
    //   if(x.isSelected)
    //     {
    //       selectedIds.push(x.bookId);
    //     }
    // })
    // let body = {
    //   StudentCode:this.selectedStudentCodeForBook,
    //   SelectedBookIds:selectedIds
    // };

    // this.studentPage
    //   .SaveBooks(body)
    //   .finally(() => {
    //     if (this.pageProps.message != '') {
    //       this.studentCode = null;
    //       this.studentName = null;
    //       this.className = null;
    //       this.modalService.open(this.Status);
    //     }
    //     else{
    //       this.modalService.dismissAll();
    //       this.pageProps.students.find(_=>_.studentCode == this.selectedStudentCodeForBook).books = selectedIds;
    //     }
    //   });
    
  }



}
