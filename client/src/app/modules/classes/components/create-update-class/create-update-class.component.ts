import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { GradePage } from '../../pages/grade.page';

@Component({
  selector: 'app-create-update-class',
  templateUrl: './create-update-class.component.html',
  styleUrls: ['./create-update-class.component.scss'],
})
export class CreateUpdateClassComponent implements OnInit {
  @Output() addedSuccess: EventEmitter<{
    NumOfClasses: number;
    Type: boolean;
  }> = new EventEmitter<{ NumOfClasses: number; Type: boolean }>();
  numOfclassValue: number;
  constructor(private modalService: NgbModal, private gradePage: GradePage) {}

  pageProps;

  ngOnInit(): void {
    this.pageProps = this.gradePage.pageProps;
    this.numOfclassValue = this.pageProps.selectedGrade.classNum;
  }

  close() {
    this.modalService.dismissAll();
  }

  submit(type: boolean) {
    this.addedSuccess.emit({ NumOfClasses: this.numOfclassValue, Type: type });
  }
}
