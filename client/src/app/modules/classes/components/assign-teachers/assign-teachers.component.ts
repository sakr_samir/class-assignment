import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {
  Component,
  ElementRef,
  OnInit,
  QueryList,
  ViewChild,
  ViewChildren,
} from '@angular/core';
import { GradePage } from '../../pages/grade.page';
import { GradePageProps } from '../../interfaces/gradePageProps.interface';
import { Grade } from '../../interfaces/grade.interface';

@Component({
  selector: 'app-assign-teachers',
  templateUrl: './assign-teachers.component.html',
  styleUrls: ['./assign-teachers.component.scss'],
})
export class AssignTeachersComponent implements OnInit {
  pageProps: GradePageProps;
  items = [1, 1, 1, 1];
  grids = [2, 2, 2, 2];

  subId;
  constructor(private modalService: NgbModal, private gradePage: GradePage) {}

  ngOnInit(): void {
    this.gradePage.getGradeSubjects(this.gradePage.school_code);
    this.pageProps = this.gradePage.pageProps;
  }

  close() {
    this.modalService.dismissAll();
  }

  submit() {
    this.gradePage.AssignTeacherOnSetUp(this.pageProps.selectedGrade.subjects);
    this.close();
  }
}
