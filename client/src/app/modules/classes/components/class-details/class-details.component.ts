import { Subject } from './../../../teachers/interfaces/subject.interface';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ClassDetailsPage } from '../../pages/class-details.page';
import { ClassDetailsPageProps } from '../../interfaces/class-detailsPageProps.interface';
import { SubjectVM } from '../../interfaces/subject.interface';
import { GradePage } from '../../pages/grade.page';

@Component({
  selector: 'app-class-details',
  templateUrl: './class-details.component.html',
  styleUrls: ['./class-details.component.scss'],
})
export class ClassDetailsComponent implements OnInit {
  pageProps: ClassDetailsPageProps;
  selectedTeachers: number[] = [];
  subjects: Subject[];

  @ViewChild('Status') Status: TemplateRef<any>;
  @ViewChild('ConfirmTeacher') ConfirmTeacher: TemplateRef<any>;
  @ViewChild('ConfirmStudent') ConfirmStudent: TemplateRef<any>;
  @ViewChild('books') booksPopup: TemplateRef<any>;
  constructor(
    private modalService: NgbModal,
    private activatedRoute: ActivatedRoute,
    private classDetailsPage: ClassDetailsPage,
    private gradePage: GradePage,
    private router: Router
  ) {}

  ngOnInit(): void {
    let classId = this.activatedRoute.snapshot.params['id'];
    this.classDetailsPage.onPageEnter(classId);
    this.pageProps = this.classDetailsPage.pageProps;
    // this.activatedRoute.params.subscribe((params: Params) => {
    //   this.classDetailsPage.onPageEnter(params['id']);
    //   this.pageProps = this.classDetailsPage.pageProps;
    // });
  }

  getStudents() {
    this.classDetailsPage.getClassStudents().finally(() => {
      if (this.pageProps.message != '') this.modalService.open(this.Status);
    });
  }
  updateTeachers(subject: SubjectVM) {
    if(subject.selectedTeachers.length > 1){
      this.pageProps.message='يرجى تحديد معلم واحد فقط على المادة';
      this.modalService.open(this.Status);
      return;
    }
      
    this.classDetailsPage.updateSubjects(subject).finally(() => {
      if (this.pageProps.message != '') this.modalService.open(this.Status);
    });
  }

  opneModal(content, size?) {
    this.modalService.open(content, {
      size: size ? size : 'md',
      centered: true,
    });
  }

  close() {
    this.modalService.dismissAll();
  }
  routeToStudentswithQQUery() {
    this.router.navigateByUrl(
      `/students?selectedGradeCode=${this.gradePage.pageProps.selectedGrade.code}`
    );
  }
  confrimTeachers(status: boolean) {
    this.pageProps.teachersConfirmed = status;
    this.opneModal(this.ConfirmStudent);
  }

  exportClassData(status: boolean) {
    this.pageProps.studentsConfirmed = status;
    if (
      this.pageProps.studentsConfirmed !==
        this.pageProps.class.studentsConfirmed ||
      this.pageProps.teachersConfirmed !==
        this.pageProps.class.teachersConfirmed
    ) {
      this.classDetailsPage.ConfirmClassData().then(() => {
        if (this.pageProps.message != '') this.modalService.open(this.Status);
        else if (this.pageProps.students.length === 0) {
          this.classDetailsPage.getClassStudents().then(() => {
            this.classDetailsPage.exportClassDetials();
          });
        } else this.classDetailsPage.exportClassDetials();
      });
    } else if (this.pageProps.students.length === 0) {
      this.classDetailsPage.getClassStudents().then(() => {
        this.classDetailsPage.exportClassDetials();
      });
    } else this.classDetailsPage.exportClassDetials();
  }

  selectAllBooks() {
    this.pageProps.books.forEach(book => book.isSelected = true);
  }

  openBooks(){
    this.pageProps.books.forEach(c=>c.isSelected = false);

    // student.books.forEach(_=>{
    //   let x = this.pageProps.schoolGrades[this.active].books.find(x=> x.bookId == _);
    //   if(x){
    //     x.isSelected = true;
    //   }
    // });
    this.modalService.open(this.booksPopup);
  }

  getBookTermName(term){
    if(term == 1){
      return 'ترم أول ';
    }
    else if(term ==2)
      {
        return 'ترم ثانى';
      }
      else{
        return 'ترم أول / ترم ثانى'
      }
  }

  SaveStudentBooks(){
    this.modalService.dismissAll();
    let selectedIds = [];
    this.pageProps.books.forEach(x=>{
      if(x.isSelected)
        {
          selectedIds.push(x.bookId);
        }
    });

    let body = {
      ClassId:+this.pageProps.classId,
      BooksIds:selectedIds
    };

    this.classDetailsPage
      .SaveClassBooks(body)
      .finally(() => {
        if (this.pageProps.message != '') {
          this.modalService.open(this.Status);
        }
        else{
          this.modalService.open('تم الحفظ بنجاح');
          this.modalService.dismissAll();
        }
      });
    
  }
}
