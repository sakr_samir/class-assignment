import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-edit-teacher-info',
  templateUrl: './edit-teacher-info.component.html',
  styleUrls: ['./edit-teacher-info.component.scss'],
})
export class EditTeacherInfoComponent implements OnInit {
  grids = [1, 1, 1, 1];
  modalReference: any;
  constructor(private modalService: NgbModal) {}

  ngOnInit(): void {}
  close() {
    this.modalService.dismissAll();
  }

  opneModal(content, size?) {
    this.modalReference = this.modalService.open(content, {
      size: size ? size : 'md',
      centered: true,
    });
  }

  submit() {
    this.close();
  }
}
