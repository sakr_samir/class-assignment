import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-add-students',
  templateUrl: './add-students.component.html',
  styleUrls: ['./add-students.component.scss']
})
export class AddStudentsComponent implements OnInit {

  items = [1, 1, 1, 1, 1, 1, 1, 1, 1];
  grids = [2, 2, 2, 2];

  
  constructor(private modalService:NgbModal) { }

  ngOnInit(): void {
  }

  close(){
    this.modalService.dismissAll();
  }

  submit(){
    this.close()
  }


}
