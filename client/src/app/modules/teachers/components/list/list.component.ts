import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TeacherService } from '../../services/teacher.service';
import { AuthUserService } from '@app/modules/user/services/auth-user.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ListComponent implements OnInit {

  // pageProps: TeacherPageProps;
  @ViewChild('Status') Status: TemplateRef<any>;
  teacherList = [];
  statusMessage='';
  isLoading=false;


  constructor(private modal: NgbModal,
    private activatedRoute: ActivatedRoute,
    private _teacherService: TeacherService,
    private _userService: AuthUserService) { }

  ngOnInit(): void {
    // this.activatedRoute.queryParams.subscribe((params) => {
    //   let  unassign= params.unassign;
    //   if (unassign) {
    //     this.teacherPage.OnPageEnter(unassign);
    //   } else {
    //     this.teacherPage.OnPageEnter("all");
    //   }
    //   this.pageProps = this.teacherPage.pageProps;
    // });

    this.getTeachers();
  }



  async getTeachers()
  {
    try {
      this.isLoading = true;
      const schoolCode = JSON.parse(
        this._userService.getUserSchoolData()
      ).scH_CODE;
      const result: any = await this._teacherService.GetTeachers(schoolCode,'all');
      if (result.success) {
        this.groupTeachersByCode(result.returnObject);
        this.isLoading =false;
        } 
      else
      {
        this.statusMessage = result.arabicMessage;
        this.isLoading =false;
        this.modal.open(this.Status);
       }
    } catch (err) {  console.error('An error occurred:', err);
    
    }
  }

  groupTeachersByCode(teacherList) {
    this.teacherList = teacherList.reduce((acc, teacher) => {
      if (!acc[teacher.teacherCode]) {
        acc[teacher.teacherCode] = [];
      }
      acc[teacher.teacherCode].push(teacher);
      return acc;
    }, {});
  }

  getTotalNisab(teacher: any[]): number {
    return teacher.reduce((total, subject) => total + subject.nisab, 0);
  }

  getTotalCurrentNisab(teacher: any[]): number {
    return teacher.reduce((total, subject) => total + subject.currentNisab, 0);
  }


  // addTeacherSubjects(teacherSubject: TeacherSubject) {
  //   this.teacherPage.assignSubjects(teacherSubject).finally(() => {
  //     if (this.pageProps.message !== '') {
  //       this.modal.open(this.Status);
  //     }
  //   });
  // }

  // openModal(content, teacher: TeacherVm) {
  //   let ngbModalOptions: NgbModalOptions = {
  //     backdrop: 'static',
  //     keyboard: false
  //   };
  //   this.pageProps.selectedTeacher = teacher;
  //   this.modal.open(content,ngbModalOptions);
  // }
  // reset() {
  //   this.pageProps.selectedTeacher = null;
  //   this.pageProps.teacherItemList = this.pageProps.teacherListDeepCopy;
  //   this.pageProps.teacherItemListCopy = this.pageProps.teacherItemList;
  //   this.pageProps.teacherListDeepCopy = JSON.parse(
  //     JSON.stringify(this.pageProps.teacherItemList)
  //   );
  // }

  // edit() {
  //   this.modal.dismissAll();
  //   this.teacherPage
  //     .updateSubjects(this.pageProps.selectedTeacher)
  //     .finally(() => {
  //       if (this.pageProps.message !== '') {
  //         this.modal.open(this.Status);
  //       }
  //     });
  // }

  // search() {
  //   this.pageProps.teacherItemListCopy = this.pageProps.teacherListDeepCopy;
  //   if (
  //     this.codeTextSearch != null &&
  //     this.codeTextSearch != undefined &&
  //     this.codeTextSearch != ''
  //   ) {
  //     this.pageProps.teacherItemListCopy =
  //       this.pageProps.teacherItemListCopy.filter((ww) =>
  //         ww.teacher.code
  //           .toString()
  //           .toLowerCase()
  //           .includes(this.codeTextSearch.toString().toLowerCase())
  //       );
  //   }
  //   if (
  //     this.nameTextSearch != null &&
  //     this.nameTextSearch != undefined &&
  //     this.nameTextSearch != ''
  //   ) {
  //     this.pageProps.teacherItemListCopy =
  //       this.pageProps.teacherItemListCopy.filter((ww) =>
  //         ww.teacher.name
  //           .toString()
  //           .toLowerCase()
  //           .includes(this.nameTextSearch.toString().toLowerCase())
  //       );
  //   }
  //   if (
  //     this.subjectTextSearch != null &&
  //     this.subjectTextSearch != undefined &&
  //     this.subjectTextSearch != ''
  //   ) {
  //     this.pageProps.teacherItemListCopy =
  //       this.pageProps.teacherItemListCopy.filter((ww) =>
  //         ww.teacher.teacherSubjects.some((sub) =>
  //           sub.name
  //             .toString()
  //             .toLowerCase()
  //             .includes(this.subjectTextSearch.toString().toLowerCase())
  //         )
  //       );
  //   }
  //   if (
  //     this.classTextSearch != null &&
  //     this.classTextSearch != undefined &&
  //     this.classTextSearch != ''
  //   ) {
  //     this.pageProps.teacherItemListCopy =
  //       this.pageProps.teacherItemListCopy.filter((ww) =>
  //         ww.teacher.classes
  //           .toString()
  //           .toLowerCase()
  //           .includes(this.classTextSearch.toString().toLowerCase())
  //       );
  //   }
  //   this.pageProps.teacherItemList = this.pageProps.teacherItemListCopy;
  // }

}
