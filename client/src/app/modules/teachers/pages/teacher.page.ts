import { Injectable } from '@angular/core';
import { Subject } from '@app/modules/classes/interfaces/subject.interface';
import { IResultViewModel } from '@app/modules/shared/interfaces/result.interface';
import { AuthUserService } from '@app/modules/user/services/auth-user.service';
import {
  TeacherItem,
  TeacherSubject,
  TeacherVm,
  UpdateSubjectsVM,
} from '../interfaces/teacher.interface';
import { TeacherPageProps } from '../interfaces/teacherPageProps.interface';
import { TeacherService } from '../services/teacher.service';

@Injectable({
  providedIn: 'root',
})
export class TeacherPage {
  pageProps: TeacherPageProps = {
    isLoading: false,
    message: '',
    resultObj: {
      subjects: [],
      teachers: [],
    },
    teacherItemList: [],
    teacherItemListCopy: [],
    teacherListDeepCopy: [],
    selectedTeacher: {} as TeacherVm,
  };
  constructor(
    private teacherService: TeacherService,
    private userService: AuthUserService
  ) { }

  OnPageEnter(unassign:string): void {
    this.getTeachers(unassign);
  }
  async getTeachers(unassign:string) {
    this.pageProps.message = '';
    const schoolCode = JSON.parse(
      this.userService.getUserSchoolData()
    ).scH_CODE;
    this.pageProps.isLoading = true;
    try {
      const result: any = await this.teacherService.GetTeachers(schoolCode,unassign);
      if (result.success) {
        this.pageProps.resultObj = result.returnObject;
        this.pageProps.teacherItemList=[],
        this.pageProps.resultObj.teachers.forEach((teacher) => {
          this.pageProps.teacherItemList.push({
            teacher,
            teacherSubjectToAdd: {
              code: teacher.code,
              subjectesToAdd: [],
            },
          });
        });
        this.pageProps.teacherItemListCopy = this.pageProps.teacherItemList;
        this.pageProps.teacherListDeepCopy = JSON.parse(
          JSON.stringify(this.pageProps.teacherItemList)
        );
      } else {
        this.pageProps.message = result.arabicMessage;
      }
    } catch ({ error, statusText }) {
      this.pageProps.message = 'حدث خطأ ما حاول مره اخري';
    }
    this.pageProps.isLoading = false;
  }
  async assignSubjects(teacherSubject: TeacherSubject) {
    this.pageProps.message = '';
    this.pageProps.isLoading = true;
    try {
      const result: any = await this.teacherService.assignTeacherSubject(
        teacherSubject
      );
      if (result.success) {
        this.pageProps.teacherItemList.find(
          (ww) => ww.teacher.code === teacherSubject.code
        ).teacher.teacherSubjects = teacherSubject.subjectesToAdd;
        this.pageProps.teacherItemList.find(
          (ww) => ww.teacher.code === teacherSubject.code
        ).teacherSubjectToAdd.subjectesToAdd = [];
        this.pageProps.teacherItemListCopy = this.pageProps.teacherItemList;
        this.pageProps.teacherListDeepCopy = JSON.parse(
          JSON.stringify(this.pageProps.teacherItemList)
        );
      } else {
        this.pageProps.message = result.arabicMessage;
      }
    } catch ({ error, statusText }) {
      this.pageProps.message = 'حدث خطأ ما حاول مره اخري';
    }
    this.pageProps.isLoading = false;
  }
  async updateSubjects(teacher: TeacherVm) {
    this.pageProps.message = '';
    this.pageProps.isLoading = true;
    const oldSubjects = this.pageProps.teacherListDeepCopy.find(
      (ww) => ww.teacher.code === teacher.code
    )?.teacher.teacherSubjects;
    const adding = [];
    const notChanged = [];
    const removed = [];
    teacher.teacherSubjects.forEach((element) => {
      if (oldSubjects.findIndex((ww) => ww.id === element.id) === -1) {
        adding.push(element);
      } else {
        notChanged.push(element);
      }
    });
    oldSubjects.forEach((element) => {
      if (notChanged.findIndex((ee) => ee.id === element.id) === -1) {
        removed.push(element);
      }
    });
    try {
      const updateVm: UpdateSubjectsVM = {
        code: teacher.code,
        addSubjectes: adding,
        removeSubjectes: removed,
      };
      if (
        updateVm.addSubjectes.length > 0 ||
        updateVm.removeSubjectes.length > 0
      ) {
        const result: any = await this.teacherService.updateTeacherSubject(
          updateVm
        );
        if (result.success) {
          this.pageProps.teacherItemList.find(
            (ww) => ww.teacher.code === teacher.code
          ).teacher.teacherSubjects = teacher.teacherSubjects;
          this.pageProps.selectedTeacher = null;
          this.pageProps.teacherItemListCopy = this.pageProps.teacherItemList;
          this.pageProps.teacherListDeepCopy = JSON.parse(
            JSON.stringify(this.pageProps.teacherItemList)
          );
        } else {
          this.pageProps.message = result.arabicMessage;
          this.pageProps.selectedTeacher = null;
          this.pageProps.teacherItemList = this.pageProps.teacherListDeepCopy;
          this.pageProps.teacherItemListCopy = this.pageProps.teacherItemList;
          this.pageProps.teacherListDeepCopy = JSON.parse(
            JSON.stringify(this.pageProps.teacherItemList)
          );
        }
      }
    } catch ({ error, statusText }) {
      this.pageProps.message = 'حدث خطأ ما حاول مره اخري';
      this.pageProps.selectedTeacher = null;
      this.pageProps.teacherItemList = this.pageProps.teacherListDeepCopy;
      this.pageProps.teacherItemListCopy = this.pageProps.teacherItemList;
      this.pageProps.teacherListDeepCopy = JSON.parse(
        JSON.stringify(this.pageProps.teacherItemList)
      );
    }
    this.pageProps.isLoading = false;
  }
}
