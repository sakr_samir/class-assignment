import { ReturnedObj, TeacherItem, TeacherVm } from './teacher.interface';

export interface TeacherPageProps {
  isLoading: boolean;
  message: string;
  resultObj: ReturnedObj;
  teacherItemList: TeacherItem[];
  teacherItemListCopy: TeacherItem[];
  teacherListDeepCopy: TeacherItem[];
  selectedTeacher: TeacherVm;
}
