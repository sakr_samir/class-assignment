import { Subject } from './subject.interface';

export interface ReturnedObj {
  teachers: TeacherVm[];
  subjects: Subject[];
}

export interface TeacherItem {
  teacher: TeacherVm;
  teacherSubjectToAdd: TeacherSubject;
}
export interface TeacherVm {
  code: number;
  teacherCode: number;
  name: string;
  teacherSubjects: Subject[];
  classes: string[];
}
export interface TeacherSubject {
  code: number;
  subjectesToAdd: Subject[];
}
export interface UpdateSubjectsVM {
  code: number;
  addSubjectes: Subject[];
  removeSubjectes: Subject[];
}
