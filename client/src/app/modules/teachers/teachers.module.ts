import { SharedModule } from '@shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TeachersRoutingModule } from './teachers-routing.module';
import { ListComponent } from './components/list/list.component';
import { FormsModule } from '@angular/forms';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

@NgModule({
  declarations: [ListComponent],
  imports: [
    MatProgressSpinnerModule,
    CommonModule,
    FormsModule,
    SharedModule,
    TeachersRoutingModule,
  ],
})
export class TeachersModule {}
