import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { EnvService } from '@app/env.service';
import { IResultViewModel } from '@app/modules/shared/interfaces/result.interface';
import {
  TeacherSubject,
  UpdateSubjectsVM,
} from '../interfaces/teacher.interface';

@Injectable({
  providedIn: 'root',
})
export class TeacherService {
  private baseUrl;
  constructor(private http: HttpClient, private environment: EnvService) {
    this.baseUrl = `${environment.baseApiUrl}/Teacher`;
  }

  GetTeachers(schoolCode: number,unassign:string) {
    return this.http
      .get<IResultViewModel>(
        `${this.baseUrl}/GetTeachers?SchoolCode=${schoolCode}&&unassign=${unassign}`
      )
      .toPromise();
  }

  assignTeacherSubject(teacher: TeacherSubject) {
    return this.http.post(`${this.baseUrl}/AssignSubject`, teacher).toPromise();
  }
  updateTeacherSubject(teacherVM: UpdateSubjectsVM) {
    return this.http
      .post(`${this.baseUrl}/UpdateSubject`, teacherVM)
      .toPromise();
  }
}
