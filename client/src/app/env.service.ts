import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class EnvService {
  // API url
  public baseApiUrl = '';

  // Whether or not to enable debug mode
  public enableDebug = true;
  constructor() {}
}
