import { SplashScreenModule } from './modules/shared/components/_layout/splash-screen/splash-screen.module';
import { LanguageSelectorComponent } from './modules/shared/components/_layout/components/topbar/language-selector/language-selector.component';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from '@app/app-routing.module';
import {
  HttpClient,
  HttpClientModule,
  HTTP_INTERCEPTORS,
} from '@angular/common/http';

import { InlineSVGModule } from 'ng-inline-svg';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { AppComponent } from '@app/app.component';
import { SharedModule } from '@shared/shared.module';
import { HeaderComponent } from '@shared/components/_layout/components/header/header.component';
import { AsideComponent } from '@shared/components/_layout/components/aside/aside.component';
import { FooterComponent } from '@shared/components/_layout/components/footer/footer.component';
import { HeaderMobileComponent } from '@shared/components/_layout/components/header-mobile/header-mobile.component';
import { TopbarComponent } from '@shared/components/_layout/components/topbar/topbar.component';
import { HeaderMenuComponent } from '@shared/components/_layout/components/header/header-menu/header-menu.component';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { AuthUserInterceptor } from './modules/user/services/auth-user.interceptor';
import { EnvServiceProvider } from './env.service.provider';

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    AsideComponent,
    FooterComponent,
    HeaderMobileComponent,
    TopbarComponent,
    HeaderMenuComponent,
    LanguageSelectorComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    SharedModule,
    NgbDropdownModule,
    SplashScreenModule,
    InlineSVGModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient],
      },
    }),
  ],
  providers: [
    EnvServiceProvider,
    { provide: HTTP_INTERCEPTORS, useClass: AuthUserInterceptor, multi: true },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
