# SIS module boilerplate

A boilerplate for the front end of SIS modules.

## Structure

```
\---src
    +---app
    |   +---modules
    |   |   +---dashboard
    |   |   +---example
    |   |   |   +---components
    |   |   |   |   \---example-one
    |   |   |   +---helpers
    |   |   |   +---interfaces
    |   |   |   +---services
    |   |   \---shared
    |   |       +---components
    |   |       |   +---example
    |   |       |   \---layout
    |   |       |       +---footer
    |   |       |       +---header
    |   |       |       +---side-menu
    |   |       +---interfaces
    |   |       +---services
    |   \---_metronic
    +---assets
    \---environments
```

## CLI commands

Here's some commands that's commonly used:

### Create components or modules

- Suppose u want to create new module named `example`, run: `ng g m --routing --route=example -m=app modules/example`
- Suppose u want to create new component inside `example` module named `example-one`, run: `ng g c modules/example/components/example-one -m=example`
- To make the previous created component with route, register it inside `example-routing.module.ts` and create routing object for it inside `routes` array like previous components.
